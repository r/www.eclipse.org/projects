<?php
/*
 * Copyright (c) Eclipse Foundation and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

require_once dirname(__FILE__) . '/Project.class.php';
require_once dirname(__FILE__) . '/database.inc';

class Review {
	public static function getLastSuccessful(Project $project) {
		$sql = "
			select
				project, name, date, group_concat(type) as type, status, url
			from ProjectReviews
			where project=':id'
				and type in ('release', 'progress') and status='success'
			group by project, name, date
			order by date desc
			limit 1";
		$args = array(':id' => $project->getId());

		return self::getOne($sql, $args);
	}

	public static function getRecentOngoing(Project $project) {
		$sql = "
			select
				project, name, date, group_concat(type) as type, status, url
			from ProjectReviews
			where project=':id'
				and type in ('release', 'progress') and status='ongoing'
				and date < now()
			group by project, name, date
			order by date desc
			limit 1";
		$args = array(':id' => $project->getId());

		return self::getOne($sql, $args);
	}

	public static function get($since, $callable) {
		$sql = "
			select
				project, name, date, group_concat(type) as type, status, url
			from ProjectReviews
			where date >= date(':date:')
			group by project, name, date
			order by date desc";

		$args = array(
				':date:' => date('Y-m-d', $since)
		);

		query('dashboard', $sql, $args, function($row) use (&$callable) {
			call_user_func($callable, new self($row));
		});
	}

	public static function getAll($callable) {
		$sql = "
			select
				project, name, date, group_concat(type) as type, status, url
			from ProjectReviews
			group by project, name, date
			order by date desc";

		query('dashboard', $sql, array(), function($row) use (&$callable) {
			call_user_func($callable, new self($row));
		});
	}

	private static function getOne($sql, $args) {
		$review = null;
		query('dashboard', $sql, $args, function($row) use (&$review) {
			$review = new Review($row);
		});

		return $review;
	}

	var $data;

	public function __construct($data) {
		$this->data = $data;
	}

	public function getProjectId() {
		return $this->data['project'];
	}

	public function getName() {
		$name = $this->data['name'];
		if ($project = Project::getProject($this->getProjectId())) {
			$name = "{$project->getFormalName()} {$name}";
		}
		return $name;
	}

	public function getDate() {
		return strtotime($this->data['date']);
	}

	public function getStatus() {
		return $this->data['status'];
	}

	public function isPending() {
		return $this->getStatus() == 'pending';
	}

	public function getUrl() {
		return $this->data['url'];
	}
}