<?php
/*******************************************************************************
 * Copyright (c) 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Wayne Beaton (Eclipse Foundation)- initial API and implementation
 *******************************************************************************/

/**
 * This class represents a forge instance (e.g. Eclipse,
 * or PolarSys). The intent is to try and centralize the notion of a
 * forge rather than have bits of code here and there to handle all of
 * the different forges (i.e. to reduce the long term maintenance burden).
 */
class Forge {
    private $data;
    private static $forges;

    /**
     * Get $forges
     *
     * @return Forge[]
     */
    static function getForges() {
        if (!empty(self::$forges)) {
            return self::$forges;
        }

        $forges = array(
                'eclipse' => array(
                        'id' => 'eclipse',
                        'name' => 'Eclipse',
                        'url' => 'https://projects.eclipse.org'
                )
        );

        foreach ($forges as &$forge) {
            $forge = new self($forge);
        }

        return self::$forges = $forges;
    }

    static function getForge($id) {
        $forges = self::getForges();
        return $forges[$id];
    }

    static function getDefault() {
        return self::getForge('eclipse');
    }

    static function getForgeForProjectId($id) {
        $segments = explode('.', $id);

        foreach(self::getForges() as $id => $forge) {
            if ($id == $segments[0]) return $forge;
        }

        return self::getDefault();
    }

    function __construct($data) {
        $this->data = $data;
    }

    function getId() {
        return $this->data['id'];
    }

    function getName() {
        return $this->data['name'];
    }

    function getUrl() {
        return $this->data['url'];
    }

    function getLocalProjectId($id) {
        if ($this->isEclipseForge()) return $id;
        $forgeId = $this->getId();
        if (preg_match("/^$forgeId\.(.*)$/",$id, $matches)) {
            return $matches[1];
        }
        return null;
    }

    function getFoundationDBId($id) {
        if ($this->isEclipseForge()) return $id;
        return "{$this->getId()}.$id";
    }

    function isEclipseForge() {
        return $this->getId() == 'eclipse';
    }
}
?>