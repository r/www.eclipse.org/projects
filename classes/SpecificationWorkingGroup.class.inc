<?php
/*
* Copyright (c) Eclipse Foundation and others.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
*/

require_once dirname(__FILE__) . '/Project.class.php';
require_once dirname(__FILE__) . '/database.inc';

/**
 * This is a simple representation of a working group associated with
 * a specification project.
 */
class SpecificationWorkingGroup {
	public static function getForProject(Project $project) {
		$sql = "
			select
				id, specification
			from Project
			where id =':id' and specification != ''
		";
		$args = array(':id' => $project->getId());

		$specification = null;
		query('dashboard', $sql, $args, function($row) use (&$specification) {
			$specification = new self($row);
		});

			return $specification;
	}

	var $data;

	private function __construct($data) {
		$this->data = $data;
	}

	public function getId() {
		return $this->data['specification'];
	}
}