<?php
/*
* Copyright (c) Eclipse Foundation and others.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
*/

require_once dirname(__FILE__) . '/Project.class.php';
require_once dirname(__FILE__) . '/database.inc';

/**
 * This is a simple representation of a working group
 */
class WorkingGroup {

	var $data;

	public static function getWorkingGroup($id) {
		$wg = null;
		$sql = "select id, name from WorkingGroup where id=':id:';";
		query('dashboard', $sql, array(':id:' => $id), function($row) use (&$wg) {
			$wg = new WorkingGroup($row);
		});
		return $wg;
	}

	public static function getAll() {
		$wg = array();
		$sql = "select id, name from WorkingGroup";
		query('dashboard', $sql, array(), function($row) use (&$wg) {
			$wg[] = new WorkingGroup($row);
		});
		return $wg;
	}

	public static function getAny() {
		$wg = null;
		$sql = "select id, name from WorkingGroup order by rand() limit 1";
		query('dashboard', $sql, array(), function($row) use (&$wg) {
			$wg = new WorkingGroup($row);
		});
		return $wg;
	}

	private function __construct($data) {
		$this->data = $data;
	}

	public function getId() {
		return $this->data['id'];
	}

	public function getName() {
		return $this->data['name'];
	}

	public function projectsDo($callback) {
		$sql = "
			select
				wgp.id,
				p.id as project
			from WorkingGroupProject as wgp
			join Project as p on wgp.project=p.id
			where wgp.id=':id:'";

		query('dashboard', $sql, array(':id:' => $this->getId()), function($row) use (&$callback) {
			if ($project = Project::getProject($row['project'])) {
				call_user_func($callback, $project);
			}
		});
	}

	public function specificationProjectsDo($callback) {
		$sql = "
			select p.id from Project where specification=':id:'";

		query('dashboard', $sql, array(':id:' => $this->getId()), function($row) use (&$callback) {
			if ($project = Project::getProject($row['id'])) {
				call_user_func($callback, $project);
			}
		});
	}

}