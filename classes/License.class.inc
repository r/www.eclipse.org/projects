<?php
/*******************************************************************************
 * Copyright (c) 2017 Eclipse Foundation and others.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

require_once (dirname(__FILE__) . "/database.inc");

class License {
 var $id;
 var $text;
 var $url;
 var $primary;

 private function __construct($id, $text, $url, $primary = true) {
  $this->id = $id;
  $this->text = $text;
  $this->url = $url;
  $this->primary = $primary;
 }

 public static function getLicense($id) {
  $sql = "
    select
       l.LicenseID as id,
       l.description as text,
       l.SPDXCode as spdx,
       l.Url as url,
       l.IsPrimary as isPrimary
    from SYS_Licenses as l
    where l.LicenseId='$id' or l.SPDXCode='$id'";

 	$licenses = array ();
 	query ( 'foundation', $sql, array (), function ($row) use (&$licenses) {
 		$licenses [] = new License($row['spdx'], $row['text'], $row['url'], $row['isPrimary'] ? true : false);
 	} );

 	return $licenses ? $licenses[0] : null;
 }

 public static function getLicenses($codes) {
  $licenses = array();
  foreach($codes as $code) {
   $licenses[] = self::getLicense($code);
  }
  return $licenses;
 }

 /**
  * Return an array of licenses for a project.
  *
  * @param string $id Project Id
  * @return License[]
  */
 public static function getLicensesForProject($id) {
  $sql = "
    select
       l.LicenseId as id,
       l.description as text,
       l.SPDXCode as spdx,
       l.Url as url,
       l.IsPrimary as isPrimary
    from ProjectLicenses as pl
      join SYS_Licenses as l on pl.LicenseId=l.LicenseId
    where pl.ProjectId='$id'";

  $licenses = array ();
  query ( 'foundation', $sql, array (), function ($row) use (&$licenses) {
   $licenses [] = new License($row['spdx'], $row['text'], $row['url'], $row['isPrimary'] ? true : false);
  } );

  self::sortLicenses($licenses);
  return $licenses;
 }

 private static function sortLicenses(&$licenses) {
  // Make sure that the EPL is always at the top of the list.
  usort($licenses, function($a, $b) {
   if ($a->getId() == 'EPL-2.0') return -1;
   if ($b->getId() == 'EPL-2.0') return 1;
   if ($a->getId() == 'EPL-1.0') return -1;
   if ($b->getId() == 'EPL-1.0') return 1;
   return strcasecmp($a->getId(), $b->getId());
  });
 }

 public static function getDefaultFileHeader($licenses) {
  $lines = array();
  $lines[] = "Copyright (c) {date} {owner}[ and others]";

  return self::getHeader($licenses, $lines);
 }

 public static function getAlternativeFileHeader($licenses) {
  $lines = array();
  $lines[] = "Copyright (c) {date} Contributors to the Eclipse Foundation";
  $lines[] = "See the NOTICE file(s) distributed with this work for additional information regarding copyright ownership.";

  return self::getHeader($licenses, $lines);
 }

 private static function getHeader($licenses, $lines) {
  $lines[] = self::getLicensesStatement($licenses);

  if (count($licenses) == 1 && ($licenses[0]->getSPDXCode() == 'Apache-2.0')) {
   $lines[] = 'Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.';
  }

  $lines[] = "SPDX-License-Identifier: " . self::getSPDXExpression($licenses);
  return implode("\n\n", $lines);
 }

 /**
  * Answer a statement (suitable for use in a file header)
  * that describes the licenses of the project.
  *
  * @param License[] $licenses
  * @param string $nbsp value to use as the "non-breaking" space in the license names.
  * @return string
  */
 public static function getLicensesStatement($licenses) {
  if (empty($licenses)) return null;
  // Initial statement of license(s)
  $parts = array();
  $secondary = array();
  foreach($licenses as $license) {
   $text = $license->getText();
   if ($license->isSecondaryLicense()) {
    $secondary[] = "{$text} which is available at {$license->getUrl()}";
   } else {
    $parts[] = "{$text} which is available at {$license->getUrl()}";
   }
  }

  $statement = "This program and the accompanying materials are made available under the terms of the ";

  $statement .= implodeWithConjunction($parts, 'or the') . ".";

  if ($secondary) {
   $statement .= "\n\n";
   // FIXME License text is inlined; should come from data.
   $epl20 = License::getLicense('EPL-2.0');
   // FIXME We assume that secondary licenses only apply to EPL-2.0 (true for now)
   $statement .= "This Source Code may also be made available under the following Secondary Licenses when the conditions for such availability set forth in the {$epl20->getText()} are satisfied: ";
   $statement .= implodeWithConjunction($secondary, 'and') . ".";
  }

  return $statement;
 }

 public static function getSPDXExpression($licenses) {
  $codes = array();

  foreach($licenses as $license) {
    $codes[] = $license->getSPDXCode();
  }

  return implode(' OR ', $codes);
 }

 public static function parseLicenses($text) {
  $licenses = array();
  foreach(explode(',', $text) as $code) {
   if ($license = self::getLicense($code)) {
    $licenses[] = $license;
   }
  }
  if (!$licenses) $licenses[] = License::getLicense('EPL-2.0');

  self::sortLicenses($licenses);

  return $licenses;
 }

 public function getId() {
  return $this->id;
 }

 public function getText() {
  return preg_replace('/\(secondary\) ?/','',$this->text);
 }

 public function getSPDXCode() {
  return $this->id;
 }

 public function isPrimary() {
 	return $this->primary;
 }

 /**
  * Answers true when the receiver is a secondary
  * license according to the terms of the EPL-2.0,
  * or false otherwise.
  */
 public function isSecondaryLicense() {
  return !$this->isPrimary();
 }

 public function getUrl() {
  return $this->url;
 }
}

?>