<?php
/*******************************************************************************
 * Copyright (c) 2010 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *   Wayne Beaton - initial API and implementation
 *******************************************************************************/

/*
 * This is a simple tracing facility. If a 'trace' parameter is included in the
 * HTTP GET header, then trace information is gathered and made available for display.
 * 
 * Submit trace messages using the trace($message) function. Gathered
 * trace information can be collected into a single string of HTML
 * content using the get_trace_html() function; note that this function
 * will only provide non-empty content if the 'trace' parameter is included
 * in the HTTP GET header.
 * 
 * More complex interaction is possible. Tracing can be nested by using
 * the value returned by the trace($message) function. For example:
 * 
 * $tracer = trace('started something');
 * $tracer->trace('step 1');
 * $tracer->trace('step 2');
 * etc.
 * 
 * This would be rendered as a nested list that would appear thusly:
 * 
 * - started something
 *   - step 1
 *   - step 2
 * ...
 * 
 * The nesting can be arbitrarily deep.
 * 
 * Note that turning on the trace also turns on PHP error reporting.
 */
if (reportErrors()) {
	error_reporting(E_ALL);
	ini_set("display_errors", true);
}

if (enableTrace()) {
	$_tracer = new Tracer();
} else { 
	$_tracer = new NullTracer();
}

function reportErrors() {
	return isset($_GET['errors']) || hasCommandLineArgument('-errors');
}
	
function enableTrace() {
	return isset($_GET['trace']) || hasCommandLineArgument('-trace');
}

function hasCommandLineArgument($value) {
	global $argv;
	if (!$argv) return false;
	foreach($argv as $arg) if ($arg == $value) return true;
	return false;
}

/*
 * @deprecated Use trace($message) instead.
 */
function debugMessage($message) {
	if (!isset($_GET['debug'])) return;
	
	trace($message);
}


$_file_tracer = trace("File Information");

trace_file_info(__FILE__); // Might as well trace information about this file...

function trace($message) {
	global $_tracer;
	
	return $_tracer->trace($message);
}

function get_trace_html() {
	global $_tracer;
	
	return $_tracer->to_html_div();
}

function get_trace_xml() {
	global $_tracer;
	
	return $_tracer->to_xml();
}

function get_trace_text() {
	global $_tracer;
	
	return $_tracer->to_text();
}

/**
 * Returns the trace information in an array suitable for
 * export using JSON.
 *
 * @return string
 */
function get_trace_array() {
	global $_tracer;

	return $_tracer->to_array();
}

/**
 * Add some basic information about the given file to the trace data.
 * For the time being, this is limited to the modified date. This can
 * be helpful to let you know if you're actually running the latest
 * version of the file.
 * 
 * You can use the __FILE__ super global to easily include this 
 * information by adding the following line to your PHP file (after
 * the inclusion of this file, of course):
 * 
 * trace_file_info(__FILE__);
 */
function trace_file_info($fileName) {
	global $_file_tracer;
	
	$last_modified = filemtime($fileName);
	$date = date('l, dS F, Y @ h:ia', $last_modified);
	$_file_tracer->trace("File $fileName last modified on $date");
}

/*
 * This class is not intended to be instantiated by clients.
 */
class Tracer {
	var $message;
	var $children = array();
	
	function trace($message) {
		$child = new Tracer();
		$child->message = $message;
		$this->children[] = $child;
		return $child;
	}
	
	function to_html_div() {
		$html = '<div style="border-style:solid;border-width:1px"><p>Trace information</p>';
		$html .= $this->to_html();
		$html .= '</div>';
		
		return $html;
	}
	
	/* private */ function to_html() {
		$html = $this->message;
		if ($this->children) {
			$html .= '<ul>';
			foreach($this->children as $child) {
				$html .= '<li>' . $child->to_html() . '</li>';
			}
			$html .= '</ul>';
		}
		return $html;
	}
	
	function to_xml() {
		return "<!--\n" . $this->to_text() . "-->";
	}
	
	function to_text($indent = "  ") {
		$result = "- $this->message\n";
		foreach($this->children as $child) {
			$result .= $child->to_text("$indent  ");
		}
		return $result;
	}
	
	function to_array() {
		$values = array();
		$values[] = $this->message;
		if ($this->children) {
			foreach($this->children as $child) {
				$values[] = $child->to_array();
			}
		}
		return $values;
	}
}

/*
 * This class is not intended to be instantiated by clients.
 */
class NullTracer {
	function trace($message) {
		return $this;
	}
	
	function to_html_div() {
		return '';
	}
	
	function to_xml() {
		return '';
	}
	
	function to_text() {
		return '';
	}
	
	
	function to_array() {
		return null;
	}
}
?>