<?php
$Nav->setLinkList( array() );
$Nav->addNavSeparator("Projects", 	"/projects/");
$Nav->addCustomNav("What's New?", "/projects/whatsnew.php", "", 1);
$Nav->addCustomNav("List of Projects", "/projects/listofprojects.php", "", 1);
$Nav->addCustomNav("Rules &amp; Processes", "/projects/dev_process/index-quick.php", "", 1);
$Nav->addCustomNav("FAQ", "/projects/dev_process/index.php", "", 1);
$Nav->addCustomNav("Tools for committers", "/projects/tools", "", 1);
?>
