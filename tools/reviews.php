<?php
/*******************************************************************************
 * Copyright (c) 2010, 2021 Eclipse Foundation and others.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

require_once($_SERVER['DOCUMENT_ROOT'] . "/projects/classes/common.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/projects/classes/debug.php");
trace_file_info(__FILE__);

require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php");
$App = new App();
$Nav = new Nav();
$Menu = new Menu();

include($App->getProjectCommon());
$App->AddExtraHtmlHeader('<link rel="stylesheet" type="text/css" href="/projects/web-parts/projects.css"/>');

require_once(dirname(__FILE__). "/../classes/Review.class.inc");

ob_start();

$pageKeywords = "";
$pageTitle = "Reviews";
$pageAuthor = "Wayne Beaton";
?>

<div id="midcolumn">
<div class="homeitem">
<h1><?= $pageTitle ?></h1>

<?php

$nested = array();
Review::getAll(function ($review) use (&$nested) {
	$nested[date('Y-m-d', $review->getDate())][] = $review;
});

foreach($nested as $date => $reviews) {
	$reviewDate = $App->getFormattedDate(strtotime($date), 'long');

	echo "<h3>$reviewDate</h3>";
	echo "<ul>";
	foreach($reviews as $review) {
		print "<li>";
		print "<span class=\"review-{$review->getStatus()}\"><a href=\"{$review->getUrl()}\">{$review->getName()}</a></span>";
		print "</li>";
	}
	echo "</ul>";
}

echo get_trace_html();
?>

</div>
</div>

<?php

$html = ob_get_contents();
ob_end_clean();
$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>