<?php
/**
 * Copyright (c) Eclipse Foundation and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
require_once dirname ( __FILE__ ) . '/../../eclipse.org-common/system/app.class.php';
require_once dirname ( __FILE__ ) . '/../../eclipse.org-common/system/nav.class.php';
require_once dirname ( __FILE__ ) . '/../../eclipse.org-common/system/menu.class.php';
$App = new App ();
$Nav = new Nav ();
$Menu = new Menu ();
include ($App->getProjectCommon ());

require_once dirname ( __FILE__ ) . '/../classes/Project.class.php';
require_once dirname ( __FILE__ ) . '/../classes/Release.class.inc';
require_once dirname ( __FILE__ ) . '/../classes/Review.class.inc';
require_once dirname ( __FILE__ ) . '/../classes/SpecificationWorkingGroup.class.inc';
require_once dirname ( __FILE__ ) . '/../classes/debug.php';
require_once dirname ( __FILE__ ) . '/../classes/database.inc';

$pageTitle = "Future Eclipse Project Releases";
$pageKeywords = "";
$pageAuthor = "Wayne Beaton";

/**
 * Answers an array containing information about future releases.
 * The array that gets returned maps review dates to a collection of
 * releases that will be concluded on the date.
 *
 * @return array|Release
 */
function getFutureReleases() {
	$releases = array();
	foreach (Release::getFutureReleases() as $release) {
		$releases[date('F Y', $release->getDate())][] = $release;
	}
	return $releases;
}


$App->AddExtraHtmlHeader(<<< EOS
<style>
div.past {
	opacity: 60%;
}
</style>
EOS
);
/**
 * Write HTML describing the future releases.
 */
function dumpFutureReleases() {
	foreach ( getFutureReleases () as $date => $releases ) {
		echo "<h3>{$date}</h3>";
		echo "<ul>";
		foreach ( $releases as $release ) {
			$class = $release->getDate() < time() ? 'past' : 'future';

			echo "<div class={$class}>";
			echo "<li><a href=\"{$release->getUrl()}\">{$release->getName()}</a> ";
			echo "<ul>";
			echo "<li>Release date: " . date ( 'Y-m-d', $release->getDate () ) . "</li>";

			$reviewRequired = false;
			$project = Project::getProject($release->getId());
			if (SpecificationWorkingGroup::getForProject($project)) {
				echo "<li>This is a specification project; <strong>a ballot may be required</strong></li>";
				$reviewRequired = true;
			}
			if ($review = Review::getLastSuccessful($project)) {
				echo "<li>Last successful review: <a href=\"{$review->getUrl()}\">" . date ('Y-m-d', $review->getDate()) . "</a></li>";
				if ($release->getDate() - $review->getDate() > (365 * 24 * 60 * 60)) {
					echo "<li>The last review was more than one year before</li>";
					$reviewRequired = true;
				}
			} else {
				echo " <li>No prior reviews</li>";
				$reviewRequired = true;
			}
			if ($review = Review::getRecentOngoing($project)) {
				echo "<li>Ongoing review: <a href=\"{$review->getUrl()}\">" . date ('Y-m-d', $review->getDate()) . "</a></li>";
			}
			if ($reviewRequired) {
				echo "<li><b>A release review is required</b></li>";
			} else {
				echo "<li>A release review is not required.</li>";
			}

			dumpOpenCQs ( $release );
			echo "</ul>";
			echo "</li>";
			echo "</div>";
		}
		echo "</ul>";
	}
}

/**
 * Write an HTML unordered list of all open CQs for the project
 * that owns the release.
 *
 * @param Release $release
 */
function dumpOpenCQs(Release $release) {
	/*
	 * Query IPZilla directly to get the list of open CQs
	 * associated with the release.
	 */
	$sql = "
		select
			id, title
		from IPLabIssue
		where project=':project:' and closedBy is null;
	";

	$rows = array();
	query ( 'dashboard', $sql, array (':project:' => $release->getId ()), function ($row) use (&$rows) {
		$id = $row ['id'];
		$title = $row ['title'];
		$rows[] = "<li><a target=_blank href=\"https://gitlab.eclipse.org/eclipsefdn/emo-team/iplab/-/issues/{$id}\">{$id}</a> {$title}</li>";
	} );

	if ($rows) {
		echo "<li>Pending IP Issues:";
		echo "<ul>";
		foreach ($rows as $row) echo $row;
		echo "</ul>";
		echo "</li>";
	}
}

ob_start ();
?>
<div id="maincontent">
	<div id="midcolumn">
		<h1><?=$pageTitle?></h1>
		<p>
			<strong>EXPERIMENTAL</strong>. This page shows a list of recent past and upcoming
			releases and&mdash;where they exist&mdash;corresponding lists of open
			intellectual property review requests (CQs). Note that the CQs are
			associated with the projects, not with any particular release. This
			is for informational purposes only.
		</p>

<?php dumpFutureReleases(); ?>

</div>
</div>

<?php
$html = ob_get_contents ();
ob_end_clean ();
$App->generatePage ( $theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html );
?>
