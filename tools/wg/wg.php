<?php
/**
 * Copyright (c) Eclipse Foundation and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */


require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php");
$App = new App();
$Nav = new Nav();
$Menu = new Menu();

require_once($_SERVER['DOCUMENT_ROOT'] . "/projects/classes/database.inc");
require_once($_SERVER['DOCUMENT_ROOT'] . "/projects/classes/common.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/projects/classes/debug.php");

include($App->getProjectCommon());

ob_start();

$pageKeywords = "";
$pageTitle = "Eclipse Foundation Working Group Activity Report";
$pageAuthor = "Wayne Beaton";
?>

<div id="midcolumn">
<h1><?= $pageTitle ?></h1>

<div class="homeitem">

<p>On this page, activity that has occurred in the three months before the "Generated on" date above.
is considered to be "recent". The number for "Recent Member Companies", for example, indicates the number of member companies that
have been active in project repositories (at least one commit) in the last three months).</p>

<p>"Lifetime" values are just that: data captured over the entire lifetime of the project. In most
cases, lifetime data includes work that was done in project repositories prior to projects being moved
over to the Eclipse Foundation.</p>

<p>Charts show the last five years of activity.</p>

<hr/>

<?php
$files = glob("/home/data/httpd/writable/projects/wg-*.html");
rsort($files);

foreach($files as $file) {
	$matches = array();
	if (preg_match('/^\/home\/data\/httpd\/writable\/projects\/(?<wg>.+)\-(?<date>\d\d\d\d\-\d\d\-\d\d)\-(?<time>\d\d\d\d)\.html$/', $file, $matches)) {
		$dates[$matches['date']] = $matches['date']; // eliminate duplicates
	}
}

if ($dates) {
	$Nav->addNavSeparator("Working Groups", null);

	query('dashboard', 'select id, name from WorkingGroup', array(), function($row) use (&$Nav) {
		$Nav->addCustomNav($row['name'], "#{$row['id']}", "_self", 2);
	});

	$Nav->addNavSeparator("Previous Reports", null);
	krsort($dates);
	$count = 0;
	foreach($dates as $date) {
		if ($count++ >= 18) break;
		$Nav->addCustomNav("{$date}", "?date={$date}", "_self", 2);
	}
}

if (isset($_GET['date']) && preg_match('/\d\d\d\d\-\d\d\-\d\d/', $_GET['date'])) {

	// We just want the first one that we find.
	foreach(glob("/home/data/httpd/writable/projects/wg-{$_GET['date']}-*.html") as $file) {
		include $file;
		break;
	}
} else {
	reset($files);
	if ($latest = current($files)) {
		include $latest;
	} else {
		echo "No data.";
	}
}
?>

</div>
</div>

<?php
$html = ob_get_contents();
ob_end_clean();

$App->generatePage(null, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>