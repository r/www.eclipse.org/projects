<?php
/**
 * Copyright (c) Eclipse Foundation and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */


require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php");
$App = new App();
$Nav = new Nav();
$Menu = new Menu();

require_once dirname(__FILE__) . '/../../classes/common.php';
mustBeFoundationEmployee();

include($App->getProjectCommon());

ob_start();

$pageKeywords = "";
$pageTitle = "Eclipse Foundation Cryptography Use Report";
$pageAuthor = "Wayne Beaton";
?>

<div id="midcolumn">
<h1><?= $pageTitle ?></h1>

<div class="homeitem">

<?php
$files = glob("/home/data/httpd/writable/projects/cryptography-*.txt");
rsort($files);

foreach($files as $file) {
	$matches = array();
	if (preg_match('/^\/home\/data\/httpd\/writable\/projects\/(?<wg>.+)\-(?<date>\d\d\d\d\-\d\d\-\d\d)\-(?<time>\d\d\d\d)\.txt$/', $file, $matches)) {
		if (preg_match('/0[123]$/',$matches['date'])) {
			$dates[$matches['date']] = $matches['date']; // eliminate duplicates
		}
	}
}

if ($dates) {
	$Nav->addNavSeparator("Previous Reports", null);
	krsort($dates);
	$count = 0;
	foreach($dates as $date) {
		if ($count++ >= 18) break;
		$Nav->addCustomNav("{$date}", "?date={$date}", "_self", 2);
	}
}

echo "<pre>";
if (isset($_GET['date']) && preg_match('/\d\d\d\d\-\d\d\-\d\d/', $_GET['date'])) {

	// We just want the first one that we find.
	foreach(glob("/home/data/httpd/writable/projects/cryptography-{$_GET['date']}-*.txt") as $file) {
		include $file;
		break;
	}
} else {
	reset($files);
	if ($latest = current($files)) {
		include $latest;
	} else {
		echo "No data.";
	}
}
echo "</pre>";
?>

</div>
</div>

<?php
$html = ob_get_contents();
ob_end_clean();

$App->generatePage(null, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>