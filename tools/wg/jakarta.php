<?php
/**
 * Copyright (c) Eclipse Foundation and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */


require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php");
$App = new App();
$Nav = new Nav();
$Menu = new Menu();

require_once dirname(__FILE__) . '/../../classes/common.php';

include($App->getProjectCommon());

ob_start();

$pageKeywords = "";
$pageTitle = "Jakarta EE Working Group Stats";
$pageAuthor = "Wayne Beaton";
?>

<div id="midcolumn">

<div class="homeitem">

<?php include "/home/data/httpd/writable/projects/jakarta.html"; ?>

</div>
</div>

<?php
$html = ob_get_contents();
ob_end_clean();

$App->generatePage(null, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>