<?php
/**
 * Copyright (c) Eclipse Foundation and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php");
$App = new App();
$Nav = new Nav();
$Menu = new Menu();

require_once dirname(__FILE__) . '/../../classes/common.php';
mustBeFoundationEmployee();

include($App->getProjectCommon());

ob_start();

$pageKeywords = "";
$pageTitle = "Eclipse Cloud Development Tools Working Group Stats";
$pageAuthor = "Wayne Beaton";
?>

<div id="midcolumn">
<h1><?= $pageTitle ?></h1>

<div class="homeitem">

<?php
function includeContent($wg) {
	global $Nav;
	$dates = array();
	foreach(glob("/home/data/httpd/writable/projects/{$wg}-*.html") as $file) {
		$matches = array();
		if (preg_match('/^\/home\/data\/httpd\/writable\/projects\/(?<wg>.+)\-(?<date>\d\d\d\d\-\d\d\-\d\d)\-(?<time>\d\d\d\d)\.html$/', $file, $matches)) {
			$dates[$matches['date']] = $matches['date']; // eliminate duplicates
		}
	}

	if ($dates) {
		$Nav->addNavSeparator("Previous Reports", null);
		krsort($dates);
		$count = 0;
		foreach($dates as $date) {
			if ($count++ >= 12) break;
			$Nav->addCustomNav("{$date}", "?date={$date}", "_self", 2);
		}
	}
	if (isset($_GET['date'])) {
		$date = date('Y-m-d', strtotime($_GET['date']));
		// We just want the first one that we find.
		foreach(glob("/home/data/httpd/writable/projects/{$wg}-{$date}-*.html") as $file) {
			include $file;
			return;
		}
	}
	include "/home/data/httpd/writable/projects/{$wg}.html";
}

includeContent('ecd-tools');

?>

</div>
</div>

<?php
$html = ob_get_contents();
ob_end_clean();

$App->generatePage(null, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>