<?php
/**
 * Copyright (c) 2023 Eclipse Foundation and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */


require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php");
$App = new App();
$Nav = new Nav();
$Menu = new Menu();

require_once dirname(__FILE__) . '/../../classes/common.php';

include($App->getProjectCommon());

ob_start();

$pageKeywords = "";
$pageTitle = "Eclipse Project SBOMs";
$pageAuthor = "Wayne Beaton";
?>

<div id="midcolumn">
	<h1><?php echo $pageTitle; ?></h1>

	<p>
		For information regarding how to generate SBOMs for your project,
		please see <a
			href="https://gitlab.eclipse.org/eclipsefdn/emo-team/sbom">Eclipse
			Foundation SBOM Generation</a>.
	</p>

	<p>These are some of the SBOMs that we know about. This list is updated
		regularly.</p>

	<blockquote>
		This page is not currently discoverable from eclipse.org. This is
		intentional as this page is considered <em>temporary</em> while we
		explore more robust solutions. We will most likely retire this page in
		2024.
	</blockquote>

	<div class="homeitem">

<?php include "/home/data/httpd/writable/projects/sboms.html"; ?>

</div>
</div>

<?php
$html = ob_get_contents();
ob_end_clean();

$App->generatePage(null, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>