<?php
/**
 * Copyright (c) Eclipse Foundation and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
require_once dirname ( __FILE__ ) . '/../../eclipse.org-common/system/app.class.php';
require_once dirname ( __FILE__ ) . '/../../eclipse.org-common/system/nav.class.php';
require_once dirname ( __FILE__ ) . '/../../eclipse.org-common/system/menu.class.php';
require_once dirname(__FILE__) . '/../classes/database.inc';
require_once dirname(__FILE__) . '/../classes/common.php';

$App = new App ();
$Nav = new Nav ();
$Menu = new Menu ();
include ($App->getProjectCommon ());

$pageTitle = "Eclipse Specification Projects";
$pageKeywords = "";
$pageAuthor = "Wayne Beaton";

$wg = array();
$sql = "select wg.id as wgId, wg.name as wgName, p.id as project, p.name from WorkingGroup as wg join Project as p on wg.id=p.specification order by wgId, project";
query("dashboard", $sql, array(), function($row) use (&$wg) {
	$wg[$row['wgId']]['name'] = $row['wgName'];
	$wg[$row['wgId']]['projects'][] = array('id' => $row['project'], 'name' => $row['name']);
});

ob_start ();
?>
<div id="maincontent">
	<div id="midcolumn">
		<h1><?=$pageTitle?></h1>
		<p>
			<strong>EXPERIMENTAL!</strong>
		</p>
		<p>
			This page provides a list of specification projects operating under
			the Eclipse Foundation Specification process.
		</p>

	<?php
	foreach($wg as $wgId => $each) {
		echo "<h2>{$each['name']}</h2>";
		echo "<ul>";
		foreach($each['projects'] as $project) {
			echo "<li><a href=\"https://projects.eclipse.org/projects/{$project['id']}\">{$project['name']}</a></li>";
		}
		echo "</ul>";
	}


	?>

	</div>
</div>

<?php
$html = ob_get_contents ();
ob_end_clean ();
$App->generatePage ( $theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html );
?>
