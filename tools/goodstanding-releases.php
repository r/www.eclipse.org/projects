<?php
/**
 * Copyright (c) Eclipse Foundation and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
require_once dirname ( __FILE__ ) . '/../../eclipse.org-common/system/app.class.php';
require_once dirname ( __FILE__ ) . '/../../eclipse.org-common/system/nav.class.php';
require_once dirname ( __FILE__ ) . '/../../eclipse.org-common/system/menu.class.php';
require_once dirname(__FILE__) . '/../classes/database.inc';

$App = new App ();
$Nav = new Nav ();
$Menu = new Menu ();
include ($App->getProjectCommon ());

$pageTitle = "Eclipse Project Releases in Good Standing";
$pageKeywords = "";
$pageAuthor = "Wayne Beaton";

ob_start ();
?>
<style>
	li.future {color: gray}
	li.future a:link {color: gray}
	li.future a:visited {color: gray}
	li.past {}
</style>
<div id="maincontent">
	<div id="midcolumn">
		<h1><?=$pageTitle?></h1>
		<p>
			<strong>EXPERIMENTAL</strong>. This page shows a list of Eclipse open
			source projects and releases from the last year that are believed to be in good
			standing.

		</p>
<h2 id="projects">Projects</h2>
		<p>
			The projects listed below are considered to be in good standing and
			are in active development. A project that conforms to the Eclipse
			Foundation Development Process but is not in active development (e.g.,
			the code is stable) would be considered to be in good standing, but
			is none-the-less excluded here. To be included in this list, the
			corresponding Eclipse open source project must have engaged in either
			a release or progress review in the year preceding the release.</p>
		<ul>
<?php
$sql = <<<EOQ
select
	distinct p.id,
	p.name,
	p.url,
	max(rev.date) as lastReviewDate
from Project as p
	join ProjectReviews as rev on p.id=rev.project
where
	rev.type in ('release','graduation', 'progress', 'other')
	and rev.status = 'success'
group by p.id
having lastReviewDate >= date_sub(now(), interval 1 year)
order by p.name;
EOQ;

query('dashboard', $sql, array (), function ($row) {
	echo "<li><a href=\"{$row['url']}\">{$row['name']}</a></li>";
});
?>
</ul>

<h2 id="releases">Releases</h2>
		<p>
			Grayed out releases are have future dates (these releases have been
			projected into the future by the corresponding project team); only
			the releases scheduled in the next month are included in this block.
		</p>

		<p>
			Note that service releases that are based on releases in good standing,
			but are themselves dated more than one year after a review are
			absent from this list.
		</p>

		<p>
			If you believe that your project's release should be listed here,
			first confirm that your project engaged in a review within the 12
			months leading up to the release date, and if you're satisfied that your
			release is in good standing, contact emo@eclipse-foundation.org for
			assistance (or to initiate a review).
		</p>

		<ul>
<?php
$sql = <<<EOQ
select
	p.id,
	concat(p.name, ' ', rel.name) as name,
	rel.url as url,
	rel.date as releaseDate,
	max(rev.date) as lastReviewDate
from Project as p
	join ProjectReleases as rel on p.id=rel.project
	join ProjectReviews as rev on p.id=rev.project
where
	rev.type in ('release','graduation', 'progress', 'other')
	and rev.status = 'success'
	and 
		rel.date between date_sub(now(), interval 1 year) and date_add(now(), interval 1 month)
group by p.id, rel.name
having lastReviewDate >= date_sub(releaseDate, interval 1 year)
order by releaseDate desc;
EOQ;

query('dashboard', $sql, array (), function ($row) {
	$class = strtotime($row['releaseDate']) > time() ? 'future' : 'past';
	echo "<li class=\"{$class}\"> {$row['releaseDate']} <a href=\"{$row['url']}\">{$row['name']}</a></li>";
});

?>
		</ul>

</div>
</div>

<?php
$html = ob_get_contents ();
ob_end_clean ();
$App->generatePage ( $theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html );
?>
