<?php
/*******************************************************************************
 * Copyright (c) 2010,2017 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *******************************************************************************/
require_once ($_SERVER ['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");
require_once ($_SERVER ['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php");
require_once ($_SERVER ['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php");
$App = new App ();
$Nav = new Nav ();
$Menu = new Menu ();
include ($App->getProjectCommon ());

$pageTitle = "Eclipse Project Tools";
$pageKeywords = "";
$pageAuthor = "Wayne Beaton";

ob_start ();
?>
<div id="maincontent">
	<div id="midcolumn">
		<h1><?= $pageTitle ?></h1>
		<p>This area contains some tools for Eclipse committers. Note that you
			have to be logged in as a committer (using your Eclipse Foundation Account)
			in order to use these tools.</p>
	</div>
</div>

<?php
$html = ob_get_contents ();
ob_end_clean ();
$App->generatePage ( $theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
