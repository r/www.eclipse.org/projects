<?php
/*******************************************************************************
 * Copyright (c) 2017 Eclipse Foundation and others.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
/*******************************************************************************
 * Copyright (c) 2017 Eclipse Foundation and others.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

require_once dirname(__FILE__) . '/../legal.inc';
require_once dirname(__FILE__) . '/../../classes/Project.class.php';
require_once dirname(__FILE__) . '/../../classes/License.class.inc';
require_once dirname(__FILE__) . '/../../classes/common.php';
require_once dirname(__FILE__) . '/../../classes/debug.php';

require_once(dirname(__FILE__) . "/../../../eclipse.org-common/system/app.class.php");
$App = new App();

callIfNotCommitter(function() {
	header('Location: /project');
	exit;
});

header("Content-Type: text/plain");

if ($id = @$_GET['id']) {
 $project = Project::getProject($id);
}

if (isset($project)) {
 echo renderContributingFile($id);
}
?>