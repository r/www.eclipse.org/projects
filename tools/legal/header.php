<?php
/*******************************************************************************
 * Copyright (c) 2017 Eclipse Foundation and others.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

header("Content-Type: text/plain");

require_once(dirname(__FILE__) . "/../../../eclipse.org-common/system/app.class.php");
require_once dirname(__FILE__) . '/../legal.inc';
require_once dirname(__FILE__) . '/../../classes/Project.class.php';
require_once dirname(__FILE__) . '/../../classes/License.class.inc';
require_once dirname(__FILE__) . '/../../classes/common.php';
require_once dirname(__FILE__) . '/../../classes/debug.php';

$App = new App();

if (!$width = (int)@$_GET['width']) $width = 80;
if ($list = @$_GET['licenses']) {
 $licenses = License::parseLicenses($list);
} elseif ($id = @$_GET['id']) {
 if ($project = Project::getProject($id)) {
  $licenses = License::getLicensesForProject($project->getId());
 }
}

if (!$licenses) {
	echo "No licenses specified or available.";
} else {
	echo getDefaultFileHeader($licenses, $width);
}