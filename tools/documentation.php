<?php
/*******************************************************************************
 * Copyright (c) 2016, 2017 Eclipse Foundation and others.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
require_once (dirname ( __FILE__ ) . "/../../eclipse.org-common/system/app.class.php");
require_once (dirname ( __FILE__ ) . "/../../eclipse.org-common/system/nav.class.php");
require_once (dirname ( __FILE__ ) . "/../../eclipse.org-common/system/menu.class.php");
require_once dirname(__FILE__) . "/../classes/Project.class.php";
require_once dirname(__FILE__) . "/../classes/database.inc";
require_once dirname(__FILE__) . "/../classes/common.php";
require_once dirname(__FILE__) . "/../classes/debug.php";
$App = new App ();
$Nav = new Nav ();
$Menu = new Menu ();
include ($App->getProjectCommon ());

callIfNotCommitter(function() {
	header('Location: /projects');
	exit;
});

require_once dirname ( __FILE__ ) . '/legal.inc';

$pageTitle = "Legal Documentation Generator";
$pageKeywords = "";
$pageAuthor = "Wayne Beaton";

$project = null;
if ($id = @$_GET ['id'])
	if (isValidProjectId ( $id ))
		$project = Project::getProject ( $id );

ob_start ();
function renderTemplate($function) {
	echo "<pre style=\"background: lightgrey\">";
	$function ();
	echo "</pre>";
}

/**
 * This function renders a project selection drop-down.
 * When the
 * user selects a value, the page is reloaded with the id of the
 * selected project in the <code>id</code> parameter. All other
 * parameters that had been passed to the page are preserved via
 * hidden fields.
 *
 * @param string $id
 *        	The id of the selected project
 */
function dumpProjectSelectionForm($id) {
	echo "<form method=\"get\">";
	foreach ( $_GET as $key => $value ) {
		if ($key == 'id')
			continue;
		echo "<input type=\"hidden\" name=\"$key\" value=\"$value\"";
	}
	echo "<p>Select a project: ";
	echo "<select name=\"id\" onchange=\"this.form.submit()\">";

	foreach ( Project::getActiveProjects () as $project ) {
		$indent = str_repeat ( '&nbsp;', 4 * (count ( explode ( '.', $project->getId () ) ) - 1) );
		$selected = $id == $project->getId () ? ' selected="true"' : '';
		echo "<option value=\"{$project->getId()}\"$selected>{$indent}{$project->getFormalName()}</option>";
	}

	echo "</select>";
	echo "<input type=\"submit\" value=\"Go!\"/>";
	echo "</p>";
	echo "</form>";
}

function dumpProjectDocumentStatusTable($id) {

	$sql = "
		select
			gr.project, gr.path, pm.type, pm.file
		from GitRepo as gr
			left join ProjectMetadata as pm on gr.path=pm.path
		where gr.project=':id:'
		order by gr.path";
	$args = array(':id:' => $id);
	$repositories = array();
	query('dashboard', $sql, $args, function($row) use (&$repositories) {
		$repositories[$row['path']][$row['type']] = $row['file'];
	});

	$types = array('README','CONDUCT','CONTRIBUTING','LICENSE','NOTICES','SECURITY');

	echo "<div class=\"metadata\">";
	echo "<table><tbody>";

	echo "<tr><th>Repository</th>";
	foreach($types as $type) {
		echo "<th>{$type}</th>";
	}
	echo "</tr>";
	foreach($repositories as $path => $data) {
		if (preg_match('/([^\/]+)$/', $path, $matches)) {
			$name = $matches[1];
		}
		echo "<tr><td><a href=\"{$path}\">{$name}</a></td>";
		foreach($types as $type) {
			$status = @$data[$type] ? "<span class=\"provided\">&check;</span>" : "<span class=\"not-provided\">&#x2715;</span>";
			echo "<td>{$status}</td>";
		}
		echo "</tr>";
	}
	echo "</tbody></table>";
	echo "</div>";
}

function usesEPL10(Project $project) {
	foreach(License::getLicensesForProject($project->getId()) as $license) {
		if ($license->getSPDXCode() == 'EPL-1.0') return true;
	}
	return false;
}

?>
<style>
.metadata table {
  width:100%;
}

.metadata td, .metadata th {
  border: 1px solid #ddd;
  padding: 8px;
  white-space: nowrap;
}

.metadata tr:nth-child(even){background-color: #f2f2f2;}

.metadata th {
  padding-top: 12px;
  padding-bottom: 12px;
  text-align: left;
  background-color: #040404;
  color: white;
}

.metadata tbody tr td:first-child {
  text-overflow: clip;
  width: 20%;
  min-width: 20%;
  max-width: 20%;
}

.metadata span.provided {
	color: green;
}

.metadata span.not-provided {
	color: red;
}

pre {
	background: lightgrey;
}

</style>
<div id="maincontent">
	<div id="midcolumn">
		<h1><?= $pageTitle ?></h1>
		<p>
			<strong>EXPERIMENTAL!</strong>
		</p>
		<p>
			This page provides automatically generated template content for legal
			documentation, including copyright and license headers, copyright
			assignments for notices, and more. The information here is generated
			from information found in our databases. <strong>Some information may be
			missing; it is ultimately the project team's responsibility to
			provide this information.</strong> As we evolve this <em>experimental</em>
			implementation, we will identify gaps and improve the quality of the
			output.
		</p>
		<p>
			There is more information in the
			<a href="https://www.eclipse.org/projects/handbook/#legaldoc">Eclipse
			Project Handbook</a>.
		</p>

		<?php dumpProjectSelectionForm($id); ?>

		<?php
		if ($project) {
		?>
			<h2><?php echo $project->getFormalName(); ?></h2>

			<p>
				The Eclipse Foundation scans project Git repositories to
				harvest various metadata. These scans look for the existence of the
				various sorts of documentation that are required for governance purposes
				or are strongly recommended to help adopters and contributors acquaint
				themselves and otherwise be successful with the project.
			</p>
			<p>
				Repositories are scanned <em>nightly</em>. Changes to repositories
				may take as long as a day to be reflected here.
			</p>
			<p>
				This feature is <strong>experimental</strong> and under development.
				Please report any issues that you discover here by opening an issue
				on <a href="https://gitlab.eclipse.org/eclipsefdn/emo-team/emo/-/issues">EMO GitLab</a>.
			</p>

			<?php dumpProjectDocumentStatusTable($project->getId()); ?>

			<h3 id="copyright">Copyright and License Header</h3>
			<p>
				Where possible, all source code, property files, and metadata files
				(including application, test, and generated source code as well as
				other types of files such as XML, HTML, etc.) must include a header
				with appropriate <a
					href="/projects/handbook/#ip-copyright-headers">copyright
					and license notices</a>.
			</p>

		<?php
		if (!empty(License::getLicensesForProject($project->getId()))) {
		?>

			<?php
			if (usesEPL10($project)) {
			?>
				<blockquote>
				<strong><em>NOTICE!</em> This Eclipse project lists the EPL-1.0 as a project license.</strong>
				The EPL-1.0 has been deprecated: in order to reduce license proliferation,
				please migrate to the new EPL-2.0 (EPL-2.0 headers are shown here).
				For more information, please see the <a href="/legal/epl-2.0/faq.php">EPL-2.0 FAQ</a>.
				Contact the <a href="mailto:emo@eclipse-foundation.org">EMO</a> if you have
				any questions.
				</blockquote>
			<?php
			}
			?>

			<?php
			$licenses = array_map(
					function($license) {
						return $license->getSPDXCode() == 'EPL-1.0' ? License::getLicense('EPL-2.0') : $license;
					}, License::getLicensesForProject($project->getId()));
			?>

			<p>The following copyright and license header should be a reasonable
				default for project files based on the information available in the
				Eclipse Foundation database.</p>


			<p>A <a href="legal/header.php?id=<?php echo $id?>">Plain Text Version</a>
				is available.</p>

			<?php
				renderTemplate ( function () use (&$project, &$licenses) {
					echo getDefaultFileHeader ( $licenses );
				} );
			?>

			<p>Alternatively, the project team may choose to adopt this more
				generic copyright header for each file and include the identities of
				the copyright holders in the notice file.</p>

			<?php
				renderTemplate ( function () use (&$project, &$licenses) {
					echo getAlternativeFileHeader ( $licenses );
				} );
			?>

		<?php
		} else {
		?>

			<p><strong>License information for this project is not available. Please check with your
			Project Lead, PMC, or the EMO for assistance.</strong></p>

		<?php
		}
		?>

			<h3 id="conduct">
				<a class="link" href="#conduct">CODE_OF_CONDUCT File</a>
			</h3>

			<p>
				Use the standard <a
					href="http://www.eclipse.org/org/documents/Community_Code_of_Conduct.php">Eclipse
					Foundation Community Code of Conduct</a> in <a
					href="https://raw.githubusercontent.com/eclipse/.github/master/CODE_OF_CONDUCT.md">Markdown
					format</a>.

			<p>
				The file should be named
				<code>CODE_OF_CONDUCT</code>
				and may have an extension; it should be placed in the root of all
				project repositories.
			</p>

			<h3 id="notice">
				<a class="link" href="#notice">NOTICE File</a>
			</h3>

			<p>
				<strong>This is being phased out.</strong> For more information, see <a
				href="https://gitlab.eclipse.org/eclipsefdn/helpdesk/-/issues/1194">Revise
				and update the Eclipse Foundation IP Policy and Due Diligence
				Process</a>.
			</p>

			<p>The <a href="/projects/handbook/#legaldoc-notice">NOTICE</a>
				file must include basic project metadata, an expression of the
				declared project licenses, information regarding the licensing of any
				third party content, and a statement regarding the use of
				cryptography in cases where the content employs cryptography.</p>

			<p>The file should be named <code>NOTICE</code> and may have an extension;
				it should be placed in the root of all project repositories.</p>

			<?php
				renderTemplate ( function () use (&$id) {
					renderNoticeFile ( $id );
				} );
			?>

			<h3 id="license">
				<a class="link" href="#license">LICENSE File</a>
			</h3>

			<p>
				The <a href="https://www.eclipse.org/projects/handbook/#legaldoc-license">LICENSE</a>  file contains the exact text of the project's declared licenses in human readable form.

			</p>
			<p>
				We don't have a generator for this yet. For most projects,
				however, this file should contain the text of the
				Eclipse Public License.
			</p>

			<ul>
				<li><a href="https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.txt">Eclipse Public License 2.0</a>
				<li><a href="https://www.eclipse.org/org/documents/epl-1.0/EPL-1.0.txt">Eclipse Public License 1.0</a></li>
			</ul>

			<p>The LICENSE file may have an extension, and should be placed in the root of all project
				repositories.</p>

			<h3 id="contributing">
				<a href="#contributing">CONTRIBUTING File</a>
			</h3>

			<p>The CONTRIBUTING file describes the steps by which a developer
				can contribute to the project. This includes information like
				pointers to the Eclipse Contributor Agreement (ECA), coding
				guidelines, build instructions, etc.</p>

			<p>The CONTRIBUTING file may have an extension, and should be placed in the root of all project
				repositories.</p>

			<p>
				This file is available in both <a
					href="legal/contributing.php?id=<?php echo $id?>">Markdown</a> and <a
					href="legal/contributing.php?id=<?php echo $id?>&asciidoc">AsciiDoc</a>
				format. Note that you must be logged in for this link to work.
			</p>

		<?php
				renderTemplate ( function () use (&$id) {
					renderContributingFile ( $id );
				} );
			?>

			<h3 id="security">
				<a href="#security">SECURITY File</a>
			</h3>

			<p>The SECURITY file describes products and product versions that for
				which security updates may be applied and the means by which vulnerabilities
				are reported.</p>

			<p>The Eclipse Security team provides the following
				<a href="https://github.com/eclipse-csi/security-handbook/blob/main/templates/SECURITY.md?plain=1">template</a>.
				<strong>You must to modify it with information that corresponds to your project.</strong>
				Note that it is acceptable to describe your supported versions rather
				than list them explicitly (that is, you can say, for example, that &quot;two
				most recent major or minor release versions are supported&quot; or similar).</p>

			<p>The SECURITY file may have an extension, and should be placed in the root of all project
				repositories.</p>

			<?php
				renderTemplate ( function () use (&$id) {
					renderSecurityFile ( $id );
				} );
			?>

		<?php
		}
		?>
	</div>
</div>
<?php

$html = ob_get_contents ();
ob_end_clean ();
$App->generatePage ( $theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html );
?>
