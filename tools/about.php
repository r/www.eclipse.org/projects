<?php
$parameters = '';
$separator = '?';
foreach ($_GET as $key => $value) {
	$parameters .= $separator . $key . '=' . $value;
	$separator = '&';
}
header("Location: ./documentation.php{$parameters}");