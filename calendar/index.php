<?php
/*******************************************************************************
 * Copyright (C) Eclipse Foundation, Inc. and others.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
require_once dirname ( __FILE__ ) . "/../classes/debug.php";
require_once ($_SERVER ['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");
require_once ($_SERVER ['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php");
require_once ($_SERVER ['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php");

$App = new App ();
$Nav = new Nav ();
$Menu = new Menu ();

$pageKeywords = "";
$pageTitle = "Eclipse Foundation Projects Calendar";
$pageAuthor = "Wayne Beaton";

include ($App->getProjectCommon ());
ob_start ();
?>
<link rel="stylesheet" href="/projects/handbook/resources/handbook.css" />

<div id="maincontent">
	<h1><?php echo $pageTitle; ?></h1>
	<p>
		The Eclipse Foundation Projects Team schedules events to discuss the <a
			href="../dev_process">Eclipse Foundation Development Process</a>, the
		<a href="https://www.eclipse.org/org/documents/Eclipse_IP_Policy.pdf">Eclipse
			Foundation Intellectual Property Policy</a> Policy and <a
			href="https://www.eclipse.org/projects/handbook/#ip">Eclipse
			Foundation Due Diligence Process</a>, and other related topics.
	</p>

	<p>
		Upcoming events (all times are UTC) <a
			href="https://calendar.google.com/calendar/ical/c_cfe894f8291f1dd1ee8214c5f7109af549ca7cd3e7df16f6ce69485aae97fced%40group.calendar.google.com/public/basic.ics">[iCal]</a>:
	</p>

	<iframe
		src="https://calendar.google.com/calendar/embed?height=300&wkst=1&bgcolor=%23ffffff&ctz=Etc%2FGMT&mode=AGENDA&showNav=1&showTitle=0&showDate=1&showTabs=0&showPrint=0&showCalendars=0&src=Y19jZmU4OTRmODI5MWYxZGQxZWU4MjE0YzVmNzEwOWFmNTQ5Y2E3Y2QzZTdkZjE2ZjZjZTY5NDg1YWFlOTdmY2VkQGdyb3VwLmNhbGVuZGFyLmdvb2dsZS5jb20&color=%23B39DDB"
		style="border: solid 1px #777" width="800" height="300"
		frameborder="0" scrolling="no"></iframe>


	<h2 id="office-hours">Office Hours Recordings</h2>

	<p>The EMO hosts a monthly call to discuss the Eclipse Foundation
		Development Process, changes to the Eclipse IP Policy and Due
		Diligence Process, and other related topics. All Eclipse open source
		project committers are invited to join. The format is flexible: we
		generally start with a very short presentation followed by a question
		and answer session. Bring your questions and the Eclipse Management
		Organization team will try to provide answers.</p>

	<p>We record only the formal presentation part of our "Office Hours"
		sessions.</p>

	<h3 id="2024-11-14">
		<a href="#2024-11-14">IT Update</a>
	</h3>
	<p>Recorded on November 14, 2024</p>
	<p>Denis Roy, the Eclipse Foundation's IT Director, delivered a brief
		presentation on the latest IT updates, followed by a Q&A session.</p>

	<iframe width="560" height="315"
		src="https://www.youtube.com/embed/BtDmDKUOzJw?si=Vsmd5BqxYxT6qSXW"
		title="YouTube video player" frameborder="0"
		allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
		referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

	<h3 id="2024-10-10">
		<a href="#2024-10-10">Marketing Service for Eclipse Open Source Projects</a>
	</h3>
	<p>Recorded on October 10, 2024</p>
	<p>In this session, we discuss marketing services offered to Eclipse
		open source projects.</p>

	<iframe width="560" height="315"
		src="https://www.youtube.com/embed/7gqfCEV4LcI?si=NhkGJPwa_RTdtl-n"
		title="YouTube video player" frameborder="0"
		allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
		referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

	<h3 id="2024-09-12">
		<a href="#2024-09-12">Vulnerability Reporting and Security for Eclipse Projects</a>
	</h3>
	<p>Recorded on September 12, 2024</p>
	<p>In this session, guest speaker Marta Rybczynska from the Eclipse
		Security Team started with a refresher on the vulnerability reporting
		and handling process from the committer's perspective. Then she
		reviewed take-aways from the new CNA rules covering common situations,
		including how you determine whether or not a specific bug is a
		vulnerability.</p>

	<iframe width="560" height="315"
		src="https://www.youtube.com/embed/7Z3WQJMQGIo?si=GPcIH8tfc14tigJI"
		title="YouTube video player" frameborder="0"
		allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
		referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

	<h3 id="2024-07-11">
		<a href="#2024-07-11">Code Signing and GitHub Configuration Self-Service</a>
	</h3>
	<p>Recorded on July 11, 2024</p>
	<p>For this session, we had two topics from the Security Team.

	<p>Recent changes to the code signing services, specifically for JAR
		signing and Windows Authenticode, have led to performance issues in CI
		builds. Let's explore strategies to mitigate these issues and outline
		our remediation plan.</p>
	<p>The Eclipse Foundation will soon enable GitHub configuration
		self-service (also known as Eclipse OtterDog) for all projects with
		sources on GitHub. We will explain what will happen and be available
		to answer your questions.</p>

	<iframe width="560" height="315"
		src="https://www.youtube.com/embed/QB61cDf8b1w?si=M5LBElDXDCjFxtFl"
		title="YouTube video player" frameborder="0"
		allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
		referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

	<h3 id="2024-04-11">
		<a href="#2024-04-11">Generative AI Usage Guidelines</a>
	</h3>
	<p>Recorded on April 11, 2024</p>
	<p>In this session, we presented the Eclipse Foundation's Generative
		Artificial Intelligence Usage Guidelines for Eclipse Committers. There
		is some discussion about copyright in the context of GPT technologies,
		but the primary focus is the guidelines themselves. Bear in mind that
		Wayne is not a lawyer, and nothing that we present in this session
		should be considered legal advice.</p>

	<iframe width="560" height="315"
		src="https://www.youtube.com/embed/CmJIPs3XZyA?si=pkqRpLHi334Z6_Mk"
		title="YouTube video player" frameborder="0"
		allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
		referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

	<h4>Errata:</h4>
	<p>
		In the discussion that followed the presentation, we concluded that
		including a citation in the copyright and license header was
		incorrect. Further, it was also incorrect to specify a license, even
		one that expresses <em>public domain</em>, as doing so would be
		inconsistent with the prevailing opinion that AI-generated content
		cannot be copyrighted and licensed.
	</p>
	<p>
		The slide should have appeared as such:<br /> <img
			style="max-width: 50%; border: 1px solid"
			src="images/2024-04-11-errata-1.png" />
	</p>
	<h4>Links:</h4>
	<ul>
		<li><a href="https://www.eclipse.org/projects/guidelines/genai">Generative
				Artificial Intelligence Usage Guidelines for Eclipse Committers</a></li>
		<li><a
			href="https://gitlab.eclipse.org/eclipsefdn/it/webdev/hugo-eclipsefdn-website-boilerplate">Hugo
				Website template for your project website</a></li>
	</ul>

	<h3 id="2024-03-14">
		<a href="#2024-03-14">Frequently Asked Questions</a>
	</h3>
	<p>Recorded on March 14, 2024</p>

	<p>During this session, we tackled some frequently asked questions,
		including discussion of merit for committer elections, various roles
		that are (and are not) part of the Eclipse Foundation Development
		Process, and more.</p>

	<iframe width="560" height="315"
		src="https://www.youtube.com/embed/am1-XvcRIcE?si=JKM8nI2jYDOpcb5N"
		title="YouTube video player" frameborder="0"
		allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
		referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

	<h4>Notes</h4>
	<ul>
		<li><a href="https://www.eclipse.org/projects/handbook/#elections-committer">Committer Elections</a></li>
		<li><a href="https://www.eclipse.org/projects/handbook/#roles">Project Roles</a></li>
	</ul>

	<h3 id="2024-02-08">
		<a href="#2024-02-08">IP Lab</a>
	</h3>
	<p>Recorded on February 8, 2024</p>

	<p>IPLab is what we call the combination of the GitLab repository that
		we've set up for committers to use to engage in intellectual property
		due diligence review and the automated processes that support the IP
		due diligence process. In this session, we focus on manual creation of
		reviews, which is often required when the review of project content is
		required (the Eclipse Dash License Tool automates most of the heavy
		lifting for third party content).</p>

	<iframe width="560" height="315"
		src="https://www.youtube.com/embed/cGCs0puewiM?si=4NljkB1mUcHuKsnY"
		title="YouTube video player" frameborder="0"
		allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
		allowfullscreen></iframe>

	<h4>Notes</h4>
	<ul>
		<li><a href="https://github.com/eclipse/dash-licenses">The Eclipse
				Dash License Tool</a></li>
		<li><a href="https://gitlab.eclipse.org/eclipsefdn/emo-team/iplab">Manually
				create IP review requests via IPLab</a></li>
	</ul>

	<h3 id="2023-12-15">
		<a href="#2023-12-15">December 2023 Updates</a>
	</h3>
	<p>Recorded on November 9, 2023</p>

	<p>Our topic this month was a general update in which we touched
		briefly on multiple topics including project metadata, our IP due
		diligence process, the Eclipse Dash License Tool, IPLab, SBOMs,
		security and more.</p>

	<iframe width="560" height="315"
		src="https://www.youtube.com/embed/aHtO5M16jzE?si=SSuwKHbgdKHLhKqB"
		title="YouTube video player" frameborder="0"
		allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
		allowfullscreen></iframe>

	<h3 id="2023-11-09">
		<a href="#2023-11-09">Intellectual Property Due Diligence</a>
	</h3>
	<p>Recorded on November 9, 2023</p>

	<p>During this week's (online) office hours, we spent a few minutes
		reminding committers of the services that we make available for
		Eclipse committers to help reduce the burden of intellectual property
		management. Specifically, we discussed the processes and tools that we
		have in place to help with due diligence review of project code and
		the third party content that leverage. Naturally, this discussion
		covered some usage scenarios of the Eclipse Dash License Tool and
		IPLab. We also spoke a little bit about SBOMs.</p>

	<iframe width="560" height="315"
		src="https://www.youtube.com/embed/pKGzgTflHYg?si=_rliUdi7aebHkCIe"
		title="YouTube video player" frameborder="0"
		allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
		allowfullscreen></iframe>

	<h4>Notes</h4>
	<ul>
		<li>Open Source distributions in a cloud-native world: from a
			technical to a legal point of view<a
			href="https://www.eclipsecon.org/2023/sessions/open-source-distributions-cloud-native-world-technical-legal-point-view">
				[EclipseCon 2023]</a> <a
			href="https://www.youtube.com/watch?v=EVg9aP_toG4">[recording]</a>
		</li>
		<li><a href="https://github.com/eclipse/dash-licenses">The Eclipse
				Dash License Tool</a></li>
		<li><a href="https://gitlab.eclipse.org/eclipsefdn/emo-team/iplab">Manually
				create IP review requests via IPLab</a></li>
		<li><a
			href="https://gitlab.eclipse.org/eclipsefdn/emo-team/sbom/-/blob/main/docs/sbom.adoc">SBOM
				Best Practices</a></li>
		<li>Wayne is not a lawyer</li>
	</ul>

	<h3 id="2023-10-12">
		<a href="#2023-10-12">SBOMs and Project Metadata</a>
	</h3>
	<p>Recorded on October 12, 2023</p>

	<p>Generating SBOMs directly as part of your build, and (at least in
		the case of Maven) sharing them to the software repository is
		relatively straightforward. To really leverage the the tools to
		generate SBOMs, however, we need your help to tighten up the metadata
		captured in your build scripts (e.g., capture license information as
		SPDX expressions in your pom.xml file) and update your builds to
		generate the SBOMs.</p>

	<iframe width="560" height="315"
		src="https://www.youtube.com/embed/VeGfHzfdep4?si=AKOLkjIY7PCvX4eU"
		title="YouTube video player" frameborder="0"
		allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
		allowfullscreen></iframe>

	<h4>Notes</h4>
	<ul>
		<li><a
			href="https://gitlab.eclipse.org/eclipsefdn/emo-team/sbom/-/blob/main/docs/sbom.adoc">SBOM
				Best Practices</a></li>
	</ul>

	<h3 id="2023-09-14">
		<a href="#2023-09-14">Eclipse Otterdog</a>
	</h3>
	<p>Recorded on September 14, 2023</p>

	<p>Thomas Neidhart from the Eclipse Foundation's Security Team will
		present Otterdog: a tool to manage GitHub organizations at scale using
		a configuration as code approach.</p>

	<iframe width="560" height="315"
		src="https://www.youtube.com/embed/VnzF57EQaDI?si=hZYPAZHzAXoFDxkp"
		title="YouTube video player" frameborder="0"
		allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
		allowfullscreen></iframe>
	<h4>Notes:</h4>

	<ul>
		<li><a href="https://gitlab.eclipse.org/eclipsefdn/security/otterdog">Otterdog
				on GitLab</li>
		<li><a
			href="https://gitlab.eclipse.org/eclipsefdn/helpdesk/-/issues/new">Request
				assistance with Otterdog</a>

	</ul>

	<h3 id="2023-08-10">
		<a href="#2023-08-10">2023 Committer Survey Hightlights</a>
	</h3>
	<p>Recorded on August 10, 2023</p>

	<p>Maria Teresa provides an overview of what we learned from our recent
		(2023) committer survey.</p>

	<iframe width="560" height="315"
		src="https://www.youtube.com/embed/G02xs9PiOBs"
		title="YouTube video player" frameborder="0"
		allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
		allowfullscreen></iframe>

	<h3 id="2023-06-08">
		<a href="#2023-06-08">Progress Reviews</a>
	</h3>
	<p>Recorded on June 8, 2023</p>

	<p>Progress reviews are a fundamental bit of governance that Eclipse
		open source project teams engage in periodically (generally annually).
		By engaging in a progress review, an open source project team can push
		out official releases for a full year. For long-time Eclipse
		Committers... progress reviews are fundamentally the same as release
		reviews (the primary difference being that release reviews tend to be
		aligned with a specific release). Note that specification projects are
		required to engage in release reviews for every release.</p>
	<p>The EMO doesn't tend to think of reviews as pass/fail events.
		Rather, these reviews are an opportunity for the EMO and PMC to make
		sure that Eclipse project teams understand their responsibilities
		under the Eclipse Foundation Development Process and the Eclipse IP
		Due Diligence Process, and are generally engaging in vendor neutral
		open, transparent, and meritocratic practices to attract contribution
		and participation.</p>

	<iframe width="560" height="315"
		src="https://www.youtube.com/embed/kytojCkw97A"
		title="YouTube video player" frameborder="0"
		allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
		allowfullscreen></iframe>

	<h4>Notes:</h4>

	<p>
		One thing that we forgot to mention in the talk is how to actually
		initiate a progress review. To initiate a progress review, send a note
		to <a href="mailto:emo@eclipse-foundation.org">EMO</a> and the team
		will lead you through the process. Note that the EMO does periodically
		initiate progress reviews on behalf of a project.
	</p>

	<ul>
		<li><a
			href="https://www.eclipse.org/projects/handbook/#progress-review">Progress
				Reviews</a> in the Eclipse Foundation Project Handbook;</li>
		<li><a href="https://www.eclipse.org/projects/tools/documentation.php">Documentation
				Generator</a> (accessible by committers only);</li>
		<li><a href="https://github.com/eclipse/dash-licenses">The Eclipse
				Dash License Tool</a>;</li>
		<li><a
			href="https://gitlab.eclipse.org/eclipsefdn/emo-team/emo/-/issues/46">Generating
				SBOM for Eclipse Projects</a></li>
	</ul>

	<h3 id="2023-05-11">
		<a href="#2023-05-11">May 2023 Update</a>
	</h3>
	<p>Recorded on May 11, 2023</p>

	<p>In ths session, we delivered status updates on various efforts that
		we've been engaged in, including: progress reviews; the Eclipse Dash
		License Tool; and generating SBOMs for Eclipse Projects.</p>

	<iframe width="560" height="315"
		src="https://www.youtube.com/embed/uTodfmOog70"
		title="YouTube video player" frameborder="0"
		allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
		allowfullscreen></iframe>

	<h4>Notes:</h4>

	<ul>
		<li><a
			href="https://www.eclipse.org/projects/handbook/#progress-review">Progress
				Reviews</a>;</li>
		<li><a href="https://github.com/eclipse/dash-licenses">The Eclipse
				Dash License Tool</a>;</li>
		<li><a
			href="https://gitlab.eclipse.org/eclipsefdn/emo-team/emo/-/issues/46">Generating
				SBOM for Eclipse Projects</a></li>
	</ul>

	<h3 id="2023-04-24">
		<a href="#2023-04-24">Someone Reports a Security Issue in my Project!
			Now What?</a>
	</h3>
	<p>Recorded on April 24, 2023</p>

	<p>
		<em>Special Extra!</em> This session was recorded as part of the
		Virtual Eclipse Community Meetups.
	</p>

	<p>All projects have bugs. Some of them have a security impact and can
		be used to cause harm. We call them vulnerabilities. Because of the
		possible impact of security issues, we handle them differently. This
		talk will guide the audience through the Eclipse Foundation processes
		of reporting and managing vulnerabilities with new tooling. As a
		bonus, Marta will show resources for your project, like a SECURITY.md
		template.</p>

	<iframe width="560" height="315"
		src="https://www.youtube.com/embed/SDm477kS_g0?si=9HFh5CnX15ufFjWL"
		title="YouTube video player" frameborder="0"
		allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
		allowfullscreen></iframe>

	<h4>Notes:</h4>
	<ul>
		<li><a href="https://www.eclipse.org/projects/handbook/#vulnerability">Managing
				and Reporting Vulnerabilities</a></li>
	</ul>

	<h3 id="2023-04-13">
		<a href="#2023-04-13">AI and Contribution</a>
	</h3>
	<p>Recorded on April 13, 2023</p>

	<p>This presentation is intended to start a conversation about what our
		policy/position should be with regard to how we deal with
		contributions of content generated by an AI.</p>

	<p>Note that our current policy is that we do not accept contributions
		generated by an AI as doing so is--at least--in conflict with the
		ECA/DCO (note that an AI cannot sign the ECA).</p>

	<iframe width="560" height="315"
		src="https://www.youtube.com/embed/NgeRDAuzVXw"
		title="YouTube video player" frameborder="0"
		allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
		allowfullscreen></iframe>

	<h3 id="2023-03-07">
		<a href="#2023-03-07">SBOMs</a>
	</h3>
	<p>Recorded on March 7, 2023</p>

	<p>A short discussion of some of the work that we're doing for Eclipse
		projects regarding the generation of SBOMs and some of the
		technologies that we're exploring to assist with their generation.</p>

	<iframe width="560" height="315"
		src="https://www.youtube.com/embed/iVOOcR1vHWo"
		title="YouTube video player" frameborder="0"
		allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
		allowfullscreen></iframe>

	<h3 id="2023-02-09">
		<a href="#2023-02-09">Security @ The Eclipse Foundation</a>
	</h3>
	<p>Recorded on February 9, 2023</p>

	<iframe width="560" height="315"
		src="https://www.youtube.com/embed/6Udk9aK4nDc"
		title="YouTube video player" frameborder="0"
		allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
		allowfullscreen></iframe>

	<h3 id="2023-01-13">
		<a href="#2023-01-13">The Eclipse Dash License Tool</a>
	</h3>
	<p>Recorded on January 13, 2023</p>

	<iframe width="560" height="315"
		src="https://www.youtube.com/embed/xDodKRtkNVw"
		title="YouTube video player" frameborder="0"
		allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
		allowfullscreen></iframe>

	<h4>Notes:</h4>
	<ul>
		<li><a href="https://github.com/eclipse/dash-licenses">The Eclipse
				Dash License Tool on GitHub</a></li>
		<li><a
			href="https://www.eclipse.org/projects/handbook/#ip-license-tool">Eclipse
				Dash License Tool in the Handbook</a></li>
		<li><a href="https://gitlab.eclipse.org/eclipsefdn/emo-team/iplab">IP
				Lab on Eclipse GitLab</a></li>
	</ul>

	<h3 id="2022-12-08">
		<a href="#2022-12-08">Eclipse Foundation Intellectual Property Policy
			Updates</a>
	</h3>
	<p>Recorded on December 8, 2022</p>

	<iframe width="560" height="315"
		src="https://www.youtube.com/embed/O0n9aExZ1n0"
		title="YouTube video player" frameborder="0"
		allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
		allowfullscreen></iframe>
</div>

<?php
$html = ob_get_contents ();
ob_end_clean ();
$App->generatePage ( $Theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html );
?>