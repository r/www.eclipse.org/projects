<?php
/**
 * Copyright (c) 2005 Eclipse Foundation and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

/**
 * Generate a header for a proposal.
 *
 * @deprecated
 * @param string $the_name The project name
 */
function generate_header($the_name) {
	global $App;
	$App->setOutdated();
}
?>
