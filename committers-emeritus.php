<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'
/* 221934 - this page to remain on eclipse.org */

$pageTitle 		= "Eclipse Committers Emeritus";
$pageKeywords   = '';
$pageAuthor     = '';

include( '_commonLeftNav.php' );

ob_start();
?>
<div id="maincontent">
<div id="midcolumn">

<h1>Eclipse Committers Emeritus</h1>
<p>
When Committers who have made significant contributions to Eclipse
become inactive and thus no longer Committers<sup>1</sup>, we want to make sure that we acknowledge their
huge contributions to our (collective) success. We choose to make that acknowledgement
by appointing them to <em>Committer Emeritus</em> status.
<p>
Our heartfelt thanks to each of these people and their
significant contributions to the long term success
of all of Eclipse.
<p>
<span style="font-color: grey">
A list of all former Committers can be found at <a href="committers-alumni.php">Committers Alumni</a>.
</span>
<div style="border: 3px solid #D4A017; padding-top: 5px; padding-bottom: 5px">
<ul>
<?php
$sql = "SELECT People.PersonID AS PID, FName, LName, ProjectID FROM People, PeopleProjects
		WHERE People.PersonID = PeopleProjects.PersonID
		  AND PeopleProjects.Relation = 'CE'
		  AND PeopleProjects.InactiveDate IS NULL
		  ORDER BY LName, FName
		  ";
$result = $App->foundation_sql($sql);
$showing = -1;
while( $row = mysqli_fetch_assoc($result) ) {
	if( $showing == $row['PID'] ) {
		?>, <?= $row['ProjectID'] ?><?php
	} else {
		if( $showing == -1 ) {
			//first row, nothing to close
		} else {
			?>)</li><?php
		}
		?><li><b><?= $row['FName'] ?> <?= $row['LName'] ?></b> (<?= $row['ProjectID'] ?><?php
		$showing = $row['PID'];
	}
}
if( $showing != -1 ) {
	?>)</li></ul>
	<?php
}
?>
</div>

<br/>
<div style="font-size: small">
<b>Project leads</b> may nominate people to this page as part of the process of
removing a committer by sending a request to the <a href="mailto:emo@eclipse-foundation.org">EMO</a>.
</div>

<hr style="margin-top: 5px; margin-bottom: 5px;">
<div style="font-size: 60%"><sup>1</sup>
Being a Committer is a privilege that is earned by
contributing actively, and showing discipline and good judgment. It is a responsibility
that is not given or treated lightly. At the same
time, we acknowledge that active participation in Eclipse Projects
is not a lifetime commitment, and thus Committers may/will become
inactive for a variety of reasons.
</div>
</div><!-- midcolumn -->
</div><!-- maincontent -->
<?php
	# Paste your HTML content between the EOHTML markers!
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
