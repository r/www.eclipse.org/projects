<?php
/*******************************************************************************
 * Copyright (c) 2010, 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Wayne Beaton (Eclipse Foundation)- initial API and implementation
 *******************************************************************************/

/*
 * This script generates an RSS feed of recent Eclipse review/proposal
 * activity. By default, it shows the last 90 days of activity. This value
 * can be tuned by providing the 'age' get parameter. For example, to obtain
 * information for the past thirty-day, call this script as follows:
 *
 * http://www.eclipse.org/projects/reviews-rss.php?age=30
 *
 * Known issues: Links may not be encoded correctly on all URLs.
 */

//header("Content-Type: application/rss+xml");
require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/projects/classes/Review.class.inc");
require_once($_SERVER['DOCUMENT_ROOT'] . "/projects/classes/debug.php");
$App = new App();

$age = 30;
if (isset($_GET['age'])) {
	$value = $_GET['age'];
	if (is_numeric($value) && $value > 0) $age = $value;
}

?>
<rss version="2.0">
<channel>
<title>Eclipse Reviews</title>
<link>
http://www.eclipse.org/projects/whatsnew.php
</link>
<description>
Upcoming reviews of Projects and
Proposals at the Eclipse Foundation.
</description>
<language>
en-us
</language>
<pubDate>
<?= date('r', strtotime("now")) ?>
</pubDate>
<docs>
http://blogs.law.harvard.edu/tech/rss
</docs>
<managingEditor>
emo@eclipse.org
</managingEditor>
<webMaster>
webmaster@eclipse.org
</webMaster>
<?php

Review::get(strtotime("-${age} days"), function($activity) {
	if ($activity->isPending()) return;
	$title = htmlentities($activity->getName());
	$link = $activity->getUrl();
	$date = date('r', $activity->getDate());
	$guid = sha1($link);

	echo "<item>\n";
	echo "\t<title>$title</title>\n";
	echo "\t<link>$link</link>\n";
	echo "\t<pubDate>$date</pubDate>\n";
	echo "\t<guid isPermaLink=\"false\">$guid</guid>\n";
	echo "</item>\n";

});
?>
</channel>
</rss>
