<?php
/*******************************************************************************
 * Copyright (C) 2024 Eclipse Foundation, Inc. and others.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
require_once ($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");
require_once ($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php");
require_once ($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php");
$App = new App();
$Nav = new Nav();
$Menu = new Menu();

$pageKeywords = "";
$pageTitle = "Generative Artificial Intelligence Usage Guidelines for Eclipse Committers";
$pageAuthor = "Wayne Beaton";

include($App->getProjectCommon());

ob_start();
?>
<link rel="stylesheet" href="/projects/handbook/resources/handbook.css"/>

<div id="maincontent">
	<?php include "../../content/en_genai.html"; ?>
</div>

<?php
$html = ob_get_contents();
ob_end_clean();
$App->generatePage(null, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html );
?>