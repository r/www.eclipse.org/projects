<?php
/**
 * Copyright (c) 2016, 2022 Eclipse Foundation and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Christopher Guindon (Eclipse Foundation) - Initial implementation
 *   Eric Poirier (Eclipse Foundation)
 *
 * SPDX-License-Identifier: EPL-2.0
 */

require_once ($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php");

$theme = NULL;

$Nav = new Nav();
$Nav->setType("collapse");

$Nav->addNavSeparator("Committers", "/projects/");
$Nav->addCustomNav("Committer Training", "/projects/training", "_self", 1);
$Nav->addCustomNav("Project Handbook", "/projects/handbook", "_self", 1);
$Nav->addCustomNav("Office Hours", "/projects/calendar#office-hours", "_self", 1);

$Nav->addNavSeparator("Projects", "/projects/");
$Nav->addCustomNav("Projects Home", "/projects", "_self", 1);
$Nav->addCustomNav("Development Process", "/projects/dev_process", "_self", 1);
$Nav->addCustomNav("Activity", "/projects/project_activity.php", "_self", 1);
//$Nav->addCustomNav("Proposals", "/projects/tools/proposals.php", "_self", 1);
//$Nav->addCustomNav("Reviews", "/projects/tools/reviews.php", "_self", 1);
//$Nav->addCustomNav("Releases", "/projects/tools/goodstanding-releases.php", "_self", 1);
$Nav->addCustomNav("Search Projects", "https://projects.eclipse.org", "_self", 1);

$Nav->addNavSeparator("Specifications", null);
//$Nav->addCustomNav("Eclipse Specifications", null, "_self", 1);
$Nav->addCustomNav("Specifications Home", "/specifications/", "_self", 1);
$Nav->addCustomNav("Specification Process", "/projects/efsp", "_self", 1);
$Nav->addCustomNav("Operations Guide", "/projects/efsp/operations.php", "_self", 1);
$Nav->addCustomNav("IP Flows", "/projects/efsp/ip-flows.php", "_self", 1);

$Nav->addNavSeparator("Legal", null);
$Nav->addCustomNav("Legal Home", "/legal/", "_self", 1);
$Nav->addCustomNav("Intellectual Property Policy", "/org/documents/Eclipse_IP_Policy.pdf", "_self", 1);
$Nav->addCustomNav("Contributor Agreement", "/legal/ECA.php", "_self", 1);
$Nav->addCustomNav("EPL-2.0", "/legal/epl-2.0", "_self", 1);

$Nav->addNavSeparator("Collaborations", null);
$Nav->addCustomNav("Industry Collaborations Home", "/collaborations", "_self", 1);
$Nav->addCustomNav("Working Group Process", "/org/workinggroups/industry_wg_process.php", "_self", 1);
$Nav->addCustomNav("Operations Guide", "/org/workinggroups/operations.php", "_self", 1);
