#/*******************************************************************************
# * Copyright (c) 2010, 2015 Eclipse Foundation and others.
# * All rights reserved. This program and the accompanying materials
# * are made available under the terms of the Eclipse Public License v1.0
# * which accompanies this distribution, and is available at
# * http://www.eclipse.org/legal/epl-v10.html
# *
# * Contributors:
# *    Wayne Beaton (Eclipse Foundation)- initial API and implementation
# *******************************************************************************/
# This script generates a diff between the 2014 and 2015 versions of the development process
#

wget 'https://bugs.eclipse.org/bugs/buglist.cgi?f1=blocked&o1=substring&query_format=advanced&v1=463857&ctype=csv&human=1' -O bugs.csv

wget http://local.eclipse.org/projects/dev_process/development_process_2014/index.php -O - | tr '\n' ' ' | perl -pe 's|^.*<div id="midcolumn">(.*)<!\-\- midcolumn \-\->.*$|\1|' | perl -pe 's|<div class="comment">.*?</div>||g' | perl -pe 's|<div class="postit">.*?</div>||g' | perl -pe 's|</?[abi][^>]*>||g' | tidy -wrap 72 --tidy-mark false | perl -pe 's|<| <|g' > /tmp/2014.html
wget http://local.eclipse.org/projects/dev_process/development_process_2015/index.php -O - | tr '\n' ' ' | perl -pe 's|^.*<div id="midcolumn">(.*)<!\-\- midcolumn \-\->.*$|\1|' | perl -pe 's|<div class="comment">.*?</div>||g' | perl -pe 's|<div class="postit">.*?</div>||g' | perl -pe 's|</?a[^>]*>||g' | tidy -wrap 72 --tidy-mark false | perl -pe 's|<| <|g' > /tmp/2015.html

# Generate a PDF
htmldoc --book --no-title --no-toc -t pdf11 --webpage - < /tmp/2015.html > development_process_2015.pdf

wdiff -i -w "<strike>" -x "</strike>" -y "<font color=\"green\">" -z "</font>" /tmp/2014.html /tmp/2015.html > diff.html

# PDF generation of the diff document fails in odd ways; generate by printing from the browser
# htmldoc --book --no-title --no-toc -t pdf11 --webpage - < diff.html > diff.pdf
