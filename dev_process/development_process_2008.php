<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'
/* 221934 - this page to remain on eclipse.org */

$pageTitle 		= "Eclipse Development Process";
$next = "/projects/dev_process/development_process_2010.php";

include( '../_commonLeftNav.php' );

ob_start();
?>
<style>
p {
margin-bottom:10px;
}
blockquote {
margin-left:2em;
margin-right:2em;
margin-bottom:10px;
}
sup {
font-size:9px;
vertical-align:top;
}
.postit {
float: right;
width:150px;
margin: 5px;
padding: 2px;
border: 1px dashed black;
background-color: #FFFFDD;
}
#toc {
	padding: 5px;
}
#toc ul {
	list-style-type: none;
	list-style-image: none;
	margin-top: 1px;
	margin-bottom: 1px;
	padding-top: 1px;
	padding-bottom: 1px;
	text-align: left;
}
#toc li{
	margin-top: 1px;
	margin-bottom: 1px;
	padding-top: 0;
}
</style>
<div id="maincontent">
<div id="midcolumn">

<h2>Eclipse Development Process</h2>
<div class="homeitem3col">
<h3>Contents</h3>
<div id="toc">
<ul>
<li><a href="#1_Purpose">1 Purpose</a></li>
<li><a href="#2_Principles">2 Principles</a>
<ul>
<li><a href="#2_1_Open_Source_Rules_of_Engagement">2.1 Open Source Rules of Engagement</a></li>
<li><a href="#2_2_Eclipse_Ecosystem">2.2 Eclipse Ecosystem</a></li>
<li><a href="#2_3_Three_Communities">2.3 Three Communities</a></li>
<li><a href="#2_4_Clear_Concise_and_Evolving">2.4 Clear, Concise, and Evolving</a></li>
</ul></li>
<li><a href="#3_Requirements">3 Requirements</a>
<ul>
<li><a href="#3_1_Requirements_and_Guidelines">3.1 Requirements and Guidelines</a></li>
</ul></li>
<li><a href="#4_Structure_and_Organization">4 Project Structure and Organization</a>
<ul>
<li><a href="#4_1_Committers">4.1 Committers</a></li>
<li><a href="#4_2_Code_and_Releases">4.2 Code and Releases</a></li>
<li><a href="#4_3_IP_Records">4.3 IP Records</a></li>
<li><a href="#4_4_Community_Awareness">4.4 Community Awareness</a></li>
<li><a href="#4_5_Scope">4.5 Scope</a></li>
<li><a href="#4_6_Leaders">4.6 Leaders</a></li>
<li><a href="#4_7_Committers_and_Contributors">4.7 Committers and Contributors</a></li>
<li><a href="#4_8_Councils">4.8 Councils</a></li>
<li><a href="#4_9_Incubators">4.9 Incubator Projects</a></li>
</ul></li>
<li><a href="#5_Roadmap_Process">5 Roadmap Process</a></li>
<li><a href="#6_Development_Process">6 Development Process</a>
<ul>
<li><a href="#6_1_Mentors">6.1 Mentors</a></li>
<li><a href="#6_2_Project_Lifecycle">6.2 Project Lifecycle</a></li>
<li><a href="#6_2_1_Pre-Proposal">6.2.1 Pre-proposal</a></li>
<li><a href="#6_2_2_Proposal">6.2.2 Proposal</a></li>
<li><a href="#6_2_3_Incubation">6.2.3 Incubation</a></li>
<li><a href="#6_2_4_Mature">6.2.4 Mature</a></li>
<li><a href="#6_2_5_Top-Level">6.2.5 Top-Level</a></li>
<li><a href="#6_2_6_Archived">6.2.6 Archived </a></li>
<li><a href="#6_3_Reviews">6.3 Reviews</a>
<ul>
<li><a href="#6_3_1_Creation_Review">6.3.1 Creation Review</a></li>
<li><a href="#6_3_2_Graduation_Review">6.3.2 Graduation Review</a></li>
<li><a href="#6_3_3_Release_Review">6.3.3 Release Review</a></li>
<li><a href="#6_3_4_Promotion_Review">6.3.4 Promotion Review</a></li>
<li><a href="#6_3_5_Continuation_Review">6.3.5 Continuation Review</a></li>
<li><a href="#6_3_6_Termination_Review">6.3.6 Termination Review</a></li>
<li><a href="#6_3_7_Move_Review">6.3.7 Move Review</a></li>
<li><a href="#6_3_8_Restructuring_Review">6.3.8 Restructuring Review</a></li>
<li><a href="#6_3_9_Combining_Reviews">6.3.9 Combining Reviews</a></li>
</ul></li>
<li><a href="#6_4_Releases">6.4 Releases</a></li>
<li><a href="#6_5_Grievance_Handling">6.5 Grievance Handling</a></li>
</ul></li>
<li><a href="#7_Precedence">7 Precedence</a></li>
<li><a href="#8_Revisions">8 Revisions</a>
<ul>
<li><a href="#8_1_Revision">8.1 Revision 2.4</a></li>
</ul></li>
</ul>
</div>
</div>

<h2><a name="1_Purpose"></a>1. Purpose</h2>
This document describes the Development Process for the Eclipse Foundation. In
particular, it describes how the Membership at Large, the Board of Directors, other
constituents of the Ecosystem, and the Eclipse Management Organization (EMO) lead,
influence, and collaborate with Eclipse Projects to achieve these Eclipse purposes:

<blockquote><em>The Eclipse technology is a vendor-neutral, open development platform supplying
frameworks and exemplary, extensible tools (the 'Eclipse Platform').  Eclipse Platform tools
are exemplary in that they verify the utility of the Eclipse frameworks, illustrate the
appropriate use of those frameworks, and support the development and maintenance of
the Eclipse Platform itself; Eclipse Platform tools are extensible in that their
functionality is accessible via documented programmatic interfaces.  The purpose
of Eclipse Foundation Inc., is to advance the creation,
evolution, promotion, and support of the Eclipse Platform and to cultivate both an
open source community and an ecosystem of complementary products, capabilities, and services.</em></blockquote>
<p>
<div class="postit"><table><tr><td width="250px">Explanatory comments, guidelines,
and checklists - as well as additional requirements added by the EMO per <a href="#3_Requirements">section 3</a> - are noted
in yellow boxes.</td>
</tr></table></div>This document has five sections:
<ul>
<li><a href="#2_Principles"><em>Principles</em></a> outlines the basic principles upon which the development process is based.
<li><a href="#3_Requirements"><em>Requirements</em></a> describes the requirements that the Eclipse community has for its development process.
<li><a href="#4_Structure_and_Organization"><em>Structure and Organization</em></a> specifies the structure and organization of the projects and project community at Eclipse.
<li><a href="#5_Roadmap_Process"><em>Roadmap Process</em></a> describes the manner by which the EMO will work with the projects to create the annual Eclipse Roadmap.
<li><a href="#6_Development_Process"><em>Development Process</em></a> outlines the lifecycle and processes required of all Eclipse projects.
</ul>
<h2><a name="2_Principles"></a>2. Principles</h2>
The following describe the guiding principles used in developing this Development Process.

<h3><a name="2_1_Open_Source_Rules_of_Engagement"></a>2.1 Open Source Rules of Engagement</h3>
<ul>
<li> <b>Open</b> - Eclipse is open to all; Eclipse provides the same opportunity
to all. Everyone participates with the same rules; there are no rules to exclude
any potential contributors which include, of course, direct competitors in the marketplace.
<li> <b>Transparent</b> - Project discussions, minutes, deliberations, project plans,
plans for new features, and other artifacts are open, public, and easily accessible.
<li> <b>Meritocracy</b> - Eclipse is a meritocracy. The more you contribute the more
responsibility you will earn. Leadership roles in Eclipse are also merit-based and
earned by peer acclaim.
</ul>

<h3><a name="2_2_Eclipse_Ecosystem"></a>2.2 Eclipse Ecosystem</h3>
Eclipse as a brand is the sum of its parts (all of the Projects), and Projects
should strive for the highest possible quality in extensible frameworks,
exemplary tools, transparent processes, and project openness.
<p>
The Eclipse Foundation has the responsibility
to <em>...cultivate...an ecosystem of complementary products, capabilities, and services...</em>.
It is therefore a key
principle that the Eclipse Development Process ensures that the projects are managed for the
benefit of both the open source community and the ecosystem members.
To this end, all Eclipse projects are required to:
<ul>
<li> communicate their project plans and plans for new features (major and minor) in a timely, open and transparent manner;
<li> create platform quality frameworks capable of supporting the building of commercial grade products on top of them;
<li> ship extensible, exemplary tools which help enable a broad community of users; and
<li> participate in the annual Roadmap process to ensure maximum transparency and bi-directional communication with the ecosystem.
</ul>

<h3><a name="2_3_Three_Communities"></a>2.3 Three Communities</h3>
Essential to the Purposes of the Eclipse Foundation is the development of three
inter-related communities around each Project:
<ul>
<li> <b>Contributors</b> and <b>Committers</b> - a thriving, diverse and active community
of developers is the key component of any Eclipse Project. Ideally, this
community should be an open, transparent, inclusive, and diverse community
of Committers and (non-Committer) Contributors. Attracting new Contributors and
Committers to an open source project is time consuming and requires active
recruiting, not just passive "openness". The Project Leadership must make reasonable
efforts to encourage and nurture promising new Contributors.
<ul>
<li> Projects must have the diversity goals to ensure diversity of thought and avoiding
relying on any one company or organization. At the same time, we acknowledge
that enforcing a particular diversity metric is a poor way to achieve these goals;
rather we expect the project leadership to help the diversity evolve organically.
<li> Diversity is a means to an end, not an end in itself, thus diversity goals
will differ by project based on the other accomplishments of the project(s).
<li> Project are required to explain their diversity efforts and accomplishments during Reviews.
</ul>
<li> <b>Users</b> - an active and engaged user community is proof-positive that
the Project's exemplary tools are useful and needed. Furthermore, a large
user community is one of the key factors in creating a viable
ecosystem around an Eclipse project, thus encouraging additional open source and
commercial organizations to participate. Like all good things, a user community
takes time and effort to bring to fruition, but once established is
typically self-sustaining.
<li> <b>Adopters</b> - an active and engaged adopter/plug-in developer community
is the
only way to prove that an Eclipse project is providing extensible frameworks
and extensible tools accessible via documented APIs. Reuse of the frameworks
within the companies that are contributing to the project is necessary, but
not sufficient to demonstrate an adopter community. Again, creating, encouraging,
and nurturing an adopter community outside of the Project's developers takes time,
energy, and creativity by the Project Leadership, but is essential
to the Project's long-term open source success.
</ul>
The Eclipse community considers the absence of any one or more of these
communities as proof that the Project is not sufficiently open, transparent,
and inviting, and/or that it has emphasized tools at the expense of
extensible frameworks or vice versa.

<h3><a name="2_4_Clear_Concise_and_Evolving"></a>2.4 Clear, Concise, and Evolving</h3>
It is an explicit goal of the Development Process to be as clear and concise
as possible so as to help the Project teams navigate the complexities,
avoid the pitfalls, and become successful as quickly as possible.
<p>
This document imposes requirements and constraints on the operation of the
Projects, and it does so on behalf of the larger Eclipse community. It is
an explicit goal of the Development Process to provide as much freedom
and autonomy to the Projects as possible while ensuring the collective
qualities benefit the entire Eclipse community.
<p>
Similarly, this document should not place undue constraints on the EMO or the Board
that prevent them from governing the process as necessary.  We cannot foresee all
circumstances and as such should be cautious of being overly prescriptive and/or
requiring certain fixed metrics.
<p>
The frameworks, tools, projects, processes, community, and even the
definition of Quality continues to, and will continue to, evolve. Creating rules
or processes that force a static snapshot of any of these is detrimental to the
health, growth, and ecosystem impact of Eclipse.
<p>
Part of the strength of this document is in what it does not say, and
thus opens for community definition through convention, guidelines, and public consultation.
A document with too much structure becomes too rigid and prevents the kind of
innovation and change we desire for Eclipse. In areas where this document is
vague, we expect the Projects and Members to engage the community-at-large
to clarify the current norms and expectations.

<h2><a name="3_Requirements"></a>3. Requirements</h2>
This document and any additional criteria as established by the EMO contain requirements, recommendations, and suggestions.
<p>
<span style="margin-right:6px; margin-top:5px; float:left; color:ivory; background:#FF9999;
border:1px solid #444; font-size:30px; line-height:25px; padding-top:2px;
padding-left:2px; padding-right:2px; font-family:times; ">R</span><b>Required</b> - Certain responsibilities and behaviors
are required of participants in Eclipse open source projects. Projects that fail to
perform the required behaviors will be terminated by the EMO. In keeping with the
Guiding Principles, the number of requirements must be kept to an absolute minimum.
<p>
<span style="margin-right:6px; margin-top:5px; float:left; color:ivory; background:#00CC99;
border:1px solid #444; font-size:30px; line-height:25px; padding-top:2px; padding-left:2px;
padding-right:2px; font-family:times; ">G</span><b>Guideline</b> - Other responsibilities
and behaviors are recommended best practices. Collectively, we have learned that Projects
are more likely to be successful if the team members and leaders follow these recommendations.
Projects are strongly encouraged to follow these recommendations, but will not be penalized by
this Process if they do not.

<h3><a name="3_1_Requirements_and_Guidelines"></a>3.1 Requirements and Guidelines</h3>
This document is entirely composed of requirements. In addition to the requirements specified
in this Development Process, the EMO is instructed to clarify, expand, and extend this Process
by creating a set of Eclipse Project Development Guidelines to advance the creation, evolution,
promotion, and support of the Eclipse Platform and to cultivate both an open source community
and an ecosystem of complementary products and services.
<p>
The EMO is not permitted to override or ignore the requirements listed in this document
without the express written endorsement of the Board of Directors.

<h2><a name="4_Structure_and_Organization"></a>4. Structure and Organization</h2>
The Eclipse Projects are organized hierarchically.
The top of the hierarchy are the set of <b>Top Level Projects</b>.
Each Top Level Project contains one or more <b>Sub-Projects</b>.
Each Sub-Project contains zero or more Sub-Projects.
The term <b>Project</b> refers to either a Top-Level Project or a Sub-Project.
Projects may be referred to as Sub-Projects or Components, but the choice of common name does not change the characteristics of the Project.
<p>
Projects with no child Projects are <b>Operating Projects</b>.
Projects with one or more child Projects are <b>Container Projects</b>.
The <b>descendants</b> of a Project are the Project itself and transitive closure of its child Projects.
The <b>top parent</b> of a Project is the Top-Level Project at the top of the hierarchy.
<p>
Projects are the unit entity for:
<ul>
<li>Committers</li>
<li>Code and Releases</li>
<li>IP Records</li>
<li>Community Awareness</li>
</ul>
<p>
As defined by the <a href="/org/documents/eclipse_foundation-bylaws.pdf#page=19">Eclipse Bylaws - Article VII</a>,
the <b>Eclipse Management Organization (EMO)</b> consists of the Foundation staff and the Councils. The
term <b>EMO(ED)</b>, when discussing an approval process, refers to the subset of the EMO
consisting of the Executive Director and whomever he or she may delegate that specific approval authority to.

<h3><a name="4_1_Committers"></a>4.1 Committers</h3>
An Operating Project has a self-managing set of Committers.
The Committers of an Operating Project have the exclusive right to elect new Committers to
their Project -- no other group, including a parent Project, can force a Project to accept a new Committer.
<p>
The set of Committers of a Container Project is the union of all the Committers of the child Projects.
<h3><a name="4_2_Code_and_Releases"></a>4.2 Code and Releases</h3>
Each Operating Project owns and maintains a collection of source code and/or web pages.
<p>
Each Operating Project is the finest grained unit of infrastructure supplied by the Eclipse Foundation.
Each Operating Project has a single Unix group of its Committers that provides write-access to the Project's files.
Each Operating Project has a single bugzilla component for its bugs.
... The exact infrastructure provided by the Eclipse Foundation varies over time
and is defined outside this process document.
<p>
While Operating Projects are the finest grained unit of infrastructure, there are no constraints on
Projects self governing themselves with finer-grained divisions on labor. For example, if Project A
wants to divide its code-based into two modules, A1 and A2, and have separate groups of its Committers
work on each module, that's perfectly acceptable. However, if Project A wants to have fine-grained
access control for those two groups (i.e., separate unix groups), then Project A will need to become
a Container Project and create two new Sub-Projects, A.A1 and A.A2, as Operating Projects. That division
will require a Creation+Move Review.
<p>
Container Projects do not have file infrastructure: no Unix group and no repository.
<p>
Any Project in the Mature Phase may make a <b>Release</b>.
A Project in the Incubation Phase with two Mentors may make a <b>Release</b>.
A Release may include the code from any subset of the Project's descendants.
However, if any code is included from an Operating Project, all the code
from that Project must be included.
In other words, an Operating Project is the level of granularity of code.

<h3><a name="4_3_IP_Records"></a>4.3 IP Records</h3>
A Project at any level may receive IP clearance for contributions and third-party libraries.
IP approval will often include the same approval for all descendant Projects.
However, IP clearance will only be granted at the most appropriate technical
level thus only Operating Projects should request IP clearance for contributions
and Container Projects may request IP clearance for third-party libraries only if
a majority of their descendants need that library.

<h3><a name="4_4_Community_Awareness"></a>4.4 Community Awareness</h3>
Projects are the level of communication with the larger Eclipse community and eco-system.
Projects may either have their own communications (website, mailing lists, newsgroups, etc)
or they may be part of a parent Project's communications (website, mailing list, newsgroups, etc).
In either case, the Project is required to maintain an open and public communication channel with
the Eclipse community including, but not limited to, project plans, schedules, design discussions, and so on.
<p>
All Projects must make the communication channels easy to find. Container Projects are further required to
make the separate communication channels of their child Projects (if any) easy to find.
<p>
Any Project in the Incubation Phase must correctly identify its website and Releases.
A Container Project with at least one descendant Project in Incubation Phase must correctly annotate
its own website so as to notify the Eclipse community that incubating Projects exist in its hierarchy.
Any Release containing code from an Incubation Phase project must be correctly labeled,
i.e., the Incubation Phase is viral and expands to cover all Releases in which it is included.
<p>
<h3><a name="4_5_Scope"></a>4.5 Scope</h3>
Each Top-Level Project has a <b>Charter</b> which describes the purpose, <b>Scope</b>,
and operational rules for the Top-Level Project. The Charter should refer to, and describe
any refinements to, the provisions of this Development Process. The Board approves the
Charter of each Top-Level Project.
<p>
Sub-Projects do not have separate Charters; Sub-Projects operate under the Charter of their parent Top-Level Project.
<p>
All Projects have a defined <b>Scope</b> and all initiatives within that Project are
required to reside within that Scope. Initiatives and code that is found to be
outside the Scope of a Project may result in the termination of the Project.
The Scope of Top-Level Projects is part of the Charter, as approved by the Board
of Directors of the Eclipse Foundation.
<p>
The Scope of a Sub-Project
is defined by the initial project proposal
as reviewed and approved by the <b>Project Management Committee (PMC)</b> (as further
defined below) of the Project's
Project's top parent
and by the EMO. A Project's Scope
must be a subset of
its parent's Scope.

<h3><a name="4_6_Leaders"></a>4.6 Leaders</h3>
<p>Top-Level Projects are managed by a <b>Project Management Committee (PMC)</b>. Sub-Projects
are managed by one or more <b>Project Leaders</b>.
The PMC Lead(s) of a Top-Level Project are the Project Leader(s)
of that project.</p>

<p>
PMC Leads are approved by the Board; PMC members and Project Leads are
approved by the EMO(ED). The initial Project Leadership is appointed and
approved in the Creation Review. Subsequently, additional Project Leadership
(PMC members or Sub-Project
Leaders) must be
elected by the Project's Committers<sup>1</sup>
and the Board or EMO(ED) (for PMC members and Sub-Project Leads respectively).
In the unlikely
event that a member of the Project Leadership becomes disruptive to the process
or ceases to contribute for an extended period, the member may be removed by
(a) if there are at least two other Project Leaders, then unanimous vote of
the remaining Project Leadership; or (b) unanimous vote of the Project
Leadership of the parent Project.</p>
<p><sup>1</sup>Until such time as the Foundation portal supports Project Leader elections,
an election held on the Project's developer mailing list will suffice.</p>

<p>Each Project's leadership is required to:<ul>
<li> ensure that the Project is operating effectively by guiding the Project's
overall direction and by removing obstacles, solving problems, and resolving conflicts
<li> operate using open source rules of engagement: meritocracy, transparency,
and open participation.
<li> ensure that the projects and its sub-projects (if any)
conform to the Eclipse Foundation IP Policy and procedures.
</ul></p>
<p>
In exceptional situations,
such as Projects with zero active Committers or Projects with
disruptive Committers and no effective Project Leader(s),
the Project Leadership Chain has the authority to make changes (add, remove)
to the set of Committers and/or Project Lead(s) of that Project. </p>

<h3><a name="4_7_Committers_and_Contributors"></a>4.7 Committers and Contributors</h3>
Each Project has a <b>Development Team</b>, led by
the Project Leaders.
The Development Team is composed of
<b>Committers</b> and <b>Contributors</b>. <b>Contributors</b> are individuals who contribute code,
fixes, tests, documentation, or other work that is part of the Project.
<b>Committers</b> have write access to the source code repository(ies)
of the
Project and are expected to influence the Project's development.
<p>
<div class="postit"><table><tr><td width="100px">See <a href="new-committer.php">guidelines and
checklists</a> for electing a new committer.</td>
</tr></table></div>Contributors who have the trust of the Project's Committers can, through election,
be promoted Committer for that Project. The breadth of a Committer's influence
corresponds to the breadth of their contribution. A Development Team's Contributors
and Committers may (and should) come from a diverse set of organizations. A Committer
has write access to the source code repository for the Project and/or website and/or
bug tracking system. A Committer gains voting rights allowing them to affect the
future of the Project. Becoming a Committer is a privilege that is earned by
contributing and showing discipline and good judgment. It is a responsibility
that should be neither given nor taken lightly, nor is it a right based on
employment by an Eclipse Member company or any company employing existing
committers.
<p>
The election process for Committers uses the open and transparent portal election system.
The election process begins with an existing Committer on the same Project
nominating the Contribtor. The Project's Committers will vote for a period of no
less than one week of standard business days.
If there are at least three (3) positive votes and no negative votes within the
voting period, the Contributor is recommended to the root project's PMC
for commit privileges. If there are three (3) or fewer Committers on the Project,
a unanimous positive vote of all Committers is substituted.
If the PMC approves, and the Contributor signs the appropriate Committer legal agreements
established by the EMO (wherein, at the very least, the Developer
agrees to abide by the Eclipse Intellectual Property Policy), the Contributor
becomes a Committer and is given write access to the source code for that Project.
<p>
At times, Committers may become inactive for a variety of reasons.
The decision making process of the Project relies on active committers who respond
to discussions and vote in a constructive and timely manner.
The Project Leaders are responsible for ensuring the smooth operation of the Project.
A Committer who is disruptive, does not participate actively, or has been inactive
for an extended period may have his or her commit status revoked by the Project Leaders.
(Unless otherwise specified, "an extended period" is defined as "no activity for more than
six months".)
<p>
Active participation in the user newsgroup and the appropriate developer mailing lists
is a responsibility of all Committers, and is critical to the success of the Project.
Committers are required to monitor and contribute to the user newsgroup.
<p>
Committers are required to monitor the mailing lists associated with the Project.
This is a condition of being granted commit rights to the Project.
It is mandatory because committers must participate in votes (which in some cases
require a certain minimum number of votes) and must respond to the mailing list in
a timely fashion in order to facilitate the smooth operation of the Project. When a
Committer is granted commit rights they will be added to the appropriate mailing lists.
A Committer must not be unsubscribed from a developer mailing list unless their
associated commit privileges are also revoked.
<p>
Committers are required to track, participate in, and vote on, relevant
discussions in their associated Projects and components. There are three
voting responses: +1 (yes), -1 (no, or veto), and 0 (abstain).
<p>
Committers are responsible for proactively reporting problems in the bug
tracking system, and annotating problem reports with status information,
explanations, clarifications, or requests for more information from the
submitter. Committers are responsible for updating problem reports when
they have done work related to the problem.
<p>
Committer, PMC Lead, Project Lead, and Council Representative(s) are roles;
an individual may take on more than one of these roles simultaneously.

<h3><a name="4_8_Councils"></a>4.8 Councils</h3>
The three Councils defined in Bylaws section VII are comprised of Strategic
members and PMC representatives. The three Councils help guide the Projects as follows:<ul>
<li>The <b>Requirements Council</b> is primarily responsible for the Eclipse Roadmap. There
will always be more requirements than there are resources to satisfy them, thus the
Requirements Council gathers, reviews, and categorizes all of these incoming
requirements - from the entire Eclipse ecosystem - and proposes a coherent
set of <b>Themes and Priorities</b>.
<li>The <b>Planning Council</b> is responsible for establishing a coordinated
Simultaneous Release (a.k.a, "the release train") that supports the Themes
and Priorities in the Roadmap. The Planning Council is responsible for
cross-project planning, architectural issues, user interface conflicts,
and all other coordination and integration issues. The Planning Council
discharges its responsibility via collaborative evaluation, prioritization,
and compromise.
<li><div class="postit"><table><tr><td width="100px">See <a href="architecture-council.php">guidelines and
checklists</a> for the Architecture Council.</td>
</tr></table></div>The <b>Architecture Council</b> is responsible for the development,
articulation, and maintenance of the Eclipse Platform Architecture and
ensuring the Principles of the Development Process through mentorship.
Membership in the Architecture Council is per the Bylaws through Strategic
Membership, PMCs, and by appointment. The Architecture Council will, at
least annually, recommend to the EMO(ED), Eclipse Members who have sufficient
experience, wisdom, and time to be appointed to the Architecture Council and
serve as Mentors. Election as a Mentor is a highly visible confirmation of
the Eclipse community's respect for the candidate's technical vision, good
judgement, software development skills, past and future contributions to
Eclipse. It is a role that should be neither given nor taken lightly.
Appointed members of the Architecture Council are appointed to
two
year renewable terms.
</ul>

<h2><a name="5_Roadmap_Process"></a>5. Roadmap Process</h2>
The Roadmap describes the collective Eclipse Projects future directions and consists of two parts:<ol>
<li><b>Themes and Priorities</b> from the Requirements Council
<li><b>Project Plans</b> from Projects
</ol>
The Roadmap must be consistent with the Purposes as described in Bylaws section 1.1.  It is
developed using the prescribed <a href="/org/councils/roadmap_v2_0/index.php#process">roadmap process</a>.
<p>
The Roadmap is prepared by the Councils and approved by the Board annually. A proposed Roadmap
or Roadmap update is disseminated to the Membership at Large for comment and feedback in advance
of its adoption. This dissemination and all discussion and debate around the Roadmap must be
held in an open and transparent public forum, such as mailing lists or newsgroups.
<p>
Prior to any Board vote to approve a Roadmap or Roadmap update, every Member has the right to
communicate concerns and objections to the Board.
<p>
The process of producing or updating the Roadmap is expected to be iterative. An initial
set of Themes and Priorities may be infeasible to implement in the desired timeframe; subsequent
consideration may reveal new implementation alternatives or critical requirements that alter
the team's perspective on priorities. The EMO orchestrates interaction among and within the
Councils to drive the Roadmap to convergence.
<p>
This Development Process, the EMO, the Councils, and the Projects all acknowledge that the
success of the Eclipse ecosystem is dependent on a balanced set of requirements and
implementations. A Roadmap that provides too large a burden on the Projects will be
rejected and ignored; similarly, a Roadmap that provides no predictable Project plans
will be unhelpful to the business and technical plans being created by the ecosystem.
A careful balance of demands and commitments is essential to the ongoing success of the
Eclipse Projects, frameworks, and ecosystem.
<p>
The Project Leadership is expected to ensure that their Project Plans are consistent
with the Roadmap, and that all plans, technical documents and reports are publicly
available. To meet this requirement, each Project is required
to create a transparently
available Project Plan in an EMO-defined file format
that meets the following criteria:<ol>
<li>Enumerates the areas of change in the frameworks and tools for each proposed Release
<li>Consistent with and categorized in terms of the themes and priorities of the Roadmap
<li>Identifies and accommodates cross-project dependencies
<li>Addresses requirements critical to the Ecosystem and/or the Membership at Large
<li>Advances the Project in functionality, quality, and performance
</ol>
A Project may incrementally revise their Project Plan to deliver additional tasks provided that:<ol>
<li>the approved Roadmap is not put in jeopardy; and
<li>the work is consistent with the Project Plan criteria (as described above)
</ol>
<h2><a name="6_Development_Process"></a>6. Development Process</h2>
All Eclipse Projects, and hence all Project Proposals, must be consistent with the Purposes and the then-current Roadmap.
<p>
Projects must work within their Scope. Projects that desire to expand beyond their
current Scope must seek an enlargement of their Scope using a public Review as described below.
<p>
All projects are required to report their status at least quarterly
using the <a href="/projects/dev_process/project-status-infrastructure.php">EMO defined status reporting procedures</a>.
<p>
Projects must provide advanced notification of upcoming features and frameworks via their Project Plan.
<p>
<h3><a name="6_1_Mentors"></a>6.1 Mentors</h3>
New Proposals that intend to do a Release
are required to have at least two <b>Mentors</b>. New Proposals
that will only Release code as part of a parent Project's Release are not required
to have Mentors. Mentors must
be members of the Architecture Council. The Mentors (including
name, affiliation, and current Eclipse projects/roles) must be listed in the Proposal.
Mentors are required to monitor and
advise the new Project during its Incubation Phase, but are released from that
duty
once the Project graduates to the Mature Phase.
<p>
The Mentors must attend the Creation and Graduation Reviews.

<h3><a name="6_2_Project_Lifecycle"></a>6.2 Project Lifecycle</h3>
<img src="/projects/images/Development-process-small.gif" align="right">
Projects go through six distinct phases. The transitions from phase to phase are open and transparent public reviews.

<h4><a name="6_2_1_Pre-Proposal"></a>6.2.1 Pre-proposal</h4>
<div class="postit"><table><tr><td width="100px">See <a href="pre-proposal-phase.php">guidelines and
checklists</a> about writing a proposal.</td>
</tr></table></div>An individual or group of individuals declares their interest in,
and rationale for, establishing a project. The EMO will assist such groups in
the preparation of a project Proposal. <ul>
<li>The Pre-proposal phase ends when the Proposal is published by EMO and announced to the membership by the EMO.
</ul>

<h4><a name="6_2_2_Proposal"></a>6.2.2 Proposal</h4>
<div class="postit"><table><tr><td width="100px">See <a href="proposal-phase.php">guidelines and
checklists</a> about gathering support for a proposal.</td>
</tr></table></div>The proposers, in conjunction with the destination PMC and
the community, collaborate in public to enhance, refine, and clarify
the proposal. Mentors (if necessary)
for the project must be identified during this phase.<ul>
<li>The Proposal phase ends with a Creation Review or a Termination Review.
</ul>

<h4><a name="6_2_3_Incubation"></a>6.2.3 Incubation</h4>
<div class="postit"><table><tr><td width="100px">See <a href="incubation-phase.php">guidelines and
checklists</a> about incubation.</td>
</tr></table></div>After the project has been created, the purpose of the incubation
phase is to establish a fully-functioning open-source project. In this context,
incubation is about developing the process, the community, and the technology.
Incubation is a phase rather than a place: new projects may be incubated under
any existing Project. <ul>
<li>The Incubation phase may continue with a Continuation Review or a Release Review.
<li>Top-Level Projects cannot be incubated and can only be created from one or more
existing Mature-phase Projects.
<li>The Incubation phase ends with a Graduation Review or a Termination Review.
</ul>
Many Eclipse Projects are proposed and initiated by individuals with extensive and
successful software development experience. This document attempts to define a process
that is sufficiently flexible to learn from all its participants. At the same time,
however, the Incubation phase is useful for new Projects to learn the community-defined
Eclipse-centric open source processes.
<p>
<div class="postit"><table><tr><td width="200px">See <a href="parallel-ip-process.php">guidelines and
checklists</a> for utilizing the Parallel IP process.</td>
</tr></table></div>Only projects that are properly identified as being in the incubation phase may use
the Parallel IP process to reduce IP clearance process for new contributions.

<h4><a name="6_2_4_Mature"></a>6.2.4 Mature</h4>
<div class="postit"><table><tr><td width="100px">See <a href="mature-phase.php">guidelines and
checklists</a> about the mature phase.</td>
</tr></table></div>The project team has demonstrated that they are an open-source
project with an open and transparent process; an actively involved and growing
community; and Eclipse Quality technology. The project is now a mature member of
the Eclipse Community. Major releases continue to go through Release Reviews.<ul>
<li>Mature phase projects have Releases through Release Reviews.
<li>A Mature Project may be promoted to a Top-Level Project through a Promotion Review.
<li>A Mature Project that does not participate in a Release
in given year may continue through a Continuation Review.
<li>Inactive Mature phase projects may be archived through a Termination Review.
</ul>

<h4><a name="6_2_5_Top-Level"></a>6.2.5 Top-Level</h4>
<div class="postit"><table><tr><td width="100px">See <a href="top-level-phase.php">guidelines and
checklists</a> about being a top-level project.</td>
</tr></table></div>
Projects that have demonstrated the characteristics of a Top-Level Project (e.g.,
consistent leadership in a technical area and the recruitment of a wider developer
community) can be promoted to Top-Level Project status. This promotion occurs through
a Promotion Review. Upon the successful completion of a Promotion Review, the EMO(ED)
may recommend that the project be promoted to the Board of Directors and ask that its
Charter be reviewed and approved.

<h4><a name="6_2_6_Archived"></a>6.2.6 Archived</h4>
<div class="postit"><table><tr><td width="100px">See <a href="archived-phase.php">guidelines and
checklists</a> for archiving projects.</td>
</tr></table></div>Projects that become inactive, either through dwindling resources or by reaching their natural
conclusion, are archived. Projects can reach their natural conclusion in a number
of ways: for example, a project might become so popular that it is absorbed into one of the
other major frameworks. Projects are moved to Archived status through a Termination Review.
<p>
If there is sufficient community interest in reactivating an Archived Project, the Project
will start again with Creation Review. As there must be good reasons to have moved a
Project to the Archives, the Creation Review provides a sufficiently high bar to prove that those
reasons are no longer valid.  It also ensures that the original or updated project goals
are still consistent with the Purposes and Roadmap.

<h3><a name="6_3_Reviews"></a>6.3 Reviews</h3>
The Eclipse Development Process is predicated on open and transparent behavior. All major
changes to Eclipse projects must be announced and reviewed by the membership-at-large. Major
changes include the Project Phase transitions as well as the introduction or exclusion of
significant new technology or capability. It is a clear requirement of this document that
members who are monitoring the appropriate media channels (e.g., mailing lists or RSS feeds)
not be surprised by the post-facto actions of the Projects.
<p>
All Projects are required to participate in
at least one Review per year.
<p>
For each <b>Review</b>, the project leadership makes a presentation to, and
receives feedback from, the Eclipse membership.
<p>
A Review is a fairly comprehensive process. Gathering the material for a Review and
preparing the presentation is a non-trivial effort, but the introspection offered by
this exercise is useful for the Project and results are very useful for the entire
Eclipse community. In addition, Reviews have a specific relationship to the requirements
of the <a href="/org/documents/Eclipse_IP_Policy.pdf">Eclipse IP Policy</a>.
<p>
All Reviews have the same general process:
<ol>
<li>Projects are responsible for initiating the appropriate reviews. However, if a Project
  does not do so and the EMO believes a Review is necessary, the EMO may initiate a Review on the Project's behalf.
  The Project Leader initiates a review through the portal.<sup>2</sup>
<li> A Review then continues with the Project's Leadership requesting that the EMO(ED) schedule the Review.
<li> No less than one week in advance of the Review conference call, and preferably
at least two weeks in advance, the Project leadership provides the EMO with
the archival presentation material. <ol>
<li> The presentation material always includes a summary slide presentation or document. The minimum
contents of the presentation are proscribed by the individual Review types.
<li> The presentation material must be available in a format that anyone in the Eclipse
membership can review. For example, Microsoft Powerpoint files are not an acceptable
single format - such files may be one of the formats, but not the only format. Similarly
for Apple Keynote files and Microsoft Word files. PDF and HTML are acceptable single formats.
<li> The presentation material must have a correct copyright statement
and license.
<li> The presentation material must be <em>archival quality</em>. This means that the materials
must be comprehensible and complete on their own without requiring explanation by a human
presenter, reference to a wiki, or to other non-archived
web pages.
</ol>
<li> The EMO announces the Review schedule and makes the presentation materials available to the membership-at-large.
</ol>
The criteria for the successful completion of each type of Review will be documented
in writing by the EMO in guidelines made available via the www.eclipse.org website.
Such guidelines will include, but are not limited to the following:<ol>
<li> Clear evidence that the project has vibrant committer, adopter and user communities
as appropriate for the type of Review.
<li> Reasonable diversity in its committer population as appropriate for the type of
Review. Diversity status must be provided not only as number of people/companies, but
also in terms of effort provided by those people/companies.
<li> Documented completion of all required due diligence under
the <a href="/org/documents/Eclipse_IP_Policy.pdf">Eclipse IP Policy</a>.
<li> For Continuation, Graduation and Release Reviews, the project
must have a current project plan, in the format specified by the EMO, available to the community.</li>
<li> Balanced progress in creating both frameworks and extensible, exemplary tools.
<li> Showcase the project's quality through project-team chosen metrics and
measures, e.g., coupling, cyclomatic complexity, test/code coverage,
documentation of extensions points, etc.
</ol>
<p><sup>2</sup>Until such time as the Foundation portal supports initiating Reviews,
email to the <a href="mailto:emo@eclipse.org">EMO</a> will suffice.</p>
<p>
The Review itself:<ol>
<li> The Review is
 open for no less than one week and usually no more than
 two weeks of generally accepted business days. This is the <b>Review period</b>.
<li> The Review begins with
the EMO's posting of the review materials at the start of the Review period, and
ends with either the end of the Review period or (see below) an optional conference call or other conference technology (e.g., web conferencing)
so long as the technology is available to all members and incurs no additional costs to
the attendees.
<li>
The proper functioning of the Eclipse Development Process is contingent on
the active participation of the Eclipse Members and Committers, especially in Reviews,
thus each Review has an EMO-designated discussion and feedback
communication channel: a newgroup, a mailing list, or some other public forum.
<li>If a Committer election is required for a Review (for example, for a Move Review), then it
is held simultaneously with the Review period. Thus the election and the Review will end at
the same time, allowing quick and efficient provisioning of the resulting Project.
<li>Simultaneously with the opening of the Review,
a date and time for the optional conference call is announced. The call date shall be
no less than the next day and no more than one week of standard business days after
the last day of the Review. (For example, if the Review runs from Wednesday the 4th through
Tuesday the 10th, the call may be previously scheduled for any time from Wednesday the 11th through
Wednesday the 18th.)<br>
The default is that the optional conference call not be held. However,
during the Review period, any Eclipse Member with voting rights may request, via the Review's public
communication channel, that the conference call be held. If any such requests exist at
the end of the Review period,
the conference call is held at its previously scheduled date and time.
<li> During the conference call, the Project Leadership (or EMO appointed Project
representative) provides a brief summary of the reasons and justifications for the
phase transition followed by a question and answer session.
<li> The EMO(ED) approves or fails the Review based on the public comments,
the scope of the Project, and the Purposes of the Eclipse Foundation as
defined in the Bylaws. The EMO(ED) announces the result
in the defined Review communication channel.
</ol>
<p>
If any Member believes that the EMO has acted incorrectly in approving or
failing a Review may appeal to the Board to review the EMO's decision.

<h4><a name="6_3_1_Creation_Review"></a>6.3.1 Creation Review</h4>
<div class="postit"><table><tr><td width="100px">See <a href="creation-review.php">guidelines and
checklists</a> about Creation Reviews.</td>
</tr></table></div>The purpose of the Creation Review is to assess the community and membership
response to the proposal, to verify that appropriate resources are available
for the project to achieve its plan, and to serve as a committer election for
the project's initial Committers. The Eclipse Foundation strives not to be a
repository of ''code dumps'' and thus projects must be sufficiently staffed for forward progress.
<p>
The Creation Review archival documents must include
short nomination bios of the proposed initial committers.
These bios should discuss
their relationship to, and history with, the incoming code
and/or their involvement with the area/technologies covered by the proposal.
The goal is to help keep any legacy contributors connected to new project and
explain that connection to the current and future Eclipse membership, as well
as justify the initial Committers' participation in a meritocracy.
(See [<a href="http://mail-archives.apache.org/mod_mbox/incubator-general/200610.mbox/%3c4522A123.5090400@rowe-clan.net%3e">1</a>])

<h4><a name="6_3_2_Graduation_Review"></a>6.3.2 Graduation Review</h4>
<div class="postit"><table><tr><td width="100px">See <a href="graduation-review.php">guidelines and
checklists</a> about Graduation Reviews.</td>
</tr></table></div>The purpose of the Graduation Review is to confirm that the Project is/has:<ul>
<li> a working and demonstratable code base of sufficiently high quality
<li> active and sufficiently diverse communities appropriate to the size of the graduating code base: adopters, developers, and users
<li> operating fully in the open following the Principles and Purposes of Eclipse
<li> a credit to Eclipse and is functioning well within the larger Eclipse community
</ul>
The Graduation Review is about the phase change from Incubation Phase to
Mature Phase. If the Project and/or some of its code is simultaneously relocating to another Project,
the Graduation Review will be combined with a Move Review.

<h4><a name="6_3_3_Release_Review"></a>6.3.3 Release Review</h4>
<div class="postit"><table><tr><td width="100px">See <a href="release-review.php">guidelines and
checklists</a> about Release Reviews.</td>
</tr></table></div>The purposes of a Release Review are: to summarize the accomplishments of the release,
to verify that the IP Policy has been followed and all approvals have been received, to
highlight any remaining quality and/or architectural issues, and to verify that the
project is continuing to operate according to the Princples and Purposes of Eclipse.

<h4><a name="6_3_4_Promotion_Review"></a>6.3.4 Promotion Review</h4>
The purpose of a Promotion Review is to determine if the Project has demonstrated
the characteristics of a Top-Level Project, e.g., consistent leadership in a
technical area and the recruitment of a wider developer community. The Project
will already be a well-functioning Mature Eclipse Project, so evidence to the
contrary will be a negative for promotion. Top-Level Projects, both through their
existance and their Council memberships, have substantial influence over direction
and operation of Eclipse, thus it behooves the membership to grant Top-Level status
only for merit: for demonstrated service to the larger Eclipse eco-system.

<h4><a name="6_3_5_Continuation_Review"></a>6.3.5 Continuation Review</h4>
The purpose of a Continuation Review is to verify that a Proposal or Project
continues to be a viable effort and a credit to Eclipse. The Project team will
be expected to explain the recent technical progress and to demonstrate sufficient
adopter, developer, and user support for the Project. The goal of the Continuation
Review is to avoid having inactive projects looking promising but never actually
delivering extensible frameworks and exemplary tools to the ecosystem.

<h4><a name="6_3_6_Termination_Review"></a>6.3.6 Termination Review</h4>
<div class="postit"><table><tr><td width="150px">See <a href="http://wiki.eclipse.org/Development_Resources/HOWTO/Review_Information_for_Project_Leads#Termination_.28Archive.29_Reviews">Termination Review &quot;How To&quot;</a> for more information.</td>
</tr></table></div>
The purpose of a Termination Review is to provide a final opportunity for the
Committers and/or Eclipse membership to discuss the proposed withdrawl of a
Proposal or archiving of a Project. The desired outcome is to find sufficient
evidence of renewed interest and resources in keeping the Project or Proposal
active.

<h4><a name="6_3_7_Move_Review"></a>6.3.7 Move Review</h4>
<div class="postit"><table><tr><td width="100px">See <a href="http://wiki.eclipse.org/Development_Resources/HOWTO/Review_Information_for_Project_Leads#Move_Reviews">Move Review &quot;How To&quot;</a> for more information about Move Reviews.</td>
</tr></table></div>
<p>The purpose of a Move Review is to verify that there are no IP Legal impediments
to the proposed move of code from one Project to another Project, and to
act as an election (if necessary) for the Committers who are being added
to the new Project.</p>
<p>
There are four Move Review cases: </p>
<ul>
<li>A subset of code is moving out of one Project (A) and into another Project (B).
-- Project B's Committers may not have new committers imposed upon them, thus
project B's Committers must elect (the subset of) Project A's Committers who are moving with the code to Project B.<sup>3</sup>
<li>An entire Project's (A) code is moving into another Project (B) and Project A is being terminated.
-- Same as above: Project B's Committers must elect Project A's Committers to committer status on Project B.
<li>An entire Project (A) is moving from one parent Project (P) to another parent Project (Q). --
No Committers are changing Operating Projects, thus no elections are necessary.
<li>Code is moving out of one Project (A) and starting a new Project (C). --
This is a Creation Review, not a Move Review. Project C committers will be defined by the
Creation Review.
</ul>
<p>
<sup>3</sup>Until such time as the Foundation portal supports Committer election-style voting for Move Reviews,
an election held on the destination Project's developer mailing list will suffice. </p>

<h4><a name="6_3_8_Restructuring_Review"></a>6.3.8 Restructuring Review</h4>
The purpose of a Restructuring Review is to verify that there are no IP Legal impediments
to the proposed restructuring, and to allow the community a chance to comment on that restructuring.
Restructuring may involve splitting an existing Project into multiple Projects (typically one Operating
Project into a Container Project with multiple new Operating Sub-Projects) and/or combining existing
Projects into fewer Projects (typically multiple Operating Projects into a single Operating Project).
<p>
If a Restructuring Review involves combining two or more Committer populations,
each Committer population must elect the other<sup>3</sup>, in order to explicitly maintain the
principle of not allowing any Committer population to have new Committers imposed there upon.

<h4><a name="6_3_9_Combining_Reviews"></a>6.3.9 Combining Reviews</h4>
Multiple Reviews may occur simultaneous for a given Project. The most common combinations
include Move+Release and Move+Graduation and Graduation+Release.

<h3><a name="6_4_Releases"></a>6.4 Releases</h3>
<em>(Most of this section is borrowed and paraphrased from the excellent
<a href="http://www.apache.org/dev/release.html">Apache Software
Foundation Releases FAQ</a>. The Eclipse community has many of the same
beliefs about Releases as does the Apache community and their words were already excellent.
The Apache Software Foundation Releases FAQ is distributed under the
<a href="http://www.apache.org/licenses/LICENSE-2.0">Apache License, Version 2.0</a>.)</em>
<p>
Releases are, by definition, anything that is distributed outside of the Committers
of a Project. If users are being directed to download a build, then
that build has been released (modulo the exceptions below). All Projects
and Committers must obey the Eclipse Foundation requirements on approving any release.
<p>
<em>(Exception 1: nightly and integration builds)</em>
During the process of developing software and preparing a Release, various
nightly and integration builds
are made available to the developer community for testing purposes. Do not include
any links on the project website, blogs, wikis, etc. that might
encourage non-early-adopters to download
and use nightly builds, release candidates, or any other similar package (links
aimed at early-adopters and the project's developers are both permitted and encouaged). The
only people who are supposed to know about such packages are the people following
the developer mailing list and thus are aware of the limitations of such builds.
<p>
<em>(Exception 2: milestone and release candidate builds)</em>
Projects are encouraged to use an agile development process including regular milestones
(for example, six week milestones). Milestones and release candidates are "almost releases" intended for
adoption and testing by early adopters. Projects are allowed to have links
on the project website, blogs, wikis, etc. to encourage these outside-the-committer-circle
early adopters to download and test the milestones and release candidates, but such communications must
include caveats explaining that these are not official Releases.
<p>
<ul>
<li>Milestones are to be labeled <code>x.yMz</code>, e.g., 2.3M1 (milestone 1 towards version 2.3), 2.3M2 (milestone 2 towards version 2.3), etc.
<li>Release candidates are to be labeled <code>x.yRCz</code>, e.g., 2.3RC1 (release candidate 1 towards version 2.3).
<li>Official Releases are the only downloads allowed to be labeled with <code>x.y</code>, e.g., 0.5, 1.0, 2.3, etc.
</ul>
<p>
All official Releases must have a successful Release Review before being made available for download.
<p>
<em>(Exception 3: bug fix releases with no new features)</em>
Bug fix releases (x.y.z, e.g., 2.3.1) with no new features over the base release (e.g., 2.3)
are allowed to be released without an additional Release Review.
If a bug fix release contains new features, then the Project must
have a full Release Review.
<p>
Under no circumstances are builds and milestones to be used as a substitute for doing
proper official Releases. Proper Release management and reviews is a key aspect of
Eclipse Quality.
<p>

<h3><a name="6_5_Grievance_Handling"></a>6.5 Grievance Handling</h3>
When a Member has a concern about a Project, the Member will raise
that concern with the Project's Leadership. If the Member is not
satisfied with the result, the Member can raise the concern with
the parent Project's Leadership. The Member can continue appeals
up the Project Leadership Chain and, if still not satisfied, thence
to the EMO, then the Executive Director, and finally to the Board.
All appeals and discussions will abide by the Guiding Principles of
being open, transparent, and public.
<p>
Member concerns may include:<ul>
<li><b>Out of Scope.</b> It is alleged that a Project is
exceeding its approved scope.
<li><b>Inconsistent with Purposes.</b> It is alleged that a Project
is inconsistent with the Roadmap and/or Purposes.
<li><b>Dysfunctional.</b> It is alleged that a Project is not
functioning correctly or is in violation of one or more requirements
of the Development Process.
<li><b>Contributor Appeal.</b> It is alleged that a Contributor who
desires to be a Committer is not being treated fairly.
<li><b>Invalid Veto.</b> It is alleged that a -1 vote on a Review is
not in the interests of the Project and/or of Eclipse.
</ul>
A variety of grievance resolutions are available to the EMO up to, and
including, rebooting or restarting a project with new Committers and leadership.

<h2><a name="7_Precedence"></a>7. Precedence</h2>
In the event of a conflict between this document and a Board-approved
project charter, the most recently approved document will take precedence.

<h2><a name="8_Revisions"></a>8. Revisions</h2>
As specified in the Bylaws, the EMO is responsible for maintaining
this document and all changes must be approved by the Board.
<p>
Due to the continued evolution of the Eclipse technology, the Eclipse
community, and the software marketplace, it is expected that the Development
Process (this document) will be reviewed and revised on at least an
annual basis. The timeline for that review should be chosen so as to
incorporate the lessons of the previous annual coordinate release and
to be applied to the next annual coordinated release.
</p><p>
The EMO is further responsible for ensuring that all plans, documents
and reports produced in accordance with this Development Process be
made available to the Membership at Large via an appropriate mechanism
in a timely, effective manner.

</p><h3><a name="8_1_Revision"></a>8.1 Revision 2.4</h3>
This document was approved by the Eclipse Foundation Board of Directors in its meeting
on August 20, 2008. It replaces all previous versions.
<p>&nbsp;
</div><!-- midcolumn -->
<div id="rightcolumn">
<div class="sideitem">
<h6>See also</h6>
<ul>
<li><a href="development_process_2010.pdf">PDF version of this document</a></li>
<li><a href="<?= $next ?>">Next edition of this document</a></li>
</ul>
</div>
</div>
</div><!-- maincontent -->
<?php
	# Paste your HTML content between the EOHTML markers!
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
