<?php
/*******************************************************************************
 * Copyright (c) Eclipse Foundation and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/projects/classes/debug.php");
$App = new App();
$Nav = new Nav();
$Menu = new Menu();
include($App->getProjectCommon());

$pageTitle = "Eclipse Foundation Development Process Changes";
$pageAuthor = "Wayne Beaton";
$pageKeywords = "EDP";

include( '../../_commonLeftNav.php' );

ob_start();
?>

<link rel="stylesheet" href="/projects/handbook/resources/handbook.css"/>

<div id="maincontent" class="draft">
	<h1><?php echo $pageTitle; ?></h1>
	<p><em>This document highlights differences between this version of the specification process and the last.
	Additions are <ins>underlined and coloured green</ins>; deletions are <del>struck through and coloured red</del>.</em></p>
	<p><strong>DRAFT</strong> This is based on a draft document that has not been approved.</p>
	<p>You can find the draft document <a href="./index.php">here</a>. Change are being tracked via
	<a href="https://gitlab.eclipse.org/eclipsefdn/emo-team/policies/development-process/-/milestones/1#tab-issues">GitLab</a>.</p>
	<hr/>
	<?php include "./diff.html"; ?>
	<hr/>
</div>

<?php
	$html = ob_get_contents();
	ob_end_clean();
	$App->generatePage(null, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
