<?php
/*******************************************************************************
 * Copyright (c) 2005, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *  David Thompson
 *  Bjorn Freeman-Benson
 *  Denis Roy
 *  Wayne Beaton
 *******************************************************************************/
require_once ($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");
require_once ($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php");
require_once ($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php");
$App = new App();
$Nav = new Nav();
$Menu = new Menu();

include ($App->getProjectCommon());
/* 221934 - this page to remain on eclipse.org */

$pageTitle = "Eclipse Standard Top-Level Charter v1.2";

include ('../_commonLeftNav.php');
ob_start();
?>

<div id="maincontent">
	<div id="midcolumn">
		<?php include 'standard_charter/standard_charter_v1.2.html'; ?>
	</div>
</div>

<?php
	# Paste your HTML content between the EOHTML markers!	
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>

