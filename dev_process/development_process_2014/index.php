<?php
/*******************************************************************************
 * Copyright (c) 2008, 2013 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Bjorn Freeman-Benson (Eclipse Foundation) - initial API and implementation
 *    Wayne Beaton (Eclipse Foundation) - 2010, 2011 updates.
 *******************************************************************************/
require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php");
$App = new App();
$Nav = new Nav();
$Menu = new Menu();
include($App->getProjectCommon());

$pageTitle = "Eclipse Development Process 2014";
$pageAuthor = "Wayne Beaton";
$pageKeywords = "EDP, Eclipse Development Process";

include( '../../_commonLeftNav.php' );

$previous = "/projects/dev_process/development_process_2011.php";
$diff = "/projects/dev_process/development_process_2014/diff.php";
$pdf = "/projects/dev_process/development_process_2014/development_process_2014.pdf";
$bylaws = "/org/documents/eclipse_foundation-bylaws.pdf#page=19";
$ip_policy = "/org/documents/Eclipse_IP_Policy.pdf";

$commentary = isset($_GET['commentary']);

function getChanges() {
    $changes = array();
    $file = fopen(dirname(__FILE__) . '/bugs.csv', 'r');
    fgetcsv($file);
    while ($line = fgetcsv($file)) {
        $id=$line[0];
        $url = "https://bugs.eclipse.org/$id";
        $summary = preg_replace('/\[[^\]]+\]/', '', $line[6]);

        $changes[] = "<li>(<a href=\"$url\">Bug $id</a>) $summary</li>";
    }
    return join("\n", $changes);
}
ob_start();
?>
<style>
p {
	margin-bottom: 10px;
}

blockquote {
	margin-left: 2em;
	margin-right: 2em;
	margin-bottom: 10px;
}

.comment {
	background-image:
		url(http://dev.eclipse.org/small_icons/apps/help-browser.png);
	background-position: 5px 5px;
	background-repeat: no-repeat;
	display: <?php echo $commentary ?   'block' :   'none'; ?>;
	float: right;
	width: 200px;
	margin: 5px;
	padding: 25px 2px 2px 2px;
	border: 1px solid black;
	background-color: #EEEEEE;
}

.comment p {
	margin: 3px;
}

.postit {
	float: right;
	width: 150px;
	margin: 5px;
	padding: 2px;
	border: 1px dashed black;
	background-color: #FFFFDD;
}

#toc {
	padding: 5px;
}

#toc ul {
	list-style-type: none;
	list-style-image: none;
	margin-top: 1px;
	margin-bottom: 1px;
	padding-top: 1px;
	padding-bottom: 1px;
	text-align: left;
}

#toc li {
	margin-top: 1px;
	margin-bottom: 1px;
	padding-top: 0;
}
</style>
<div id="maincontent">
	<div id="midcolumn">

		<h1><?php echo $pageTitle; ?></h1>
		<div class="homeitem3col">
			<h2>Contents</h2>
			<div id="toc">
				<ul>
					<li><a href="#1_Purpose">1 Purpose</a></li>
					<li><a href="#2_Principles">2 Principles</a>
						<ul>
							<li><a href="#2_1_Open_Source_Rules_of_Engagement">2.1 Open
									Source Rules of Engagement</a></li>
							<li><a href="#2_2_Eclipse_Ecosystem">2.2 Eclipse Ecosystem</a></li>
							<li><a href="#2_3_Three_Communities">2.3 Three Communities</a>
								<ul>
									<li><a href="#2_3_1_Committers">2.3.1 Contributors and Committers</a></li>
									<li><a href="#2_3_2_Users">2.3.2 Users</a></li>
									<li><a href="#2_3_3_Adopters">2.3.3 Adopters</a></li>
								</ul>
							</li>
							<li><a href="#2_4_Clear_Concise_and_Evolving">2.4 Clear, Concise,
									and Evolving</a></li>
						</ul></li>
					<li><a href="#3_Requirements">3 Requirements</a>
						<ul>
							<li><a href="#3_1_Requirements_and_Guidelines">3.1 Requirements
									and Guidelines</a></li>
						</ul></li>
					<li><a href="#4_Structure_and_Organization">4 Project Structure and
							Organization</a>
						<ul>
							<li><a href="#4_1_Committers">4.1 Committers</a></li>
							<li><a href="#4_2_Code_and_Releases">4.2 Code and Resources</a></li>
							<li><a href="#4_3_IP_Records">4.3 Intellectual Property (IP) Records</a></li>
							<li><a href="#4_4_Community_Awareness">4.4 Community Awareness</a>
							</li>
							<li><a href="#4_5_Scope">4.5 Scope</a></li>
							<li><a href="#4_6_Leaders">4.6 Leaders</a>
								<ul>
									<li><a href="#4_6_1_PMC">4.6.1 Project Management Committee
											(PMC)</a></li>
									<li><a href="#4_6_2_PL">4.6.2 Project Lead(s)</a></li>
								</ul>
							</li>
							<li><a href="#4_7_Committers_and_Contributors">4.7 Committers and
									Contributors</a></li>
							<li><a href="#4_8_Councils">4.8 Councils</a></li>
							<li><a href="#4_9_Incubators">4.9 Permanent Incubator Projects</a></li>
						</ul></li>
					<li><a href="#5_Reserved">5 [Reserved]</a></li>
					<li><a href="#6_Development_Process">6 Development Process</a>
						<ul>
							<li><a href="#6_1_Mentors">6.1 Mentors</a></li>
							<li><a href="#6_2_Project_Lifecycle">6.2 Project Lifecycle</a></li>
							<li><a href="#6_2_1_Pre-Proposal">6.2.1 Pre-proposal</a></li>
							<li><a href="#6_2_2_Proposal">6.2.2 Proposal</a></li>
							<li><a href="#6_2_3_Incubation">6.2.3 Incubation</a></li>
							<li><a href="#6_2_4_Mature">6.2.4 Mature</a></li>
							<li><a href="#6_2_5_Top-Level">6.2.5 [Reserved]</a></li>
							<li><a href="#6_2_6_Archived">6.2.6 Archived </a></li>
							<li><a href="#6_3_Reviews">6.3 Reviews</a>
								<ul>
									<li><a href="#6_3_1_Creation_Review">6.3.1 Creation Review</a>
									</li>
									<li><a href="#6_3_2_Graduation_Review">6.3.2 Graduation Review</a>
									</li>
									<li><a href="#6_3_3_Release_Review">6.3.3 Release Review</a></li>
									<li><a href="#6_3_4_Promotion_Review">6.3.4 [Reserved]</a>
									</li>
									<li><a href="#6_3_5_Continuation_Review">6.3.5 [Reserved]</a></li>
									<li><a href="#6_3_6_Termination_Review">6.3.6 Termination
											Review</a></li>
									<li><a href="#6_3_7_Move_Review">6.3.7 [Reserved]</a></li>
									<li><a href="#6_3_8_Restructuring_Review">6.3.8 Restructuring
											Review</a></li>
									<li><a href="#6_3_9_Combining_Reviews">6.3.9 Combining Reviews</a>
									</li>
								</ul></li>
							<li><a href="#6_4_Releases">6.4 Releases</a></li>
							<li><a href="#6_5_Grievance_Handling">6.5 Grievance Handling</a>
							</li>
						</ul></li>
					<li><a href="#7_Precedence">7 Precedence</a></li>
					<li><a href="#8_Revisions">8 Revisions</a>
						<ul>
							<li><a href="#8_1_Revision">8.1 Revision 2.4</a></li>
						</ul></li>
				</ul>
			</div>
		</div>

		<h2>
			<a name="1_Purpose"></a>1. Purpose
		</h2>
		<p>This document describes the development process for the Eclipse
			Foundation. In particular, it describes how the membership at large,
			the board of directors, other constituents of the ecosystem, and the
			Eclipse Management Organization (EMO) lead, influence, and
			collaborate with Eclipse projects to achieve these Eclipse purposes:</p>

		<blockquote>
			<em>The Eclipse technology is a vendor-neutral, open development
				platform supplying frameworks and exemplary, extensible tools (the
				'Eclipse Platform'). Eclipse Platform tools are exemplary in that
				they verify the utility of the Eclipse frameworks, illustrate the
				appropriate use of those frameworks, and support the development and
				maintenance of the Eclipse Platform itself; Eclipse Platform tools
				are extensible in that their functionality is accessible via
				documented programmatic interfaces. The purpose of Eclipse
				Foundation Inc., is to advance the creation, evolution, promotion,
				and support of the Eclipse Platform and to cultivate both an open
				source community and an ecosystem of complementary products,
				capabilities, and services.</em>
		</blockquote>

		<div class="postit">
			<p>
				Explanatory comments, guidelines, and checklists - as well as
				additional requirements added by the EMO per <a
					href="#3_Requirements">section 3</a> - are noted in yellow boxes.
			</p>
		</div>

		<p>This document has the following sections:</p>
		<ul>
			<li><a href="#2_Principles"><em>Principles</em> </a> outlines the
				basic principles upon which the development process is based.</li>
			<li><a href="#3_Requirements"><em>Requirements</em> </a> describes
				the requirements that the Eclipse community has for its development
				process.</li>
			<li><a href="#4_Structure_and_Organization"><em>Structure and
						Organization</em> </a> specifies the structure and organization of
				the projects and project community at Eclipse.</li>
			<li><a href="#6_Development_Process"><em>Development Process</em> </a>
				outlines the lifecycle and processes required of all Eclipse
				projects.</li>
		</ul>

		<h2>
			<a name="2_Principles"></a>2. Principles
		</h2>
		<p>The following describes the guiding principles used in developing
			this development process.</p>

		<h3>
			<a name="2_1_Open_Source_Rules_of_Engagement"></a>2.1 Open Source
			Rules of Engagement
		</h3>
		<ul>
			<li><b>Open</b> - Eclipse is open to all; Eclipse provides the same
				opportunity to all. Everyone participates with the same rules; there
				are no rules to exclude any potential contributors which include, of
				course, direct competitors in the marketplace.</li>
			<li><b>Transparent</b> - Project discussions, minutes, deliberations,
				project plans, plans for new features, and other artifacts are open,
				public, and easily accessible.</li>
			<li><b>Meritocracy</b> - Eclipse is a meritocracy. The more you
				contribute the more responsibility you will earn. Leadership roles
				in Eclipse are also merit-based and earned by peer acclaim.</li>
		</ul>

		<h3>
			<a name="2_2_Eclipse_Ecosystem"></a>2.2 Eclipse Ecosystem
		</h3>

		<p>Eclipse as a brand is the sum of its parts (all of the projects),
			and projects should strive for the highest possible quality in
			extensible frameworks, exemplary tools, transparent processes, and
			project openness.</p>
		<p>
			The Eclipse Foundation has the responsibility to <em>...cultivate...an
				ecosystem of complementary products, capabilities, and services...</em>.
			It is therefore a key principle that the Eclipse Development Process
			ensures that the projects are managed for the benefit of both the
			open source community and the ecosystem members. To this end, all
			Eclipse projects are required to:
		</p>
		<ul>
			<li>communicate their project plans and plans for new features (major
				and minor) in a timely, open and transparent manner;</li>
			<li>create platform quality frameworks capable of supporting the
				building of commercial grade products on top of them; and</li>
			<li>ship extensible, exemplary tools which help enable a broad
				community of users</li>
		</ul>

		<h3>
			<a name="2_3_Three_Communities"></a>2.3 Three Communities
		</h3>
		<p>Essential to the purposes of the Eclipse Foundation is the
			development of three inter-related communities around each project:</p>
		<h4><a name="2_3_1_Committers"></a> 2.3.1 Contributors and Committers</h4>
			<p>A  thriving, diverse,
				and active community of developers is the key component of any
				Eclipse project. Ideally, this community should be an open,
				transparent, inclusive, and diverse community of committers and
				(non-committer) contributors. Attracting new contributors and
				committers to an open source project is time consuming and requires
				active recruiting, not just passive "openness". The project
				leadership must make reasonable efforts to encourage and nurture
				promising new contributors.</p>
			<h4><a name="2_3_2_Users"></a>2.3.2 Users</h4>
			<p>An active and engaged user community is
				proof-positive that the project's exemplary tools are useful and
				needed. Furthermore, a large user community is one of the key
				factors in creating a viable ecosystem around an Eclipse project,
				thus encouraging additional open source and commercial organizations
				to participate. Like all good things, a user community takes time
				and effort to bring to fruition, but once established is typically
				self-sustaining.</p>
			<h4><a name="2_3_3_Adopters"></a>2.3.3 Adopters</h4>
			<p>An active and engaged adopter/plug-in developer
				community is the only way to prove that an Eclipse project is
				providing extensible frameworks and extensible tools accessible via
				documented APIs. Reuse of the frameworks within the companies that
				are contributing to the project is necessary, but not sufficient to
				demonstrate an adopter community. Again, creating, encouraging, and
				nurturing an adopter community outside of the project's developers
				takes time, energy, and creativity by the project leadership, but is
				essential to the project's long-term open source success.</p>

		<p>The Eclipse community considers the absence of any one or more of
			these communities as proof that the project is not sufficiently open,
			transparent, and inviting, and/or that it has emphasized tools at the
			expense of extensible frameworks or vice versa.</p>

		<h3>
			<a name="2_4_Clear_Concise_and_Evolving"></a>2.4 Clear, Concise, and
			Evolving
		</h3>
		<p>It is an explicit goal of the development process to be as clear
			and concise as possible so as to help the project teams navigate the
			complexities, avoid the pitfalls, and become successful as quickly as
			possible.</p>
		<p>This document imposes requirements and constraints on the operation
			of the projects, and it does so on behalf of the larger Eclipse
			community. It is an explicit goal of the development process to
			provide as much freedom and autonomy to the projects as possible
			while ensuring the collective qualities benefit the entire Eclipse
			community.</p>
		<p>Similarly, this document should not place undue constraints on the
			EMO or the board that prevent them from governing the process as
			necessary. We cannot foresee all circumstances and as such should be
			cautious of being overly prescriptive and/or requiring certain fixed
			metrics.</p>
		<p>The frameworks, tools, projects, processes, community, and even the
			definition of quality continues to, and will continue to, evolve.
			Creating rules or processes that force a static snapshot of any of
			these is detrimental to the health, growth, and ecosystem impact of
			Eclipse.</p>
		<p>Part of the strength of this document is in what it does not say,
			and thus opens for community definition through convention,
			guidelines, and public consultation. A document with too much
			structure becomes too rigid and prevents the kind of innovation and
			change we desire for Eclipse. In areas where this document is vague,
			we expect the projects and members to engage the community-at-large
			to clarify the current norms and expectations.</p>

		<h2>
			<a name="3_Requirements"></a>3. Requirements
		</h2>
		<p>This document and any additional criteria as established by the EMO
			contains requirements, recommendations, and suggestions.</p>
		<p>
			Required
			- Certain responsibilities and behaviors are required of participants
			in Eclipse open source projects. Projects that fail to perform the
			required behaviors will be terminated by the EMO. In keeping with the
			guiding principles, the number of requirements must be kept to an
			absolute minimum.
		</p>
		<p>
			Guideline
			- Other responsibilities and behaviors are recommended best
			practices. Collectively, we have learned that projects are more
			likely to be successful if the team members and leaders follow these
			recommendations. Projects are strongly encouraged to follow these
			recommendations, but will not be penalized by this process if they do
			not.
		</p>

		<h3>
			<a name="3_1_Requirements_and_Guidelines"></a>3.1 Requirements and
			Guidelines
		</h3>
		<p>This document is entirely composed of requirements. In addition to
			the requirements specified in this development process, the EMO is
			instructed to clarify, expand, and extend this process by creating a
			set of Eclipse project development guidelines to advance the
			creation, evolution, promotion, and support of the Eclipse Platform
			and to cultivate both an open source community and an ecosystem of
			complementary products and services.</p>
		<p>The EMO is not permitted to override or ignore the requirements
			listed in this document without the express written endorsement of
			the board of directors.</p>

		<h2>
			<a name="4_Structure_and_Organization"></a>4. Project Structure and
			Organization
		</h2>

		<p>
			A project is the main operational unit at Eclipse.
			Specifically, all open source software development at Eclipse occurs
			within the context of a project. Projects have leaders, developers,
			code, builds, downloads, websites, and more. Projects are more than
			just the sum of their many parts, they are the means by which open
			source work is organized when presented to the communities of
			developers, adopters, and users. Projects provide structure that
			helps developers expose their hard work to a broad audience of
			consumers.
		</p>

		<p>
			Eclipse projects are organized hierarchically. A special type of
			project, &quot;top-level&quot; projects, sit at the top of the hierarchy.
			Each top-level project contains one or more projects. Each project
			may itself contain zero or more projects. A project that has one or
			more projects is said to be the &quot;parent&quot; of those projects.
			A project that has a parent is oftentimes referred to as a &quot;subproject&quot;.
			The term project refers to either a top-level project or a
			subproject.
		</p>

		<p>
			The descendants of a project are the project itself and
			transitive closure of its child projects. The top parent of a
			project is the top-level project at the top of the hierarchy.
		</p>
		<p>Projects are the unit entity for:</p>
		<ul>
			<li>committers;</li>
			<li>code and releases;</li>
			<li>intellectual property (IP) records; and</li>
			<li>community awareness</li>
		</ul>
		<p>
			As defined by <a
				href="<?php echo $bylaws; ?>">Bylaws of Eclipse
				Foundation - Article VII</a>, the &quot;Eclipse Management Organization&quot;
				(EMO) consists of the Eclipse Foundation staff and the councils. The
			term EMO(ED), when discussing an approval process, refers to
			the subset of the EMO consisting of the executive director and
			whomever he or she may delegate that specific approval authority to.
		</p>

		<h3>
			<a name="4_1_Committers"></a>4.1 Committers
		</h3>

		<p>
			Each project has exactly one set of committers. Each project's set of
			committers is distinct from that of any other project, including
			subprojects or parent projects. All project committers have equal
			rights and responsibilities within the project. Partitioning of
			responsibility within a project is managed using social convention. A
			project may, for example, divide itself into logical partitions of
			functionality; it is social convention that prevents committers from
			one logical partition from doing inappropriate work in another. If
			finer-grained management of committer responsibilities is required, a
			project should consider partitioning (via a <a
				href="#6_3_8_Restructuring_Review">Restructuring Review</a>) into
			two or more subprojects.
		</p>

		<p>The committers of a project have the exclusive right to elect new
			committers to their project; no other group, including a parent
			project, can force a project to accept a new committer.</p>

		<p>There is no roll-up of committers: the set of committers on a
			project is exactly that set of people who have been explicitly
			elected into that role for the project (i.e. being a committer on a
			subproject does not give you any automatic rights on the
			&quot;parent&quot; project or any child project).</p>

		<p>In practical terms, each project has a single UNIX group of its
			committers that provides write-access to the project's resources.
			Pictorially below, we see that a project, in addition to the various
			resources and committers it has, can also have zero or more
			subprojects. Each of these subprojects has its own distinct set of
			committers and resources.</p>

		<img src="/projects/dev_process/images/subprojects-resources-291x300.png" />

		<h3>
			<a name="4_2_Code_and_Releases"></a>4.2 Code and Resources
		</h3>

		<p>Each project owns and maintains a collection of resources.</p>

		<p>Resources may include source code, a project website, space on the
			downloads server, access to build resources, and other services
			provided by the Eclipse Foundation infrastructure. The exact
			infrastructure provided by the Eclipse Foundation varies over time
			and is defined outside this process document.</p>

		<p>
			A project is not strictly required to make use of all the resources
			made available; a project might, for example, opt to <em>not</em>
			maintain a source code repository. Such a project might operate as an
			organizational unit, or container, for several subprojects.
			Similarly, a project might opt to provide a consolidated website,
			build and/or download site for its subprojects (the subprojects
			would then not require those resources for themselves).
		</p>

		<p>
			Namespaces are assigned to a project by the EMO. All project source code
			must be organized in the assigned namespaces and projects can only release
			code under their own namespace (that is, they cannot infringe on another
			Eclipse project's namespace). Projects should work with their PMCs and the
			EMO to request exceptions to this rule, and with their mentors and PMC if
			there are questions regarding the use of the namespace.
		</p>

		<h3>
			<a name="4_3_IP_Records"></a>4.3 Intellectual Property (IP) Records
		</h3>
		<p>A project at any level may receive IP clearance for contributions
			and third-party libraries. IP approval will often include the same
			approval for all descendant projects. However, IP clearance will only
			be granted at the most appropriate technical level.</p>

		<h3>
			<a name="4_4_Community_Awareness"></a>4.4 Community Awareness
		</h3>
		<p>Projects are the level of communication with the larger Eclipse
			community and ecosystem. Projects may either have their own
			communications (website, mailing lists, forums/newsgroups, etc) or
			they may be part of a parent project's communications (website,
			mailing list, forums/newsgroups, etc). In either case, the project is
			required to maintain an open and public communication channel with
			the Eclipse community including, but not limited to, project plans,
			schedules, and design discussions.</p>
		<p>All projects must make the communication channels easy to find.
			Projects are further required to make the separate communication
			channels of their child projects (if any) easy to find.</p>
		<p>Any project in the incubation phase must correctly identify its
			website and releases. A project with at least one descendant project
			in incubation phase must correctly annotate its own website so as to
			notify the Eclipse community that incubating projects exist in its
			hierarchy. Any release containing code from an incubation phase
			project must be correctly labeled, i.e., the incubation phase is
			viral and expands to cover all releases in which it is included.</p>

		<h3>
			<a name="4_5_Scope"></a>4.5 Scope
		</h3>
		<p>
			Each top-Level project has a charter which describes the
			purpose, scope, and operational rules for the top-level
			project. The charter should refer to, and describe any refinements
			to, the provisions of this development process. The board of directors approves
			the charter of each top-level project.
		</p>
		<p>Subprojects do not have separate charters; subprojects operate
			under the charter of their parent top-Level project.</p>
		<p>
			All projects have a defined scope and all initiatives within
			that project are required to reside within that scope. Initiatives
			and code that is found to be outside the scope of a project may
			result in the termination of the project. The scope of top-level
			projects is part of the charter, as approved by the board of
			directors of the Eclipse Foundation.
		</p>
		<p>
			The scope of a subproject is defined by the initial project proposal
			as reviewed and approved by the Project Management Committee (PMC)
			(as further defined below) of the project's top parent and
			by the EMO. A project's scope must be a subset of its parent's scope.
		</p>

		<h3>
			<a name="4_6_Leaders"></a>4.6 Leaders
		</h3>

		<p>There are two different types of project leadership at Eclipse: The
			Project Management Committee (PMC) and project leads. Both forms of
			leadership are required to:</p>
		<ul>
			<li>ensure that their project is operating effectively by guiding the
				overall direction and by removing obstacles, solving problems, and
				resolving conflicts;</li>
			<li>operate using open source rules of engagement: meritocracy,
				transparency, and open participation; and</li>
			<li>ensure that the project and its subprojects (if any) conform to
				the Eclipse Foundation IP policy and procedures.</li>
		</ul>

		<p>The leadership chain for a project is composed of the project's project
        lead(s), the leadership of the parent project (if any), the PMC
        leads and PMC members for the top-level project, the EMO, and the EMO(ED).</p>

		<p>In exceptional situations&mdash;such as projects with zero active committers,
		disruptive committers, or no effective project leads&mdash;the project leadership
		chain has the authority to make changes (add, remove) to the set of committers
		and/or project leads of that project, and otherwise act on behalf of the project lead.</p>

		<h4>
			<a name="4_6_1_PMC"></a>4.6.1 Project Management Committee (PMC)
		</h4>

		<p>Top-level projects are managed by a Project Management Committee
			(PMC). A PMC has one or more PMC leads and zero or more PMC Members.
			Together the PMC provides oversight and overall leadership for the
			projects that fall under their top-level project. The PMC as a whole,
			and the PMC leads in particular, are ultimately responsible for
			ensuring that the Eclipse Development Process is understood and
			followed by their projects. The PMC is additionally responsible for
			maintaining the top-level project's charter.</p>

		<p>PMC leads are approved by the board of directors; PMC members are elected by the
			existing PMC leads and members, and approved by the EMO(ED).</p>

		<h4>
			<a name="4_6_2_PL"></a>4.6.2 Project Lead
		</h4>

		<p>Eclipse projects are managed by one or more project leads. Project
			leads are responsible for ensuring that their project's committers
			are following the Eclipse Development Process, and that the project
			is engaging in the right sorts of activities to develop vibrant
			communities of users, adopters, and contributors. The initial project
			leads are appointed and approved in the creation review.
			Subsequently, additional project leads must be elected by the
			project's committers and approved by the project's PMC and the
			EMO(ED).</p>

		<p>In the unlikely event that a project lead becomes disruptive to the
		process or ceases to contribute for an extended period, the individual
		may be removed by the unanimous vote of the remaining project leads
		(if there are at least two other project leads), or unanimous vote of
		the project's PMC.</p>

		<h3>
			<a name="4_7_Committers_and_Contributors"></a>4.7 Committers and
			Contributors
		</h3>

		<p>
			Each project has a development team, led by the project
			leaders. The development team is composed of committers and contributors.
			Contributors are individuals who contribute code, fixes,
			tests, documentation, or other work that is part of the project. Committers
			have write access to the project's resources (source code repository,
			bug tracking system, website, build server, downloads, etc.) and are
			expected to influence the project's development.
		</p>

		<div class="postit">
			See <a href="new-committer.php">guidelines and checklists</a> for
			electing a new committer.
		</div>
		<p>Contributors who have the trust of the project's committers can,
			through election, be promoted committer for that project. The breadth
			of a committer's influence corresponds to the breadth of their
			contribution. A development team's contributors and committers may
			(and should) come from a diverse set of organizations. A committer
			gains voting rights allowing them to affect the future of the
			project. Becoming a committer is a privilege that is earned by
			contributing and showing discipline and good judgment. It is a
			responsibility that should be neither given nor taken lightly, nor is
			it a right based on employment by an Eclipse member company or any
			company employing existing committers.</p>
		<p>The election process begins with an existing committer on the same
			project nominating the contributor. The project's committers will
			vote for a period of no less than one week of standard business days.
			If there are at least three (3) positive votes and no negative votes
			within the voting period, the contributor is recommended to the
			project's PMC for commit privileges. If there are three (3) or fewer
			committers on the project, a unanimous positive vote of all
			committers is substituted. If the PMC approves, and the contributor
			signs the appropriate committer legal agreements established by the
			EMO (wherein, at the very least, the developer agrees to abide by the
			Eclipse Intellectual Property Policy), the contributor becomes a
			committer and is given write access to the source code for that
			project.</p>
		<p>At times, committers may become inactive for a variety of reasons.
			The decision making process of the project relies on active
			committers who respond to discussions and vote in a constructive and
			timely manner. The project leads are responsible for ensuring the
			smooth operation of the project. A committer who is disruptive, does
			not participate actively, or has been inactive for an extended period
			may have his or her commit status revoked by the project leads.
			Unless otherwise specified, "an extended period" is defined as "no
			activity for more than six months".</p>
		<p>Active participation in the user communication channels and the
			appropriate developer mailing lists is a responsibility of all
			committers, and is critical to the success of the project. Committers
			are required to monitor and contribute to the user communication channels.</p>
		<p>Committers are required to monitor the mailing lists associated
			with the project. This is a condition of being granted commit rights
			to the project. It is mandatory because committers must participate
			in votes (which in some cases require a certain minimum number of
			votes) and must respond to the mailing list in a timely fashion in
			order to facilitate the smooth operation of the project. When a
			committer is granted commit rights they will be added to the
			appropriate mailing lists. A committer must not be unsubscribed from
			a developer mailing list unless their associated commit privileges
			are also revoked.</p>
		<p>Committers are required to track, participate in, and vote on,
			relevant discussions in their associated projects.
			There are three voting responses: +1 (yes), -1 (no, or veto), and 0
			(abstain).</p>
		<p>Committers are responsible for proactively reporting problems in
			the bug tracking system, and annotating problem reports with status
			information, explanations, clarifications, or requests for more
			information from the submitter. Committers are responsible for
			updating problem reports when they have done work related to the
			problem.</p>
		<p>Committer, PMC lead or member, project lead, and council representative(s)
			are roles; an individual may take on more than one of these roles
			simultaneously.</p>

		<h3>
			<a name="4_8_Councils"></a>4.8 Councils
		</h3>

		<p>The councils defined in the bylaws, section VII are comprised of
			strategic members and PMC representatives. The councils help guide
			the projects as follows:</p>
		<ul>
			<li>The Planning Council is responsible for establishing a
				coordinated simultaneous release (a.k.a, "the release train"). The
				Planning Council is further responsible for cross-project planning,
				architectural issues, user interface conflicts, and all other
				coordination and integration issues. The Planning Council discharges
				its responsibility via collaborative evaluation, prioritization, and
				compromise.</li>
			<li><div class="postit">
					See <a href="architecture-council.php">guidelines and checklists</a>
					for the Architecture Council.
				</div>The Architecture Council is responsible for (i)
				monitoring, guiding, and influencing the software architectures used
				by projects, (ii) new project mentoring, and (iii) maintaining and
				revising the Eclipse Development Process. Membership in the
				Architecture Council is per the bylaws through strategic membership,
				PMCs, and by appointment. The Architecture Council will, at least
				annually, recommend to the EMO(ED), Eclipse Members who have
				sufficient experience, wisdom, and time to be appointed to the
				Architecture Council and serve as mentors. Election as a mentor is a
				highly visible confirmation of the Eclipse community's respect for
				the candidate's technical vision, good judgement, software
				development skills, past and future contributions to Eclipse. It is
				a role that should be neither given nor taken lightly. Appointed
				members of the Architecture Council are appointed to two year
				renewable terms.</li>
		</ul>
		<h3>
			<a name="4_9_Incubators"></a>4.9 Permanent Incubator Projects
		</h3>

		<p>
			A project may designate a subproject as a &quot;permanent incubator&quot;. A
			permanent incubator is a project that is intended to perpetually remain in the
			<a href="#6_2_3_Incubation">incubation</a> phase. Permanent incubators are an
			excellent place to innovate, test new ideas, grow functionality that
			may one day be moved into another project, and develop new
			committers.
		</p>

		<p>Permanent incubator projects never have releases; they cannot participate in the annual
			simultaneous release. Permanent incubators may have builds, and downloads. They conform to the
			standard incubation branding requirements and are subject to the IP
			due diligence rules outlined for incubating projects. Permanent incubators do
			not graduate.</p>

		<p>The scope of a permanent incubator project must fall within the scope of its
			parent project. The committer group of the permanent incubator project must
			overlap with that of the parent project (at least one committer from
			the parent project must be a committer for the incubator). Permanent incubator
			projects do not require Architecture Council mentors (the parent
			project's committers are responsible for ensuring that the incubator
			project conforms to the rules set forth by the Eclipse Development
			Process).</p>

		<p>A permanent incubator project must be designated as such by including the
			word &quot;incubator&quot; in its name (e.g. &quot;Eclipse
			Incubator&quot;). To do otherwise is considered exceptional and
			requires approval from the PMC and EMO(ED).</p>

		<p>
			Only top-level projects and projects in the <a href="#6_2_4_Mature">mature
				phase</a> may create a permanent incubator. Permanent incubator projects are created upon
				request; a creation review is not required.
		</p>

		<h2>
			<a name="5_Reserved"></a>5. [Reserved]
		</h2>

		<h2>
			<a name="6_Development_Process"></a>6. Development Process
		</h2>
		<p>Projects must work within their scope. Projects that desire to
			expand beyond their current scope must seek an enlargement of their
			scope using a public review as described below. Further, projects
			must fit within the scope defined by their containing projects and
			the scope defined in the charter of their top-level project.</p>

		<p>Projects must provide advanced notification of upcoming features
			and frameworks via their project plan.</p>

		<h3>
			<a name="6_1_Mentors"></a>6.1 Mentors
		</h3>
		<p>
			New project proposals are required to have at least two mentors.
			Mentors must be members of the Architecture Council. The
			mentors must be listed in the proposal. Mentors are required
			to monitor and advise the new project during its incubation phase;
			they are released from that duty once the project graduates to the
			mature phase.
		</p>

		<h3>
			<a name="6_2_Project_Lifecycle"></a>6.2 Project Lifecycle
		</h3>
		<img src="/projects/dev_process/development_process_2014/images/lifecycle.png"
			align="right">
		<p>Projects go through distinct phases. The transitions from phase
			to phase are open and transparent public reviews.</p>

		<h4>
			<a name="6_2_1_Pre-Proposal"></a>6.2.1 Pre-proposal
		</h4>
		<div class="postit">
			See <a href="pre-proposal-phase.php">guidelines and checklists</a>
			about writing a proposal.
		</div>
		<p>An individual or group of individuals declares their interest in,
			and rationale for, establishing a project. The EMO will assist such
			groups in the preparation of a project proposal.</p>
		<p>The pre-proposal phase ends when the proposal is published by EMO
				and announced to the membership by the EMO.</p>

		<h4>
			<a name="6_2_2_Proposal"></a>6.2.2 Proposal
		</h4>
		<div class="postit">
			See <a href="proposal-phase.php">guidelines and checklists</a> about
			gathering support for a proposal.
		</div>
		<p>The proposers, in conjunction with the destination PMC and the
			community, collaborate in public to enhance, refine, and clarify the
			proposal. Mentors for the project must be identified
			during this phase.</p>
		<p>The proposal phase ends with a <a href="#6_3_1_Creation_Review">creation
			review</a>, or withdrawal. The proposal may be withdrawn by the proposers
			at any point before the start of a creation review.
			The EMO will withdraw a proposal that has been inactive for
			more than six months.</p>

		<h4>
			<a name="6_2_3_Incubation"></a>6.2.3 Incubation
		</h4>
		<div class="postit">
			See <a href="incubation-phase.php">guidelines and checklists</a>
			about incubation.
		</div>
		<p>The purpose of the incubation
			phase is to establish a fully-functioning open-source project. In
			this context, incubation is about developing the process, the
			community, and the technology. Incubation is a phase rather than a
			place: new projects may be incubated under any existing project.</p>
		<ul>
			<li>A project in the incubation phase can (and should) make releases;</li>
			<li>Top-level projects skip incubation and are immediately put into the mature phase;</li>
			<li>The incubation phase ends with a graduation review or a
				termination review.</li>
			<li>Designated <a href="#4_9_Incubators">permanent incubator projects</a>
				remain perpetually in the incubation phase; they do not create releases, so
				no reviews are required.</li>
		</ul>
		<p>Many Eclipse projects are proposed and initiated by individuals
			with extensive and successful software development experience. This
			document attempts to define a process that is sufficiently flexible
			to learn from all its participants. At the same time, however, the
			incubation phase is useful for new projects to learn the
			community-defined Eclipse-centric open source processes.</p>

		<div class="postit">
			See <a href="parallel-ip-process.php">guidelines and checklists</a>
			for utilizing the Parallel IP Process.
		</div>
		<p>
			Only projects that are properly identified as being in the incubation
			phase (including designated <a href="#4_9_Incubators">permanent incubator
				projects</a>) may use the Parallel IP Process to reduce IP clearance
			process for new contributions.
		</p>

		<h4>
			<a name="6_2_4_Mature"></a>6.2.4 Mature
		</h4>
		<div class="postit">
			See <a href="mature-phase.php">guidelines and checklists</a> about
			the mature phase.
		</div>
		<p>The project team has demonstrated that they are an open-source
			project with an open and transparent process; an actively involved
			and growing community; and Eclipse-quality technology. The project is
			now a mature member of the Eclipse community. Major releases continue
			to go through release reviews.</p>

		<h4>
			<a name="6_2_5_Top-Level"></a>6.2.5 [Reserved]
		</h4>

		<h4>
			<a name="6_2_6_Archived"></a>6.2.6 Archived
		</h4>
		<div class="postit">
			See <a href="archived-phase.php">guidelines and checklists</a> for
			archiving projects.
		</div>
		<p>Projects that become inactive, either through dwindling resources
			or by reaching their natural conclusion, are archived. Projects can
			reach their natural conclusion in a number of ways: for example, a
			project might become so popular that it is absorbed into one of the
			other major frameworks. Projects are moved to archived status through
			a termination review.</p>

		<p>If there is sufficient community interest in reactivating an
			archived project, the project can start again with a creation review.
			As there must be good reasons to have terminated a project, the
			creation review provides a sufficiently high bar to
			prove that those reasons are no longer valid.</p>

		<h3>
			<a name="6_3_Reviews"></a>6.3 Reviews
		</h3>
		<p>The Eclipse Development Process is predicated on open and
			transparent behavior. All major changes to Eclipse projects must be
			announced and reviewed by the membership-at-large. Major changes
			include the project phase transitions as well as the introduction or
			exclusion of significant new technology or capability. It is a clear
			requirement of this document that members who are monitoring the
			appropriate media channels (e.g., mailing lists or RSS feeds) not be
			surprised by the post-facto actions of the projects.</p>
		<p>
			For each review, the project leads prepare documentation
			for, and receive feedback from, the Eclipse membership.
		</p>
		<p>
			A review is a fairly comprehensive process. Gathering the material
			for a review and preparing the documentation is a non-trivial effort,
			but the introspection offered by this exercise is useful for the
			project and results are very useful for the entire Eclipse community.
			In addition, reviews have a specific relationship to the requirements
			of the <a href="<?php echo $ip_policy; ?>">Eclipse IP
				Policy</a>.
		</p>
		<p>All reviews have the same general process:</p>
		<ol>
			<li>Projects are responsible for initiating the appropriate reviews.
				However, if a project does not do so and the EMO believes a review
				is necessary, the EMO may initiate a review on the project's behalf.</li>
			<li>A review then continues with a project lead requesting
				that the EMO(ED) schedule the review.</li>
			<li>Prior to the start of the review period, a project lead
				provides the EMO with review documentation.
				<ul>
					<li>The review documentation material always includes a document
						that describes the review. The minimum contents of the document
						are specified by the individual review types.</li>
					<li>The review documentation must be available in a format that
						anyone in the Eclipse membership can review. PDF
						and HTML are acceptable single formats.</li>
					<li>The review documentation must have a correct copyright
						statement and license.</li>
					<li>The review documentation must be <em>archival quality</em>.
						This means that the materials must be comprehensible and complete
						on their own without requiring explanation by a human presenter,
						reference to a wiki, or to other non-archived web pages.</li>
				</ul></li>
			<li>The EMO announces the review schedule and makes the documentation
				available to the membership-at-large.</li>
		</ol>
		<p>The criteria for the successful completion of each type of review
			will be documented in writing by the EMO in guidelines made available
			via an Eclipse Foundation website. Such guidelines will include, but
			are not limited to the following:</p>
		<ol>
			<li>Clear evidence that the project has vibrant committer, adopter
				and user communities as appropriate for the type of review.</li>
			<li>Reasonable diversity in its committer population as appropriate
				for the type of review. Diversity status must be provided not only
				as number of people/companies, but also in terms of effort provided
				by those people/companies.</li>
			<li>Documented completion of all required due diligence under the <a
				href="<?php echo $ip_policy; ?>">Eclipse IP Policy</a>.</li>
			<li>For graduation and release reviews, the project
				must have a current project plan, in the format specified by the
				EMO, available to the community.</li>
			<li>Balanced progress in creating both frameworks and extensible,
				exemplary tools.</li>
			<li>Showcase the project's quality through project-team chosen
				metrics and measures, e.g., coupling, cyclomatic complexity,
				test/code coverage, documentation of extensions points, etc.</li>
		</ol>
		<p>The review period is open for no less than one week and usually no
			more than two weeks of generally accepted business days.</p>
		<ol>
			<li>The review begins with the EMO's posting of the review materials
				at the start of the review period</li>
			<li>The proper functioning of the Eclipse Development Process is
				contingent on the active participation of the Eclipse members and
				committers, especially in reviews, thus each review has an
				EMO-designated discussion and feedback communication channel: a
				forum/newgroup, a mailing list, or some other public forum.</li>
			<li>If a committer election is required for a review (for example,
				for a <a href="#6_3_1_Creation_Review">creation review</a>), then it
				is held simultaneously with the review period. Thus the election and
				the review will end at the same time, allowing quick and efficient
				provisioning of the resulting project.</li>
			<li>The EMO(ED) approves or fails the review based on the public
				comments, the scope of the project, and the purposes of the Eclipse
				Foundation as defined in the bylaws.</li>
			<li>The review ends with the announcement of the results in the
				defined review communication channel (the EMO(ED) will request that
				the project lead make this announcement).</li>
		</ol>
		<p>If any member believes that the EMO has acted incorrectly in
			approving or failing a review may appeal to the board of directors to review the
			EMO's decision.</p>

		<h4>
			<a name="6_3_1_Creation_Review"></a>6.3.1 Creation Review
		</h4>
		<div class="postit">
			See <a href="creation-review.php">guidelines and checklists</a> about
			creation reviews.
		</div>
		<p>The purpose of the creation review is to assess the community and
			membership response to the proposal, to verify that appropriate
			resources are available for the project to achieve its plan, and to
			serve as a committer election for the project's initial committers.
			The Eclipse Foundation strives not to be a repository of &quot;code
			dumps&quot; and thus projects must be sufficiently staffed for
			forward progress.</p>
		<p>The creation review documents must include short nomination bios of
			the proposed initial committers. These bios should discuss their
			relationship to, and history with, the incoming code and/or their
			involvement with the area/technologies covered by the proposal. The
			goal is to help keep any legacy contributors connected to new project
			and explain that connection to the current and future Eclipse
			membership, as well as justify the initial committers' participation
			in a meritocracy.</p>

		<h4>
			<a name="6_3_2_Graduation_Review"></a>6.3.2 Graduation Review
		</h4>
		<div class="postit">
			See <a href="graduation-review.php">guidelines and checklists</a>
			about graduation reviews.
		</div>
		<p>
			The purpose of the graduation review is to mark a project's change
			from the incubation phase to the mature phase.
		</p>
		<p>The graduation review confirms that the project is/has:</p>
		<ul>
			<li>A working and demonstrable code base of sufficiently high quality.</li>
			<li>Active and sufficiently diverse communities appropriate to the
				size of the graduating code base: adopters, developers, and users.</li>
			<li>Operating fully in the open following the principles and purposes
				of Eclipse.</li>
			<li>A credit to Eclipse and is functioning well within the larger
				Eclipse community.</li>
		</ul>
		<p>A graduation review is generally <a href="#6_3_9_Combining_Reviews">combined</a>
			with a release review.</p>

		<h4>
			<a name="6_3_3_Release_Review"></a>6.3.3 Release Review
		</h4>
		<div class="postit">
			See <a href="release-review.php">guidelines and checklists</a> about
			Release Reviews.
		</div>
		<p>The purposes of a release review are: to summarize the
			accomplishments of the release, to verify that the IP Policy has been
			followed and all approvals have been received, to highlight any
			remaining quality and/or architectural issues, and to verify that the
			project is continuing to operate according to the principles and
			purposes of Eclipse.</p>

		<h4>
			<a name="6_3_4_Promotion_Review"></a>6.3.4 [Reserved]
		</h4>

		<h4>
			<a name="6_3_5_Continuation_Review"></a>6.3.5 [Reserved]
		</h4>

		<h4>
			<a name="6_3_6_Termination_Review"></a>6.3.6 Termination Review
		</h4>
		<div class="postit">
			See <a
				href="http://wiki.eclipse.org/Development_Resources/HOWTO/Review_Information_for_Project_Leads#Termination_.28Archive.29_Reviews">Termination
				Review &quot;How To&quot;</a> for more information.
		</div>
		<p>The purpose of a termination review is to provide a final
			opportunity for the committers and/or Eclipse membership to discuss
			the proposed archiving of a Project. The
			desired outcome is to find sufficient evidence of renewed interest
			and resources in keeping the project active.</p>

		<h4>
			<a name="6_3_7_Move_Review"></a>6.3.7 [Reserved]
		</h4>

		<h4>
			<a name="6_3_8_Restructuring_Review"></a>6.3.8 Restructuring Review
		</h4>

		<p>The purpose of a restructuring review is to notify the community of
			significant changes to one or more projects.
			Examples of &quot;significant changes&quot; include:</p>
		<ul>
			<li>Movement of significant chunks of functionality from one project
				to another.</li>
			<li>Modification of the project structure, e.g. combining multiple
				projects into a single project, or decomposing a single project into
				multiple projects.</li>
			<li>Change of project scope.</li>
		</ul>

		<h4>
			<a name="6_3_9_Combining_Reviews"></a>6.3.9 Combining Reviews
		</h4>

		<p>Reviews can be combined at the discretion of the PMC and EMO.
			Multiple projects may participate in a single review. Similarly,
			multiple review types can be engaged in simultaneously. A parent
			project may, for example, engage in an aggregated release review
			involving itself and some or all of its child projects; a
			consolidated restructuring review may move the code for several
			projects; or a release review may be combined with a graduation
			review. When multiple reviews are combined, the review documentation
			must explicitly state all of the projects and types of reviews
			involved, and include the required information about each.</p>

		<p>It should be noted that the purpose of combining reviews is to
			better serve the community, rather than to reduce effort on the part
			of the project (though it is fortunate when it does both). Combining
			a release and graduation review, or aggregating a release review of a
			project and several of it's child projects generally makes sense.
			Combining release reviews for multiple unrelated projects most likely
			does not.</p>

		<h3>
			<a name="6_4_Releases"></a>6.4 Releases
		</h3>

		<p>
			Any project, with exception of permanent incubators, may make a release.
			A release may include the code from any subset of the project's descendants.
		</p>
		<p>
			<em>(Most of this section is borrowed and paraphrased from the
				excellent <a href="http://www.apache.org/dev/release.html">Apache
					Software Foundation Releases FAQ</a>. The Eclipse community has
				many of the same beliefs about Releases as does the Apache community
				and their words were already excellent. The Apache Software
				Foundation Releases FAQ is distributed under the <a
				href="http://www.apache.org/licenses/LICENSE-2.0">Apache License,
					Version 2.0</a>.)</em>
		</p>
		<p>Releases are, by definition, anything that is distributed outside
			of the  committers of a project. If users are being directed to
			download a build, then that build has been released (modulo the
			exceptions below). All projects and committers must obey the Eclipse
			Foundation requirements on approving any release.</p>
		<p>
			<em>(Exception 1: nightly and integration builds)</em> During the
			process of developing software and preparing a release, various
			nightly and integration builds are made available to the developer
			community for testing purposes. Do not include any links on the
			project website, blogs, wikis, etc. that might encourage
			non-early-adopters to download and use nightly builds, release
			candidates, or any other similar package (links aimed at
			early-adopters and the project's developers are both permitted and
			encouraged). The only people who are supposed to know about such
			packages are the people following the developer mailing list and thus
			are aware of the limitations of such builds.
		</p>
		<p>
			<em>(Exception 2: milestone and release candidate builds)</em>
			Projects are encouraged to use an agile development process including
			regular milestones (for example, six week milestones). Milestones and
			release candidates are &quot;almost releases&quot; intended for
			adoption and testing by early adopters. Projects are allowed to have
			links on the project website, blogs, wikis, etc. to encourage these
			outside-the-committer-circle early adopters to download and test the
			milestones and release candidates, but such communications must
			include caveats explaining that these are not official releases.
		</p>

		<ul>
			<li>Milestones are to be labeled <code>x.yMz</code>, e.g., 2.3M1
				(milestone 1 towards version 2.3), 2.3M2 (milestone 2 towards
				version 2.3), etc.</li>
			<li>Release candidates are to be labeled <code>x.yRCz</code>, e.g.,
				2.3RC1 (release candidate 1 towards version 2.3).</li>
			<li>Official releases are the only downloads allowed to be labeled
				with <code>x.y</code>, e.g., 0.5, 1.0, 2.3, etc.</li>
		</ul>
		<p>
			All official releases must have a successful <a
				href="#6_3_3_Release_Review">release review</a> before being made
			available for download.
		</p>
		<p>
			<em>(Exception 3: bug fix releases with no new features)</em> Bug fix
			releases (x.y.z, e.g., 2.3.1) with no new features over the base
			release (e.g., 2.3) are allowed to be released without an additional
			release review. If a bug fix release contains new features, then the
			project must have a full release review.
		</p>
		<p>Under no circumstances are builds and milestones to be used as a
			substitute for doing proper official releases. Proper release
			management and reviews is a key aspect of Eclipse quality.</p>

		<div class="postit">
			See <a
				href="http://wiki.eclipse.org/Development_Resources/HOWTO/Conforming_Incubation_Branding">Incubation Branding</a> for more information.
		</div>
		<p>Releases for projects in the incubation phase must be labeled
			to indicate the incubation status of the project.</p>

		<h3>
			<a name="6_5_Grievance_Handling"></a>6.5 Grievance Handling
		</h3>
		<p>When a member has a concern about a project, the member will raise
			that concern with the project's leadership. If the member is not
			satisfied with the result, the member can raise the concern with the
			parent project's leadership. The member can continue appeals up the
			project leadership chain and, if still not satisfied, thence to the
			EMO, then the executive director, and finally to the board of directors. All
			appeals and discussions will abide by the guiding principles of being
			open, transparent, and public.</p>

		<p>Member concerns may include:</p>
		<ul>
			<li>Out of scope. It is alleged that a project is exceeding
				its approved scope.</li>
			<li>Dysfunctional. It is alleged that a project is not
				functioning correctly or is in violation of one or more requirements
				of the Eclipse Development Process.</li>
			<li>Contributor appeal. It is alleged that a contributor who
				desires to be a committer is not being treated fairly.</li>
			<li>Invalid veto. It is alleged that a -1 vote on a review is
				not in the interests of the project and/or of Eclipse.</li>
		</ul>
		<p>A variety of grievance resolutions are available to the EMO up to,
			and including, rebooting or restarting a project with new Committers
			and leadership.</p>

		<h2>
			<a name="7_Precedence"></a>7. Precedence
		</h2>
		<p>In the event of a conflict between this document and a
			board of directors-approved project charter, the most recently approved document
			will take precedence.</p>

		<h2>
			<a name="8_Revisions"></a>8. Revisions
		</h2>
		<p>As specified in the bylaws, the EMO is responsible for maintaining
			this document and all changes must be approved by the board of directors.</p>
		<p>Due to the continued evolution of the Eclipse technology, the
			Eclipse community, and the software marketplace, it is expected that
			the Eclipse Development Process (this document) will be reviewed and revised
			on at least an annual basis. The timeline for that review should be
			chosen so as to incorporate the lessons of the previous annual
			coordinate release and to be applied to the next annual coordinated
			release.</p>
		<p>The EMO is further responsible for ensuring that all plans,
			documents and reports produced in accordance with this development
			process be made available to the membership at large via an
			appropriate mechanism in a timely, effective manner.</p>
		<h4>
			<a name="8_1_Revision"></a>8.1 Revision 2.7
		</h4>
		<p>This document was approved by the Eclipse Foundation Board of
			Directors in its meeting on [TBD]. It takes effect (replacing
			all previous versions) on [TBD].</p>

		<h4>
			<a name="8_2_History"></a>8.2 History
		</h4>
		<p>Changes made in this document: </p>
		<ul><?php echo getChanges(); ?></ul>
	</div>
	<!-- midcolumn -->

	<div id="rightcolumn">
		<div class="sideitem">
			<h6>See also</h6>
			<ul>
				<li><a href="<?php echo $pdf; ?>">PDF version of this
						document</a></li>
				<li><a href="<?php echo $previous; ?>">Previous version of this document</a>
				</li>
				<li><a href="<?php echo $diff; ?>">Changes made since the previous version
						of this document</a></li>
				<?= $commentary ? '' : '<li><a href="?commentary">This document with comments</a></li>' ?>
			</ul>
		</div>
	</div>

</div>
<!-- maincontent -->

<?php
	# Paste your HTML content between the EOHTML markers!
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
