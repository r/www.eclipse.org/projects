<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'
/* 221934 - this page to remain on eclipse.org */

$pageTitle 		= "Eclipse Standard Top-Level Charter v1.0";
	
include( '../_commonLeftNav.php' );
ob_start();
?>
     <div id="midcolumn">
        <h1><?= $pageTitle ?></h1>

<p><i>This
document defines standard terms for Eclipse Top Level Project Charters. It is
intended that the Charters for Top Level Projects reference this document rather
than inheriting by copy-and-paste.</i></p>
<p><i>New projects are directed to use a 
more <a href="Eclipse_Standard_TopLevel_Charter_v1.1.php">current version</a> of this document.</i></p>

<h2>Overview</h2>
<i>To be defined in the individual Top
Level Project Charter.</i>

<h2>Mission</h2>
<i>To be defined in the individual Top Level Project Charter.</i>

<h2>Scope</h2>
<i>To be defined in the individual Top Level Project Charter.</i>

<h2>Project Management Committee</h2>
The Projects under this Charter are managed by a group known as the Project
Management Committee (the &quot;PMC&quot;).
<p>PMCs are expected to ensure that:</p>
<ul>
  <li>
    All Projects operate effectively by providing leadership to guide the
    Project's overall direction and by removing obstacles, solving problems, and
    resolving conflicts.
  </li>
  <li>
    All Project plans, technical documents and reports are publicly available
  </li>
  <li>
    All Projects operate using open source rules of engagement: meritocracy,
    transparency, and open participation. These principles work together. In
    principle, anyone can participate in a Project.
  </li>
</ul>
<p>The PMC has the following responsibilities:</p>
<ul>
  <li>Providing the leadership and vision to guide the Project's overall
    direction in a manner consistent with the Eclipse Foundation Roadmap.</li>
  <li>Providing assistance and support to the developers working on the Project
    by removing obstacles, solving problems, and resolving conflicts.</li>
  <li>Ensuring that Project plans are produced, and presenting these plans to
    the EMO.</li>
  <li>Working with the Eclipse Management Organization (the &quot;EMO&quot;) to
    establish the processes and infrastructure needed for the project teams to
    be effective.</li>
  <li>Recommending new Projects to the EMO.</li>
  <li>Recommending the initial set of Project committers for each new Project
    overseen by the PMC, and establishing the procedures consistent with this
    Charter for voting in new committers.</li>
  <li>Helping to ensure that the Projects overseen by the PMC have enough
    contributors, and working to fill vacancies in roles.</li>
  <li>Producing &quot;how to get involved&quot; guidelines to help new potential
    contributors get started.</li>
  <li>Coordinating relationships with other Eclipse Foundation Projects.</li>
  <li>Facilitating code or other donations by individuals or companies.</li>
  <li>Making recommendations to the Eclipse Foundation Board regarding
    contributions proposed under licenses other than the EPL.</li>
  <li>Working with the EMO and Committers to ensure in-bound contributions are
    made in accordance with the Eclipse Foundation IP Policy.</li>
  <li>Acting as a focal point for the community in representing the Projects it
    oversees.</li>
</ul>
<p>The PMC Lead is appointed by the Board. The initial PMC is selected by the
PMC Lead. Thereafter, to become a member of the PMC, an individual must be
nominated by another member of the PMC, and unanimously approved by all PMC
members. </p>
<p>In the unlikely event that a member of the PMC becomes disruptive to the
process or ceases to contribute for an extended period, the member may be
removed by unanimous vote of remaining PMC members. PMC members may resign at
any time by delivering notice of their resignation to the PMC Lead.</p>
<p>The PMC is responsible for producing and maintaining the Project Charter.
Development activities must conform to any rules or processes outlined in the
Charter, so a change to these processes may necessitate a change to the Charter.
Changes to the Charter are approved by the Board.</p>
<p>The work of the PMC is shared by the PMC members. All PMC members are
expected to contribute actively. In particular, PMC members are expected to take
responsibility for overseeing certain areas of work in the Project, and
reporting to the PMC on these areas. Because of the diversity amongst individual
projects, PMC members are not expected to maintain anything other than general
currency with projects outside their assigned technical areas.</p>
<p>Active participation in the user newsgroups and the appropriate developer
mailing lists is a responsibility of all PMC members, and is critical to the
success of the Project. PMC members are required to monitor the main Project
mailing list, and the developer mailing lists for all Projects and components
they are overseeing.</p>

<h2>Roles</h2>
The Projects under this Charter are operated as meritocracies -- the more you
contribute, and the higher the quality of your contribution, the more you are
allowed to do. However with this comes increased responsibility.

<h3>Users</h3>
Users are the people who use the output from the Project. Output will
typically consist of software in form of extensible frameworks and exemplary
tools. Software in this context means intellectual property in electronic form,
including source and binary code, documentation, courseware, reports and
whitepapers.

<h3>Developers</h3>
<p>Users who contribute software, documentation, or other materially useful
content become developers. Developers are encouraged to participate in the user
newsgroup(s), and should monitor the developer mailing list associated with
their area of contribution. When appropriate, developers may also contribute to
development design discussions related to their area of contribution. Developers
are expected to be proactive in reporting problems in the bug tracking system.</p>

<h3>Committers</h3>
<p>Developers who give frequent and valuable contributions to a Project, or
component of a Project (in the case of large Projects), can have their status
promoted to that of a &quot;Committer&quot; for that Project or component
respectively. A Committer has write access to the source code repository for the
associated Project (or component), and gains voting rights allowing them to
affect the future of the Project (or component).</p>
<p>In order for a Developer to become a Committer on a particular Project
overseen by the PMC, another Committer for the same Project (or component as
appropriate) can nominate that Developer or the Developer can ask to be
nominated. Once a Developer is nominated, the Committers for the Project (or
component) will vote for a PMC-designated voting period, and that period shall
be no less than one week. If there are at least 3 positive votes and no negative
votes within the voting period, the Developer is recommended to the PMC for
commit privileges. The PMC may waive the 3 vote minimum requirement in
exceptional cases (e.g., there are fewer than 3 active committers on the
project). If the PMC also approves, and the Developer signs the appropriate New
Committer agreements established by the EMO (wherein, at the very least, the
Developer agrees to abide by the Eclipse Intellectual Property Policy), the
Developer is converted into a Committer and given write access to the source
code and/or web repository for that Project (or component). Becoming a Committer
is a privilege that is earned by contributing and showing discipline and good
judgment. It is a responsibility that should be neither given nor taken lightly.</p>
<p>At times, Committers may become inactive for a variety of reasons. The
decision making process of the Project relies on active committers who respond
to discussions and vote in a constructive and timely manner. The PMC is
responsible for ensuring the smooth operation of the Project. A Committer who is
disruptive, does not participate actively, or has been inactive for an extended
period may have his or her commit status revoked by the PMC.</p>
<p>Active participation in the user newsgroup and the appropriate developer
mailing lists is a responsibility of all Committers, and is critical to the
success of the Project. Committers are required to monitor and contribute to the
user newsgroup.</p>
<p>Committers are required to monitor the mailing lists associated with all
Projects and components for which they have commit privileges. This is a
condition of being granted commit rights to the Project or component. It is
mandatory because committers must participate in votes (which in some cases
require a certain minimum number of votes) and must respond to the mailing list
in a timely fashion in order to facilitate the smooth operation of the Project.
When a Committer is granted commit rights they will be added to the appropriate
mailing lists. A Committer must not be unsubscribed from a developer mailing
list unless their associated commit privileges are also revoked.</p>
<p>Committers are required to track, participate in, and vote on, relevant
discussions in their associated Projects and components. There are three voting
responses: +1 (yes), -1 (no, or veto), and 0 (abstain).</p>
<p>Committers are responsible for proactively reporting problems in the bug
tracking system, and annotating problem reports with status information,
explanations, clarifications, or requests for more information from the
submitter. Committers are responsible for updating problem reports when they
have done work related to the problem.</p>

<h2>Projects</h2>
<p>The work under this Top Level Project is further organized into Projects.
New Projects must be consistent with the mission of the Top Level Project, be
recommended by the PMC, and confirmed by the EMO. Projects can be discontinued
by recommendation of the PMC, and confirmed by the EMO.</p>
<p>When a new Project is created, the PMC nominates a Project lead to act as the
technical leader and nominates the initial set of Committers for the Project,
and these nominations are approved by the EMO. Project leads are accountable to
the PMC for the success of their Project.</p>

<h3>Project Organization</h3>
<p>Given the fluid nature of Eclipse Projects, organizational changes are
possible, in particular: dividing a Project into components; dividing a Project
into two or more independent Projects; and merging two or more Projects into a
single Project. In each case, the initiative for the change may come either from
within the Project or from the PMC, but the PMC must approve any change, and
approval must be confirmed by the EMO.</p>
<p>If a Project wishes to divide into components, commit privileges are normally
granted at the component level, and the committers for a given component vote on
issues specific to that component. Components are established and discontinued
by the PMC. When the PMC creates a component, it appoints a component lead to
act as the technical leader and names the initial set of Committers for the
component. The component lead is designated as a committer for the Project and
represents the component in discussions and votes pertaining to the Project as a
whole. Component committers do not participate in votes at the level of the
Project as a whole, unless they are also the component lead.</p>
<p>In cases where new Projects are being created, either by splitting or by
merging, the usual procedures as set forth in this Charter are followed. In
particular, developers will not necessarily have the same rights after an
organizational change that they enjoyed in the previous structure.</p>

<h2>Infrastructure</h2>
<p>The PMC works with the EMO to ensure the required infrastructure for the
Project. The Project infrastructure will include, at minimum:</p>
<ul>
  <li>Bug Database - Bugzilla database for tracking bugs and feature requests.</li>
  <li>Source Repository -- One or more repositories containing all the
    software for the Projects.</li>
  <li>Website - A website will contain information about the Project, including
    documentation, reports and papers, courseware, downloads of releases, and
    this Charter.</li>
  <li>General Mailing List - Mailing list for discussions pertaining to the
    Project as a whole or that cross Projects. This mailing list is open to the
    public.</li>
  <li>Project Mailing Lists - Mailing list for technical discussions related to
    the Project. This mailing list is open to the public.</li>
  <li>Component Mailing Lists - Mailing list for technical discussions related
    to the component. This mailing list is open to the public.</li>
  <li>Newsgroups - Newsgroups where users, developers, and committers can
    interact regarding general questions and issues about the project. The
    newsgroup is open to the public.</li>
</ul>

<h2>The Development Process</h2>
<p>Each Project lead must produce a development plan for the release cycle, and
the development plan must be approved by a majority of Committers of the
Project. The plan must be submitted to the PMC for review. The PMC may provide
feedback and advice on the plan but approval rests with the Project Committers.</p>
<p>Each Project must identify, and make available on its web site, the
requirements and prioritizations it is working against in the current release
cycle. In addition, each Project must post a release plan showing the date and
content of the next major release, including any major milestones, and must keep
this plan up to date.</p>
<p>The Committers of a Project or component decide which changes may be
committed to the master code base of a Project or component respectively. The
PMC defines the decision process, but that process must include the ability for
Committers to veto the change. The decision process employed may change with the
phase of development.&nbsp; Common decision processes include:</p>
<ul>
  <li>Retroactive - changes are proactively made by Committers but can be vetoed
    by a single Committer.&nbsp;</li>
  <li>Proactive - for efficiency, some code changes from some
contributors (e.g. feature additions, bug fixes) may be approved in advance, or
approved in principle based on an outline of the work, in which case they may be
committed first and changed as needed, with conflicts resolved by majority vote
of the Committers of the Project or component, as applicable.</li>
  <li>Three Positive - No code is committed without a vote; three
+1 ('yes' votes) with no -1 ('no' votes or vetoes) are needed to approve a code
change.&nbsp;</li>
</ul>
<p> Vetoes must be followed by an explanation for the veto within 24 hours
or the veto becomes invalid. All votes are conducted via the developer mailing
list associated with the Project or component. Special rules may be established by the PMC for Projects or components with
fewer than three Committers.&nbsp;</p>
<p>The master copy of the code base must reside on the Project web site where it
is accessible to all users, developers and committers. Committers must check
their changes and new work into the master code base as promptly as possible
(subject to any check-in voting rules that may be in effect) in order to foster
collaboration among widely distributed groups and so that the latest work is
always available to everyone. The PMC is responsible for working with the
Eclipse Foundation to establish a release engineering and build process to
ensure that builds can be reliably produced on a regular and frequent basis from
the master code base and made available for download from the Project web site.
Builds in this context are intended to include not only code but also reports,
documentation, and courseware.</p>
<p>Each Project is responsible for establishing test plans and the level of
testing appropriate for the Project.</p>
<p>All development technical discussions are conducted using the development
mailing lists. If discussions are held offline, then a summary must be posted to
the mailing list to keep the other committers, and any other interested parties,

<h2>Licensing</h2>
<p>All contributions to Projects under this Charter must adhere to the Eclipse
Foundation Intellectual Property Policy.
</p>
</div>
<?php
	# Paste your HTML content between the EOHTML markers!	
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>

