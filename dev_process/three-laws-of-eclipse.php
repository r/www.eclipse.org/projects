<?php
/*******************************************************************************
 * Copyright (C) 2007, 2021 Eclipse Foundation, Inc. and others.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
require_once ($_SERVER ['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");
require_once ($_SERVER ['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php");
require_once ($_SERVER ['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php");
$App = new App ();
$Nav = new Nav ();
$Menu = new Menu ();
include ($App->getProjectCommon ());

$pageTitle = "The Three Laws of Eclipse";

include ('../_commonLeftNav.php');

ob_start ();
?>
<div id="maincontent">
	<div id="midcolumn">
		<h1>The Three Laws of Eclipse</h1>
		<p>
			<em>(With a tip of the hat to <a
				href="http://en.wikipedia.org/wiki/Three_Laws_of_Robotics">Isaac
					Asimov</a>.)
			</em>
		</p>

		<ol>
			<li>A committer may not, through action or inaction, violate IP
				cleanliness</li>
			<li>A committer may not, through action or inaction, disenfranchise
				contributors</li>
			<li>A committer may not, through action or inaction, surprise the
				membership</li>
		</ol>

		<h2>(1) A committer may not, through action or inaction, violate IP
			cleanliness</h2>
		<p>In addition to great open source code, one of the best features of
			the Eclipse Projects is their IP cleanliness. This cleanliness comes
			from:</p>
		<ul>
			<li><em><b>Clean Code.</b></em> Making sure that all incoming code is
				legally contributed.
				<ul>
					<li>Code written by committers is safe.</li>
					<li>Code written by contributors is safe because of the website <a
						href="/legal/termsofuse.php">terms of use</a>.

					</li>
					<li>If a committer suspects that incoming code might not be safe,
						it is their duty to ask the contributor to prove that the
						contributor has the rights to contribute that code.</li>
					<li>Code found in dark alleys is not safe.</li>
				</ul>
				<p></p></li>
			<li><em><b>Approved Libraries.</b></em> Making sure that all
				third-party code included, or merely referenced, is cleared by
				Eclipse Intellectual Property Team through a contribution
				questionnaire. The IP Team checks many things, two of the major ones
				being: (a) is the content clearly licensed? (b) is the license
				compatible with the project license?
				<p></p></li>
			<li><em><b>Paper'ed People.</b></em> Making sure that all committers
				have <a href="https://www.eclipse.org/projects/handbook/#paperwork">committer
					agreements</a> covering each of the committer's projects. The
				agreements say "I have the rights to write code for Eclipse and my
				employer agrees that I have those rights".</li>
		</ul>

		<h2>(2) A committer may not, through action or inaction,
			disenfranchise contributors</h2>
		<p>Eclipse is a community in the best open source principles: open,
			transparent, and driven by a meritocracy. We maintain this community
			by ensuring that everyone has equal opportunity to participate:</p>
		<ul>
			<li><em><b>Public Queue.</b></em> All issues and tasks are tracked
				via the issue track provided to the project team by the Ecipse
				Foundation. Anyone and everyone can see and participate in that
				queue.
				<p></p></li>
			<li><em><b>Available Tools.</b></em> All the source code and all the
				tools used to build a project are available for everyone to use.
				<p></p></li>
			<li><em><b>Transparent Elections.</b></em> Elections for new
				committers are open and public, with full records and
				justifications. Nominations include url references to the
				candidate's contributions (or, for new committers to new projects,
				bios and backgrounds). Committers are not elected "just because they
				work for the company".</li>
		</ul>

		<h2>(3) A committer may not, through action or inaction, surprise the
			membership</h2>
		<p>The Eclipse membership is a key part of the larger Eclipse
			eco-system. With members and project in a symbiotic relationship, it
			is important for both sides to avoid surprising the other. Committers
			minimize surprises through:</p>
		<ul>
			<li><em><b>Open Plans.</b></em> Planning and status reports and
				meetings are open to all.
				<p></p></li>
			<li><em><b>Public Reviews.</b></em> At major events in a project
				lifecycle, <a
				href="/projects/dev_process/development_process.php#6_3_Reviews">reviews</a>
				are held to inform the membership. Major events include any N.N
				release (e.g., 0.5, ..., 1.0, 1.2, ..., 2.1, 2.2, ...) (but not
				N.N.x bug fix releases).
				<p></p></li>
			<li><em><b>Prominent Announcements.</b></em> When starting a major
				new feature or effort that does not require a formal review,
				committers send an <a
				href="/projects/dev_process/notifying-membership.php">announcement</a>
				alerting the members to potential future new cool stuff.</li>
		</ul>
	</div>
</div>

<?php
$html = ob_get_contents ();
ob_end_clean ();

$App->generatePage ( $theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html );
?>