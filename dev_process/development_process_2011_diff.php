<?php
/*******************************************************************************
 * Copyright (c) 208, 2010, 2011 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Bjorn Freeman-Benson (Eclipse Foundation) - initial API and implementation
 *    Wayne Beaton (Eclipse Foundation) - 2010, 2011 updates.
 *******************************************************************************/
require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php");
$App = new App();
$Nav = new Nav();
$Menu = new Menu();
include($App->getProjectCommon()); 

$pageTitle = "Eclipse Development Process 2011 Changes (Draft)";

include( '../_commonLeftNav.php' );

ob_start();
?>
<div id="maincontent">
	<div id="midcolumn">
	<?php echo file_get_contents(dirname(__FILE__) . '/development_process_2011_diff.html')?>
	</div>
	<!-- midcolumn -->
</div>
<!-- maincontent -->

<?php
	# Paste your HTML content between the EOHTML markers!	
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
