<?php
/*******************************************************************************
 * Copyright (c) 2011 Eclipse Foundation and others.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
require_once(dirname(__FILE__) . "/../../eclipse.org-common/system/app.class.php");
$App = new App();

require_once(dirname(__FILE__) . "/../classes/common.php");
require_once(dirname(__FILE__) . "/../classes/debug.php");
require_once(dirname(__FILE__) . "/../classes/Project.class.php");
require_once(dirname(__FILE__) . "/../classes/License.class.inc");

function projectsAsArray($projects) {
	$all = array();
	foreach($projects as $project) {
		$all[$project->getId()] = projectAsArray($project);
	}
	return $all;
}

function formatDate($date) {
	if (empty($date)) return null;
	return date('Y-m-d', $date);
}

function projectAsArray(Project $project) {
	return array(
		'id' => $project->getId(),
		'name' => $project->getFormalName(),
		'shortname' => $project->getShortName(),
		'projecturl' => $project->getUrl(),
		'dataurl' => $project->getDataUrl(),
		'licenses' => License::getLicensesForProject($project->getId()),
		'phase' => $project->getProjectPhase(),
		'provisioned' => formatDate($project->getProvisionedDate()),
 		'children' => projectsAsArray($project->getChildren())
	);
}
?>