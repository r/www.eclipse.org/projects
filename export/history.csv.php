<?php
/*******************************************************************************
 * Copyright (c) 2023 Eclipse Foundation and others.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* http://www.eclipse.org/legal/epl-v10.html
*
* SPDX-License-Identifier: EPL-2.0
*******************************************************************************/

/*
 * The script assumes that it is being invoked via GET.
 */
require_once(dirname(__FILE__) . "/../../eclipse.org-common/system/app.class.php");
$App = new App();
require_once(dirname(__FILE__) . "/../classes/database.inc");
require_once(dirname(__FILE__) . "/../classes/common.php");
require_once(dirname(__FILE__) . "/../classes/debug.php");

$sql = <<< EOQ
	select 
		p.ProjectId as id,
		p.ParentProjectId as parent,
		p.Name as name,
		if(IsActive,'true','false') as active,
		min(pp.activeDate) as start, 
		if(IsActive,null,max(pp.inactiveDate)) as end
	from Projects as p 
		left join PeopleProjects as pp on p.ProjectId=pp.ProjectId and pp.Relation='cm'
	where p.ProjectId not like 'foundation-internal%' 
	group by p.ProjectId;
EOQ;

header("Content-type: text/csv");

$fp = fopen('php://output', 'w');
fputcsv($fp, array('id','parent','name','active','start','end'));
query('foundation', $sql, array(), function($row) use (&$fp) {
	fputcsv($fp, $row);
});
fclose($fp);
?>