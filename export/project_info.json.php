<?php
/*******************************************************************************
 * Copyright (c) 2016 Eclipse Foundation and others.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* http://www.eclipse.org/legal/epl-v10.html
*
* Contributors:
*    Wayne Beaton (Eclipse Foundation)- initial API and implementation
*******************************************************************************/

/*
 * Export information about a project. This exists primarily to
 * feed data into slack.
 *
 * The script assumes that it is being invoked via GET.
 */
require_once(dirname(__FILE__) . "/../../eclipse.org-common/system/app.class.php");
$App = new App();
require_once(dirname(__FILE__) . "/../classes/Project.class.php");
require_once(dirname(__FILE__) . "/../classes/database.inc");
require_once(dirname(__FILE__) . "/../classes/common.php");
require_once(dirname(__FILE__) . "/../classes/debug.php");

function getProjectBaseUrl($id) {
    return 'https://projects.eclipse.org/projects/' . $id;
}

if (!$id = $_SERVER['REQUEST_METHOD'] == 'GET' ? @$_GET['text'] : @$_POST['text']) {
    $data = array(
        'response_type' => 'ephemeral',
        'text' => "Specify either the full id (e.g. technology.egit) or short name (e.g. egit) of a project." . join("\n", $_GET)
    );

    header('Content-Type: application/json');
    echo json_encode($data);
    exit;
}

$sql = "
    select
        p.id, p.name, count(distinct rev.status)>0 as good
    from Project as p
		left join ProjectReviews as rev on (p.id=rev.project or p.parentId=rev.project)
			and rev.type in ('release','graduation', 'other')
			and rev.status = 'success'
			and rev.date >= date_sub(now(), interval 1 year)
	where
	    (p.id like '%:id%' or p.name like '%:id%')
	group by p.id
    limit 10";

$projects = array();
query('dashboard', $sql, array(':id' => $id), function($row) use (&$projects) {
    $id = $row['id'];
    if ($project = Project::getProject($id)) {
	    $name = $project->getFormalName();
	    $url = $project->getUrl();
	    $good = $row['good'] ? "EDP: good" : "EDP: review required";
	    $projects[] = "{$name} [<{$url}|overview>|<{$url}/downloads|downloads>|<{$url}/who|who>] {$good}";
    }
});

if (!$projects) {
    $data = array(
        'response_type' => 'ephemeral',
        'text' => "No projects found."
    );

    header('Content-Type: application/json');
    echo json_encode($data);
    exit;
}


$data = array(
    'response_type' => 'ephemeral',
    'text' => join("\n", $projects)
);

header('Content-Type: application/json');
echo json_encode($data);
?>