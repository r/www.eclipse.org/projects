<?php
/*******************************************************************************
 * Copyright (c) 2017 Eclipse Foundation and others.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* http://www.eclipse.org/legal/epl-v10.html
*******************************************************************************/

/*
 * NOT API. This script may change in the future.
 *
 * This script exports a list of all known Git source code repositories.
 * Essentially, this is the list of all repositories that are registered
 * by projects in their metadata.
 *
 * KEEP THIS FILE. As of September 2023, this is used by our metrics
 * gathering process.
 *
 * TODO This is a candidate for inclusion in api.eclipse.org
 */

require_once(dirname(__FILE__) . "/../../eclipse.org-common/system/app.class.php");
$App = new App();

require_once dirname(__FILE__) . '/../classes/debug.php';
require_once dirname(__FILE__) . '/../classes/database.inc';

$where = array("1 = 1");
$args = array();

if ($id = @$_GET['id']) {
	$where[] = "project = ':id:'";
	$args[':id:'] = $id;
}

$where = implode(" and ", $where);

/*
 * There is some chance that a repository (path) may be claimed by two projects.
 * This should be unlikely, but may happen if the repository data is not specified
 * correctly in the PMI.
 *
 * We mitigate against this with a hack that should have reasonably good results.
 * The query favours the longest project id based on an assumption that this sort
 * of scenario may happen when a project and its incubator (for example) share
 * a GitHub org. When, for example, a repository is claimed by both `automotive.leda`
 * and `automotive.leda.ledaincubator`, we assume that the repository is actually
 * owned by the latter.
 *
 * We do periodically scan for and resolve these sorts of anomalies.
 */
$sql = <<< EOQ
select 
	substring_index(group_concat(distinct project order by length(project) desc),',',1) as project, 
	path 
from GitRepo 
group by path 
having {$where}
EOQ;

$fp = fopen('php://output', 'w');
fwrite($fp, "# NOT API; this script is subject to change without notice.\n");

query('dashboard', $sql, $args, function($row) use (&$fp) {
 $project = $row['project'];
 $path = trim($row['path']);
 if (preg_match('/^\/gitroot\//',$path))
  $path = "git://git.eclipse.org{$path}";
 fputcsv($fp, array($project, $path));
});

fclose($fp);
