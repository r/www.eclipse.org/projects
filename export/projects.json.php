<?php
/*******************************************************************************
 * Copyright (c) 2011 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Wayne Beaton (Eclipse Foundation)- initial API and implementation
 *******************************************************************************/
require_once(dirname(__FILE__) . "/projects.json.inc");

//mustBeEclipseFoundationCaller();
//header("Content-type: application/json");

$projects_array = projectsAsArray(Project::getTopLevelProjects());
$count = $_GET['count'];
if (isset($count)){
	$projects_array = array_slice($projects_array, 0, $count);
}

echo json_encode($projects_array);

?>