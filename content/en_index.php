<?php
/**
 * Copyright (c) 2014, 2018 Eclipse Foundation and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *    Christopher Guindon (Eclipse Foundation) - initial API and implementation
 *    Eric Poirier (Eclipse Foundation)
 *
 * SPDX-License-Identifier: EPL-2.0
 */
?>

<div class="step-by-step margin-bottom-30">
  <div class="container">
    <div class="row intro">
      <div class="col-xs-24">
        <h2>Participate &amp; Contribute</h2>
        <p>Get involved in Eclipse projects to help contribute to their success.<br/>
        We welcome users and adopters as part of the community.</p>
      </div>
    </div>
    <div class="row step-by-step-timeline">
      <div class="col-sm-6 step">
        <a class="step-icon" href="https://www.eclipse.org/contribute/"><i data-feather="help-circle" stroke-width="1"></i></a>
        <p><a href="https://www.eclipse.org/contribute/" class="btn btn-info">How to contribute</a></p>
      </div>

      <div class="col-sm-6 step">
        <a class="step-icon" href="https://www.eclipse.org/projects/handbook/#starting"><i data-feather="zap" stroke-width="1"></i></a>
        <p><a href="https://www.eclipse.org/projects/handbook/#starting" class="btn btn-info">Start a new project</a></p>
      </div>

      <div class="col-sm-6 step">
        <a class="step-icon" href="https://www.eclipse.org/projects/handbook"><i data-feather="book-open" stroke-width="1"></i></a>
        <p><a href="https://www.eclipse.org/projects/handbook" class="btn btn-info">Running a project</a></p>
      </div>

      <div class="col-sm-6 step">
        <a class="step-icon" href="https://www.eclipse.org/projects/project_activity.php"><i data-feather="activity" stroke-width="1"></i></a>
        <p><a href="https://www.eclipse.org/projects/project_activity.php" class="btn btn-info">Project Activity</a></p>
      </div>
    </div>
  </div>
</div>
