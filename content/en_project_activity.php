<?php
/*******************************************************************************
 * Copyright (c) 2014 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Christopher Guindon (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>
<div id="maincontent">
	<h1><?php print $pageTitle;?></h1>

	<p>The Eclipse Foundation is home to the Eclipse IDE, Jakarta EE, and
		hundreds of open source projects, including runtimes, tools,
		specifications, and frameworks for cloud and edge applications, IoT,
		AI, automotive, systems engineering, open processor designs, and many
		others.</p>

	<div style="display: inline-block;">
		<div class="col-sm-8">
			<?php include 'web-parts/proposals.php'; ?>
		</div>
		<div class="col-sm-8">
			<?php include 'web-parts/reviews.php'; ?>
		</div>
		<div class="col-sm-8">
			<?php include 'web-parts/activity.php'; ?>
		</div>
	</div>
</div>
