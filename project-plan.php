<?php
/*******************************************************************************
 * Copyright (c) 2024 Eclipse Foundation and others.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

/*
 * I've retired the previous version of this file rather than continue to
 * support it. In its place, I've set  up some redirects.
 *
 * When the <code>planurl</code> parameter is specified, then we just
 * redirect to that URL and let the XSL formatting magic do its thing.
 *
 * Note that in order to support the XML formatting magic, we need to keep
 * the following files around (I'm listing them here so, when -- at some
 * future time -- we decide to search to see whether or not those files can be
 * deleted, we should at least get this one hit to explain why they exist):
 *
 * <ul>
 * <li>project-plan.xsl</li>
 * <li>project-plan-bugzilla.xsl</li>
 * <li>project-plan-render.xsl</li>
 * </ul>
 *
 * When we've been provided with a valid project id, we redirect to that
 * project's PMI page.
 *
 * Otherwise, just redirect to projects.eclipse.org.
 */

require_once dirname(__FILE__) . "/classes/common.php";
if(isset($_REQUEST['planurl'])) {
	header("Location: {$_REQUEST['planurl']}");
	exit;
} elseif(isset($_REQUEST['projectid'])) {
	if (isValidProjectId($projectid = $_REQUEST['projectid'])) {
		header("Location: https://projects.eclipse.org/projects/{$projectid}");
		exit;
	}
}

header("Location: https://projects.eclipse.org");