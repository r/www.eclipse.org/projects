<?php
/**
 * Copyright (c) 2022 Eclipse Foundation and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");
$App = new App();

require_once dirname(__FILE__) . '/classes/Project.class.php';

if (isset($_GET['id'])) $id = $_GET['id'];
else if (isset($_GET['projectid'])) $id = $_GET['projectid'];

if (Project::getProject($id)) {
	header("Location:https://projects.eclipse.org/projects/{$id}");
} else {
	header("Location:https://projects.eclipse.org");
}