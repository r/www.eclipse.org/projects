<?php
/*******************************************************************************
* Copyright (c) 2013 Eclipse Foundation and others.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* http://www.eclipse.org/legal/epl-v10.html
*
* Contributors:
*    Wayne Beaton (Eclipse Foundation)- initial API and implementation
*******************************************************************************/

if (isset($_GET['release'])) {
	$release = $_GET['release'];
	header("Location:http://projects.eclipse.org/releases/$release");
} else {
	header("Location:http://projects.eclipse.org/releases");
}

?>
