<?php
/**
 * Copyright (c) 2010, 2018 Eclipse Foundation and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *    Wayne Beaton (Eclipse Foundation)- initial API and implementation
 *    Christopher Guindon (Eclipse Foundation) - Re-implementation for the new solstice theme
 *
 * SPDX-License-Identifier: EPL-2.0
 */

require_once ($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");

$App = new App();
$App->PageRSS = "/projects/reviews-rss.php";
$Theme = $App->getThemeClass();
//$Theme->removeAttributes('breadcrumbs', 'breadcrumbs-default-margin');

$pageTitle = "Projects Gateway";
$Theme->setPageTitle($pageTitle);
$Theme->setPageKeywords("Eclipse, projects, plugin, plugins, download, plan");

// Custom theme variables
$variables = array();
$variables['main_container_classes'] = 'container-fluid';
$Theme->setThemeVariables($variables);

ob_start();
?>
<div class="jumbotron featured-jumbotron margin-bottom-0">
  <div class="container">
    <div class="row">
      <div class="col-md-20 col-md-offset-2 col-sm-18 col-sm-offset-3">
        <h1>Discover</h1>
        <p>Find an Eclipse open source project.</p>
        <div class="row row-no-gutters">
          <div class="col-xs-8">
            <form id="form-discover-search" class="form-inline form-search-projects custom-search-form margin-bottom-10 w-100" role="form" action="https://projects.eclipse.org/">
              <div class="input-group w-100">
                <input id="discover-search-box" type="text" size="25" name="search" class="form-control text-center" placeholder="Search projects">
                <span class="input-group-btn">
                <button class="btn btn-primary" aria-label="Search Projects" type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
                </span>
              </div>
              <!-- /input-group -->
            </form>
          </div>
          <div class="col-xs-4 col-xs-offset-1 col-sm-offset-0">
            <a class="btn btn-info" style="min-width: unset;" href="//projects.eclipse.org">List of projects</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<style>
  .featured-jumbotron .input-group .form-control {
    height: unset;
    border-radius: 0;
  }

  .form-search-projects {
    display: inline-block;
  }
</style>
<?php
$extra_header_html = ob_get_clean();
$Theme->setExtraHeaderHtml($extra_header_html);

// Place your html content in a file called content/en_pagename.php
ob_start();
include("content/en_" . $App->getScriptName());
$html = ob_get_clean();
$Theme->setHtml($html);
$Theme->setAttributes('header-wrapper', 'header-default-bg-img');
$Theme->removeAttributes('breadcrumbs', 'breadcrumbs-default-margin');
$Theme->setExtraHeaders('<link rel="canonical" href="https://eclipse.dev/" />');
$Theme->setExtraHeaders('<meta name="original-source" content="https://eclipse.dev/" />');
# Generate the web page
$Theme->generatePage();
