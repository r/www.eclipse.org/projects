<?php
/*******************************************************************************
 * Copyright (c) 2022 Eclipse Foundation and others.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/*
 * EXPERIMENTAL
 *
 * This script answers a GitHub badge which indicates the review status of
 * a particular project. Our logic is that if it has successfully completed a
 * review in the last year. The badge indicates the date of the last review
 * according to our records. The badge is green when the project is in good
 * standing, red when it is due for a review, or yellow when we're not sure.
 *
 * We use https://shields.io/ to generate the badges (but serve the actual
 * logos as SVGs locally.
 */

require_once ($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");
require_once dirname(__FILE__) . '/../classes/common.php';
require_once dirname(__FILE__) . '/../classes/database.inc';

$App = new App ();

$sql = <<<EOQ
select
	max(rev.date) as review,
	max(rev.date) >= date_sub(now(), interval 1 year) as goodstanding
from Project as p
	join ProjectReviews as rev on p.id=rev.project
where
	p.id = ':id:'
	and rev.type in ('progress', 'release','graduation', 'other')
	and rev.status = 'success'
EOQ;

// https://img.shields.io/badge/EDP%20Review-unknown-yellow
$badge = '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="136" height="20" role="img" aria-label="EDP Review: unknown"><title>EDP Review: unknown</title><linearGradient id="s" x2="0" y2="100%"><stop offset="0" stop-color="#bbb" stop-opacity=".1"/><stop offset="1" stop-opacity=".1"/></linearGradient><clipPath id="r"><rect width="136" height="20" rx="3" fill="#fff"/></clipPath><g clip-path="url(#r)"><rect width="75" height="20" fill="#555"/><rect x="75" width="61" height="20" fill="#dfb317"/><rect width="136" height="20" fill="url(#s)"/></g><g fill="#fff" text-anchor="middle" font-family="Verdana,Geneva,DejaVu Sans,sans-serif" text-rendering="geometricPrecision" font-size="110"><text aria-hidden="true" x="385" y="150" fill="#010101" fill-opacity=".3" transform="scale(.1)" textLength="650">EDP Review</text><text x="385" y="140" transform="scale(.1)" fill="#fff" textLength="650">EDP Review</text><text aria-hidden="true" x="1045" y="150" fill="#010101" fill-opacity=".3" transform="scale(.1)" textLength="510">unknown</text><text x="1045" y="140" transform="scale(.1)" fill="#fff" textLength="510">unknown</text></g></svg>';

// Note that the query function escapes substitution parameters
// (so using the $_GET value as below is safe).
query('dashboard', $sql, array (':id:' => $_GET['id']), function ($row) use (&$badge) {
	if ($row['review']) {
		$badge = $row['goodstanding'] == "1"
			// https://img.shields.io/badge/EDP%20Review-2022.12.12-green
			? '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="148" height="20" role="img" aria-label="EDP Review: 2022.12.12"><title>EDP Review: 2022.12.12</title><linearGradient id="s" x2="0" y2="100%"><stop offset="0" stop-color="#bbb" stop-opacity=".1"/><stop offset="1" stop-opacity=".1"/></linearGradient><clipPath id="r"><rect width="148" height="20" rx="3" fill="#fff"/></clipPath><g clip-path="url(#r)"><rect width="75" height="20" fill="#555"/><rect x="75" width="73" height="20" fill="#97ca00"/><rect width="148" height="20" fill="url(#s)"/></g><g fill="#fff" text-anchor="middle" font-family="Verdana,Geneva,DejaVu Sans,sans-serif" text-rendering="geometricPrecision" font-size="110"><text aria-hidden="true" x="385" y="150" fill="#010101" fill-opacity=".3" transform="scale(.1)" textLength="650">EDP Review</text><text x="385" y="140" transform="scale(.1)" fill="#fff" textLength="650">EDP Review</text><text aria-hidden="true" x="1105" y="150" fill="#010101" fill-opacity=".3" transform="scale(.1)" textLength="630">2022.12.12</text><text x="1105" y="140" transform="scale(.1)" fill="#fff" textLength="630">2022.12.12</text></g></svg>'
			//https://img.shields.io/badge/EDP%20Review-2022.12.12-red
			: '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="148" height="20" role="img" aria-label="EDP Review: 2022.12.12"><title>EDP Review: 2022.12.12</title><linearGradient id="s" x2="0" y2="100%"><stop offset="0" stop-color="#bbb" stop-opacity=".1"/><stop offset="1" stop-opacity=".1"/></linearGradient><clipPath id="r"><rect width="148" height="20" rx="3" fill="#fff"/></clipPath><g clip-path="url(#r)"><rect width="75" height="20" fill="#555"/><rect x="75" width="73" height="20" fill="#e05d44"/><rect width="148" height="20" fill="url(#s)"/></g><g fill="#fff" text-anchor="middle" font-family="Verdana,Geneva,DejaVu Sans,sans-serif" text-rendering="geometricPrecision" font-size="110"><text aria-hidden="true" x="385" y="150" fill="#010101" fill-opacity=".3" transform="scale(.1)" textLength="650">EDP Review</text><text x="385" y="140" transform="scale(.1)" fill="#fff" textLength="650">EDP Review</text><text aria-hidden="true" x="1105" y="150" fill="#010101" fill-opacity=".3" transform="scale(.1)" textLength="630">2022.12.12</text><text x="1105" y="140" transform="scale(.1)" fill="#fff" textLength="630">2022.12.12</text></g></svg>';
		$badge = str_replace("2022.12.12", date("Y.m.d", strtotime($row['review'])), $badge);
	}
});

header('Content-Type: image/svg+xml');
echo $badge;
?>