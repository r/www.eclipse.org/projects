<?php
/*******************************************************************************
 * Copyright (c) 2023 Eclipse Foundation and others.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
/*
 * EXPERIMENTAL
 *
 * This script dumps what we know about various metadata files in project
 * repositories. That is, it identifies what we believe are the license, notice,
 * code of conduct, ... files in each of a project's known repositories.
 */

require_once ($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");
require_once dirname(__FILE__) . '/../classes/Project.class.php';
require_once dirname(__FILE__) . '/../classes/common.php';
require_once dirname(__FILE__) . '/../classes/debug.php';
require_once dirname(__FILE__) . '/../classes/database.inc';

$App = new App ();

$id = @$_GET ['id'];
if (!isset($id) || !isValidProjectId ( $id )) {
	echo json_encode(null);
	exit;
}

$data = array('projects' => array());

$data['meta']['warning'] = 'This is not an officially supported API. Use at your own risk.';
$data['meta']['description'] = <<< EOD
This what we know about various metadata files in a project repositories. That is, it identifies what our scripts have identified as the license, notice, code of conduct, ... files in each of a project's known repositories. The 'types' field lists the categories of the files that you might find (e.g., a 'LICENSE' file). Not every repository necessarily has every type of file.
EOD;

$sql = <<< EOQ
	select distinct type from ProjectMetadata
EOQ;

query('dashboard', $sql, array(), function($row) use (&$data) {
	$data['meta']['types'][] = $row['type'];
});

query('dashboard','select max(ts) as ts from GitRepo', array(), function($row) use (&$data) {
	$data['meta']['capture_date'] = $row['ts'];
});

$sql = <<< EOQ
		select
			gr.project, gr.path, pm.type, pm.file
		from ProjectRollup as pr
		join GitRepo as gr on pr.subproject=gr.project
			left join ProjectMetadata as pm on gr.path=pm.path and pm.type != ''
		where pr.project=':id:'
		order by gr.path
EOQ;

$args = array(':id:' => $id);
query('dashboard', $sql, $args, function($row) use (&$data) {
	if (!$data['projects'][$row['project']][$row['path']]) {
		$data['projects'][$row['project']][$row['path']] = array();
	}
	if ($row['type']) {
		$data['projects'][$row['project']][$row['path']][$row['type']][] = $row['file'];
	}
});

$sql = <<< EOQ
	select distinct
		gr.project, gr.path
	from GitRepo as gr
	where gr.project=':id:';
EOQ;

query('dashboard', $sql, $args, function($row) use (&$data) {
	$data['projects'][$row['project']]['repositories'][] = $row['path'];
});

$sql = <<< EOQ
	select company, count from ProjectCompanyActivity where project=':id:' order by count desc
EOQ;

query('dashboard', $sql, $args, function($row) use (&$data, $id) {
	$data['projects'][$id]['chart_data']['company_activity_last_three_months'][] = $row;
});

$sql = <<< EOQ
	select login, SUM(count) as count from ProjectCommitterActivity where project=':id:' group by login order by count desc
EOQ;

query('dashboard', $sql, $args, function($row) use (&$data, $id) {
	$data['projects'][$id]['chart_data']['committer_activity_last_three_months'][] = $row;
});

$sql = <<< EOQ
    select
      periods.period as period,
      if (commits.count is null, 0, commits.count) as count
    from (
      select distinct period
      from ProjectCommitActivity
      where
        period
          between (
            select
              if(max(period) < date_format(date_sub(now(), interval 1000 month),'%Y%m'),
                date_format(now(),'%Y%m'),
                greatest(min(period), date_format(date_sub(now(), interval 1000 month),'%Y%m')))
            from ProjectCommitActivity
            where project=':id:')
          and date_format(now(),'%Y%m')) as periods
    left join
      ProjectCommitActivity as commits
      on (periods.period = commits.period and commits.project=':id:')
      order by period
EOQ;

query('dashboard', $sql, $args, function($row) use (&$data, $id) {
	$data['projects'][$id]['chart_data']['monthly_activity_lifetime'][] = $row;
});

echo json_encode($data);
?>