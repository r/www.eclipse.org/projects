<?php
/*******************************************************************************
 * Copyright (c) 2022 Eclipse Foundation and others.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

/*
 * THIS IS NOT API. EXPERIMENTAL.
 */
require_once(dirname(__FILE__) . "/../../eclipse.org-common/system/app.class.php");
$App = new App();

require_once dirname(__FILE__) . '/../classes/common.php';
require_once dirname(__FILE__) . "/../classes/database.inc";
require_once dirname(__FILE__) . "/../classes/debug.php";

function getOrtType($value) {
	switch ($value) {
		case 'maven' : return 'Maven';
		case 'pypi' : return 'PyPi';
		case 'npm' : return 'NPM';
	}
	return null;
}

function getAuthority($value) {
	if (preg_match("/^CQ(?<cq>\d+)$/", $value, $matches)) {
		return "https://dev.eclipse.org/ipzilla/show_bug.cgi?id={$matches['cq']}";
	}

	if (preg_match("/^#(?<iplab>\d+)$/", $value, $matches)) {
		return "https://gitlab.eclipse.org/eclipsefdn/emo-team/iplab/-/issues/{$matches['iplab']}";
	}

	return null;
}

function getCurationComment($value) {
	$url = getAuthority($value);
	if ($url) {
		return "Curated by the Eclipse Foundation, see {$url}.";
	}
	return "Curated by the Eclipse Foundation.";
}

$sql = "
	select
		id, license, authority
	from ThirdPartyLicense
	where
		substring_index(id,'/',1) in ('maven','npm','pypi')
		and status='approved'
		and license is not null
		and license != '';";

query("dashboard", $sql, array(), function($row) {
	$matches = null;
	if (preg_match('/^(?<type>[^\/]+)\/(?<source>[^\/]+)\/(?<namespace>[^\/]+)\/(?<name>[^\/]+)\/(?<revision>[^\/]+)$/',$row['id'],$matches)) {
		$type = getOrtType($matches['type']);
		$namespace = $matches['namespace'] == '-' ? '' : $matches['namespace'];
		$id = "{$type}:{$namespace}:{$matches['name']}:{$matches['revision']}";
		$comment = getCurationComment($row['authority']);

		// Apparently ORT has trouble with a "WITH" statement that is not uppercase.
		$license = $row['license'];
		$license = preg_replace('/ WITH /i',' WITH ',$row['license']);

		echo "- id: \"{$id}\"\n  curations:\n    comment: {$comment}\n    concluded_license: \"{$license}\"\n";
	}
});
?>