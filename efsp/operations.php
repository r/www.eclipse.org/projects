<?php
/*******************************************************************************
 * Copyright (c) Eclipse Foundation and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php");
$App = new App();
$Nav = new Nav();
$Menu = new Menu();

$pageTitle = "Eclipse&nbsp;Foundation Specification&nbsp;Process Operations&nbsp;Guide";
$pageAuthor = "Wayne Beaton";
$pageKeywords = "EFSP, Specifications";

include($App->getProjectCommon());

$fileName = dirname(__FILE__) . '/content/operations.html';

ob_start();
?>

<link rel="stylesheet" href="/projects/handbook/resources/handbook.css"/>

<div id="maincontent">
	<h1><?php echo $pageTitle; ?></h1>
	<?php include $fileName; ?>
</div>

<?php
	$html = ob_get_contents();
	ob_end_clean();
	$App->generatePage(null, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
