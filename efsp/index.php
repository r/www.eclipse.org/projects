<?php
/*******************************************************************************
 * Copyright (c) Eclipse Foundation and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
switch (@$_GET['version']) {
	case '1.0' : header('Location: /projects/efsp/efsp_1.0'); break;
	case '1.1' : header('Location: /projects/efsp/efsp_1.1'); break;
	case '1.2' : header('Location: /projects/efsp/efsp_1.2'); break;
	case '1.3' : header('Location: /projects/efsp/efsp_1.3'); break;
	case '1.3.1' : header('Location: /projects/efsp/efsp_1.3.1'); break;
	default: include dirname(__FILE__) . '/efsp_1.4/index.php'; break;
}
