<?php
/*******************************************************************************
 * Copyright (c) Eclipse Foundation and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php");

$App = new App();
$Nav = new Nav();
$Menu = new Menu();
include($App->getProjectCommon());

$pageTitle = "Eclipse Foundation Specification Process Changes";
$pageAuthor = "Wayne Beaton";
$pageKeywords = "EDP, Specifications";

ob_start();
?>

<link rel="stylesheet" href="/projects/handbook/resources/handbook.css"/>

<div id="maincontent" >
	<h1><?php echo $pageTitle; ?></h1>
	<p><em>This document highlights differences between this version of the specification process and the last.
	Additions are underlined and coloured green; deletions are struck through and coloured red.</em></p>
	<hr/>
	<?php include dirname(__FILE__) . '/diff_v1.3.html'; ?>
	<hr/>
</div>

<?php
	$html = ob_get_contents();
	ob_end_clean();
	$App->generatePage(null, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
