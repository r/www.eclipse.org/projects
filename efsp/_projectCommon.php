<?php
/**
 * Copyright (c) 2024 Eclipse Foundation and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

require_once dirname(__FILE__) . '/../_projectCommon.php';

function getEFSPVersion() {
	$matches = null;
	if (preg_match('/\/efsp_(?<version>\d+(?:\.\d+){1,})\/(?:index|diff)\.php$/', $_SERVER['SCRIPT_NAME'], $matches)) {
		return $matches['version'];
	}
	$all = getAllEFSPVersions();
	return end($all);
}

function getAllEFSPVersions() {
	$versions = glob(dirname(__FILE__).'/efsp_*.*');
	$versions = array_map(function($each) {
		$matches = null;
		return preg_match('/\/efsp_(?<version>\d+(?:\.\d+){1,})$/',$each, $matches)
		? $matches ['version']
		: null;
	}, $versions);
	sort($versions);
	return $versions;
}

if ($version = getEFSPVersion()) {
	$versions = getAllEFSPVersions();
	$current = array_search($version, $versions);
	$prev = $versions[$current-1];
	$next = $versions[$current+1];

	$Nav->addNavSeparator("Other Versions", null);
	if ($prev) $Nav->addCustomNav("EFSP {$prev} (previous)", "/projects/efsp/efsp_{$prev}", "_self", 1);
	if ($next) $Nav->addCustomNav("EFSP {$next} (next)", "/projects/efsp/efsp_{$next}", "_self", 1);

	$diff = "/projects/efsp/efsp_{$version}/diff.php";
	if (file_exists(dirname(__FILE__) . '/../..' . $diff))
		$Nav->addCustomNav("Compare {$version} with {$prev}", $diff, "_self", 1);
}
