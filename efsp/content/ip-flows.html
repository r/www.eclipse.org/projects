<div id="preamble">
<div class="sectionbody">
<div class="paragraph">
<p>2023-02-24</p>
</div>
<div class="paragraph">
<p>FOR ILLUSTRATION PURPOSES ONLY</p>
</div>
<div class="paragraph">
<p>This FAQ provides a general overview of the Eclipse Foundation’s specification process (EFSP), in particular in relation to the intellectual property flows associated with specification. It is not intended to be exhaustive, but rather to provide answers to many commonly asked questions and to provide examples that might provide further insight. It is also not intended to serve as legal advice, and readers are encouraged to seek independent legal counsel in relation to these flows. The relevant documents, which can be found or are linked to on our <a href="https://www.eclipse.org/org/documents/">governance documents</a> page and our <a href="https://www.eclipse.org/legal">legal resources</a> page, include:</p>
</div>
<div class="ulist">
<ul>
<li>
<p><a href="https://www.eclipse.org/projects/efsp">Eclipse Foundation Specification Process</a>;</p>
</li>
<li>
<p><a href="http://eclipse.org/org/documents/Eclipse_IP_Policy.pdf">Eclipse Foundation IP Policy</a>;</p>
</li>
<li>
<p><a href="https://www.eclipse.org/legal/committer_process/EclipseMemberCommitterAgreement.pdf">Member Committer and Contributor Agreement</a>;</p>
</li>
<li>
<p><a href="https://www.eclipse.org/legal/efsl.php">Eclipse Foundation Specification License</a>; and</p>
</li>
<li>
<p><a href="https://www.eclipse.org/legal/tck.php">Eclipse Foundation Technology Compatibility Kit License</a></p>
</li>
</ul>
</div>
</div>
</div>
<div class="sect2">
<h3 id="efsp-ipflows-faq"><a class="anchor" href="#efsp-ipflows-faq"></a><a class="link" href="#efsp-ipflows-faq">Frequently Asked Questions</a></h3>
<div class="dlist">
<dl>
<dt class="hdlist1">Does the Eclipse Foundation produce specifications today?  </dt>
<dd>
<p>Yes. The Eclipse Foundation has a fully functional specification process that is used by a number of working groups to produce high-quality (IP-clean) specifications such as AsciiDoc, MicroProfile, Jakarta EE, and Sparkplug. Jakarta EE is by far the biggest example of open source specifications in place today. As the successor to Java EE, Jakarta EE is both the open source specification and compatibility mark for a multi-billion dollar ecosystem, with the major suppliers in that ecosystem all participating. At this time, Jakarta EE has close to fifty individual specification projects, three of which are deemed as “Platforms”.</p>
</dd>
<dt class="hdlist1">Does the Eclipse Foundation have its own specification process?  </dt>
<dd>
<p>The Eclipse Foundation has a well-defined <a href="https://www.eclipse.org/projects/efsp">specification process</a> that covers the full cycle of open specification development. The specifications themselves are developed as open source projects governed under the Eclipse Public License 2.0 (<code>EPL-2.0</code>), the Apache License 2.0 (<code>Apache-2.0</code>), or the Eclipse Distribution License 1.0 (<code>BSD-3-Clause</code>). When the specifications are completed, they are then re-licensed under the <a href="https://www.eclipse.org/legal/efsl.php">Eclipse Foundation Specification License</a>, which is based on&#8212;&#8203;and is very similar to&#8212;&#8203;the well-known W3C License.</p>
<div class="paragraph">
<p>The Eclipse Foundation is also an ISO JTC1 PAS submitter. As an example of the benefit of this, as of early 2023, the Eclipse Foundation is in the process of turning the Sparkplug specification into an ISO/IEC standard. The specification license permits independent implementations under any form of license, including proprietary ones.</p>
</div>
</dd>
<dt class="hdlist1">Can the specification process be modified for individual industry initiatives?  </dt>
<dd>
<p>The Eclipse Foundation Specification Process is intended to be a template which can be specialized for the needs of different groups. Each working group may establish “customizations” based on a consensus of the working group’s steering committee, subject to approval by the Eclipse Foundation. For example, a working group may choose to have a shorter or longer period for the approval of final specifications. For more details on what and how can be customized, please see the <a href="./operations.php#efspo-specializing">Specializing the EFSP</a>.</p>
</dd>
<dt class="hdlist1">What are the basic elements of an Eclipse specification?  </dt>
<dd>
<p>Each specification requires three components: a specification document (under the specification license referenced above), at least one open source compatible implementation available under one of the Eclipse Public License 2.0 (<code>EPL-2.0</code>), the Apache License 2.0 (<code>Apache-2.0</code>), or the Eclipse Distribution License 1.0 (<code>BSD-3-Clause</code>), and a Technology Compatibility Kit (“TCK”). The TCK is also developed as an open source project and when made final the binary is re-licensed under the <a href="https://www.eclipse.org/legal/tck.php">Eclipse Foundation TCK License</a>. Compatibility is done via self-certification by passing the TCK and making those results publicly available.</p>
<div class="paragraph">
<p>A specification version can only be declared as "final" when all three assets (specification, TCK, compliant implementation) are in place.</p>
</div>
</dd>
<dt class="hdlist1">What patent licenses are used by Eclipse Foundation specifications? </dt>
<dd>
<p>As per the Eclipse Foundation&#8217;s Intellectual Property Policy, specification patent licenses are always on a royalty-free basis. Royalty-free patent grants happen when member companies appoint a Participant representative to the specification project developing the actual specification. Merely joining a working group that does specifications does not implicate a member organization’s patent portfolio. Putting a participant representative into a Specification Project does. See Section VI of the <a href="http://eclipse.org/org/documents/Eclipse_IP_Policy.pdf">IP Policy</a> for specific details.</p>
<div class="paragraph">
<p>There are two choices of patent license: The Compatible Patent License vests the royalty-free grants when an implementation passes the TCK. The Implementation Patent License grants to all implementors of the specification whether they pass the TCK or not. The latter is even more open source friendly because it makes it clear that there is no patent exposure while an OSS implementation is under development. The Implementation Patent License is our default.</p>
</div>
</dd>
<dt class="hdlist1">Patent rights are not addressed in the Contribution or Committer Agreements. How does that work? </dt>
<dd>
<p>Patent grants are not included in any of our contribution or committer agreements. That said, patent licenses are an intrinsic part of our intellectual property management processes. They are simply covered elsewhere.</p>
<div class="paragraph">
<p>For open source projects, royalty free patent licenses are provided via the open source licenses used by the projects. For example, the Eclipse Public License (<code>EPL-2.0</code>) and Apache License (<code>Apache-2.0</code>) licenses have patent provisions. It is important to note that the Eclipse Foundation (unlike the Apache Software Foundation) does not acquire any intellectual property in its projects other than: (a) trademarks, and (b) a limited copyright license to ensure that the Eclipse Foundation has clear rights to turn project contributions into specifications, even if the project is using a copyleft license such as the <code>EPL-2.0</code>.</p>
</div>
<div class="paragraph">
<p>The Eclipse Foundation follows a policy of symmetrical in-bound and out-bound licenses. We accept contributions under the project&#8217;s open source license under the committer and contributor agreements and the DCO as referenced therein, and then license those contributions out to downstream consumers under the same project license. That is why the license provisions under the committer and contributor agreements may seem incomplete: they are in the licenses, not the contribution agreements.</p>
</div>
<div class="paragraph">
<p>For specifications, royalty free patent licenses are granted via the provisions of Section VI of our Intellectual Property Policy, which members are bound to by signing the Membership Agreement. Such patent licenses are triggered by appointing a Participant Representative in a Specification Project chartered by a Working Group. It is important to note that the royalty free patent licenses provided to specifications are far broader than those provided for under the open source licenses. For our specifications, all Participants in a specification project grant royalty-free licenses to all of their patents which would be necessarily infringed by implementers or users of the specification. Such licenses are not tied to their contributions to the specification; they cover the entire specification.</p>
</div>
<div class="paragraph">
<p>At the risk of repeating ourselves, please note that these specification patent licenses are not triggered by joining the working group. They are triggered by direct participation in a specification project. See Section VI of the <a href="http://eclipse.org/org/documents/Eclipse_IP_Policy.pdf">IP Policy</a> for specific details.</p>
</div>
</dd>
<dt class="hdlist1">Is membership required to claim compatibility with a specification? </dt>
<dd>
<p>No. Claims that any implementation is compatible with an Eclipse Foundation Specification can be made after executing and passing the specification’s TCK. Eclipse does have a compatibility program that working groups generating specifications can leverage. Like the specification process, individual working groups may “customize” the compatibility program should they choose. See the Jakarta EE <a href="https://jakarta.ee/compatibility/get-listed/"><em>how to get listed</em></a> document for a check list of what&#8217;s involved to declare a product as Jakarta EE compatible and obtain the Jakarta EE compatibility logo for your products.</p>
<div class="paragraph">
<p>The only time an implementor of a specification ever needs to become a Member of the Eclipse Foundation is if they want to license the compatibility trademark. Some working groups such as MicroProfile don&#8217;t have a compatibility trademark program and rely entirely on open source declarations of compatibility.</p>
</div>
</dd>
<dt class="hdlist1">Does a specification have a reference implementation? And must it be an Eclipse open source project? </dt>
<dd>
<p>We do not have normative reference implementations. By “normative reference implementation” we mean an implementation which can be referenced to provide additional guidance in instances where the specification and/or TCK are ambiguous. We have compatible implementations. The key difference, of course, is that our compatible implementations are not intended to be normative and there is no special implementation that can be turned to as an additional definition of compatibility.</p>
<div class="paragraph">
<p>It is not required that the implementation used to validate the compatible implementation be hosted at the Eclipse Foundation, though it certainly simplifies things if it is. Therefore, it is strongly encouraged that there be an Eclipse implementation of the specification. From a practical point of view, the Eclipse project is typically developed in parallel with the specification. But formally, there must be at least one open source compatible implementation that passes the TCK before the specification can be declared "final".</p>
</div>
</dd>
<dt class="hdlist1">What’s next for specifications at Eclipse? </dt>
<dd>
<p>Next up is to leverage our status as an ISO/IEC JTC1 PAS submitter to turn our specifications into international standards. This is already underway (as of 2023) for the Sparkplug specification for machine interoperability.</p>
<div class="paragraph">
<p>Currently, our specification process is all about software and it is entirely a self-certification model. We have not yet done specifications of cyber-physical systems that combine hardware and software.</p>
</div>
<div class="paragraph">
<p>Also, Eclipse does not run any in-house processes to validate the compatibility of implementations. We have ideas on how to do such things, but like most things we do at Eclipse, we will develop such a process as the needs of a particular industry collaboration require us to do so.</p>
</div>
</dd>
</dl>
</div>
</div>