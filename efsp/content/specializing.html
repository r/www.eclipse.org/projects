<div id="preamble">
<div class="sectionbody">
<div class="paragraph">
<p>A working group may, through their specification committee, choose to specialize the Eclipse Foundation Specification Process (EFSP) for their own implementation. The process document is a foundational document that defines underlying principles, fundamental rules, and other requirements with regard to implementing specifications. The process document does not generally prescribe the use of specific technology, or provide any detail with regard to implementation.</p>
</div>
<div class="paragraph">
<p>This document starts by describing what must not be taken away from the specification process, and concludes with some suggestions of what might be considered for a working group&#8217;s specialization of the process.</p>
</div>
</div>
</div>
<div class="sect1">
<h2 id="minimum-values"><a class="anchor" href="#minimum-values"></a><a class="link" href="#minimum-values">Minimum Values</a></h2>
<div class="sectionbody">
<div class="paragraph">
<p>The most critical aspect of the EFSP is the management of Essential Claims as defined by the Eclipse IP Policy. In this regard, the requirement that all committers be covered by an Eclipse Foundation Membership Agreement and Working Group Participation Agreement cannot be relaxed. By extension, the restrictions placed on Participants and Participant Representatives cannot be relaxed in any customization of the process, nor can the ability of a Participant to appoint a Participant Representative be inhibited in any way.</p>
</div>
<div class="paragraph">
<p>The requirements regarding Scope must not be relaxed. Specifically, the requirements regarding approvals and the requirement that the development work of the project stay with the boundaries defined by the the Scope must not be curtailed.</p>
</div>
<div class="paragraph">
<p>The underlying principles of open source (the so-called “Open Source Rules of Engagement”) may not be curtailed. Specifically, all Specification Projects operate in an open and transparent manner, must follow meritocratic practices to promote individuals to positions of power and authority, and (although not strictly listed as a rule of engagement) operate in a vendor neutral manner.</p>
</div>
<div class="paragraph">
<p>The powers granted to the Project Leadership Chain by the Eclipse Development Process must not be restricted.</p>
</div>
<div class="paragraph">
<p>In general, quantities included in the EFSP and EDP can be increased, but not decreased:</p>
</div>
<div class="ulist">
<ul>
<li>
<p>The period of time required to run a simple ballot (e.g. a committer election) must not be less than seven days (It is generally accepted at a week is a reasonable minimum period of time to run a ballot that meets a minimum standard of community inclusion);</p>
</li>
<li>
<p>Specification committee approval ballots, and ballots that otherwise that require some sort of legal review must not be less than fourteen days to give adequate time for voting members to consult with their legal teams; and</p>
</li>
<li>
<p>Specification Teams must engage in at least one Progress Review during the development cycle of a Major or Minor Release (Progress Reviews are not required for Service Releases).</p>
</li>
</ul>
</div>
</div>
</div>
<div class="sect1">
<h2 id="specializing-the-process"><a class="anchor" href="#specializing-the-process"></a><a class="link" href="#specializing-the-process">Specializing the Process</a></h2>
<div class="sectionbody">
<div class="paragraph">
<p>The EFSP defines a set of underlying principles and fundamental requirements. It intentionally does not define any sort of practical implementation, or prescribe any specific technologies. Specializations of the process should take a similar approach. The process might, for example, extend the amount of time required for a specification committee ballot; but any attempt to describe the specific mechanisms and technology by which a ballet is run in a practical sense is more of an operational detail that should be defined in an operations document.</p>
</div>
<div class="sect2">
<h3 id="example-process-specializations"><a class="anchor" href="#example-process-specializations"></a><a class="link" href="#example-process-specializations">Example Process Specializations</a></h3>
<div class="paragraph">
<p>Providing a comprehensive list of everything possible thing that can be customized is an impossible task. In place of a comprehensive list, we provide a list of examples of things that might be customized and/or tuned.</p>
</div>
<div class="paragraph">
<p>A customization may extend the list of Open Source Licenses (but many not remove Licenses from the master list).</p>
</div>
<div class="paragraph">
<p>A customization may define requirements for evolving itself to create future versions of the Working Group-specific specification process.</p>
</div>
<div class="paragraph">
<p>The process requires that a Specification Project engage in at least one Progress Review. A customization may:</p>
</div>
<div class="ulist">
<ul>
<li>
<p>Require some specific number of additional Progress Reviews;</p>
</li>
<li>
<p>Specify a maximum and/or minimum period of time required for Specification Committee approval ballot;</p>
</li>
<li>
<p>Specify the period of time that must pass between Reviews; and</p>
</li>
<li>
<p>Describe mitigation steps in the event that a review fails.</p>
</li>
</ul>
</div>
<div class="paragraph">
<p>The process requires that a Specification Project engage in a Release Review. A customization may:</p>
</div>
<div class="ulist">
<ul>
<li>
<p>Specify a maximum and/or minimum period of time required for Specification Committee approval votes;</p>
</li>
<li>
<p>Specify the period of time that must pass between the last Progress Review and the Release Review; and</p>
</li>
<li>
<p>Describe mitigation steps in the event that the review fails.</p>
</li>
</ul>
</div>
<div class="paragraph">
<p>A customization may also define:</p>
</div>
<div class="ulist">
<ul>
<li>
<p>Technical namespaces;</p>
</li>
<li>
<p>Criteria for designating a release as major, minor, or service; and</p>
</li>
<li>
<p>Criteria, requirements, etc. for managing exceptions in a TCK.</p>
</li>
</ul>
</div>
<div class="paragraph">
<p>While generally considered best practices, a customization may prescribe:</p>
</div>
<div class="ulist">
<ul>
<li>
<p>How a Specification is bundled for dissemination;</p>
</li>
<li>
<p>Specific file formats for documentation; and</p>
</li>
<li>
<p>Document structure and style.</p>
</li>
</ul>
</div>
<div class="paragraph">
<p>The EFSP provides no specific criteria for designating a specification as a profile, nor does it attempt to define “platform”. A specialization may choose to provide definitions or specify the criteria for designating a specification as a profile.</p>
</div>
</div>
<div class="sect2">
<h3 id="operational-considerations"><a class="anchor" href="#operational-considerations"></a><a class="link" href="#operational-considerations">Operational Considerations</a></h3>
<div class="paragraph">
<p>Specification committees are encouraged to create an operations document that describes how the process is implemented. The evolution of an operations document tends to be organic, based on building consensus within the team instead of relying on a formal approvals process.</p>
</div>
<div class="paragraph">
<p>Out of convenience, an operations document may repeat information that’s captured in the process; as such, an operations document must include a clear statement that in the event of conflict the process document must be taken as the authority.</p>
</div>
<div class="paragraph">
<p>The practical implementation of aspects of the process are not defined by the EFSP, and so a Working Group Specification Process (customization) may choose to formalize (for example):</p>
</div>
<div class="ulist">
<ul>
<li>
<p>How to run Specification Committee Ballot;</p>
</li>
<li>
<p>How a Participant appoints a Participant Representative;</p>
</li>
<li>
<p>What to do when a ballot fails or approval is not otherwise granted;</p>
</li>
<li>
<p>The mechanism by which a Specification Committee determines whether or not a minor correction made during a ballot changes semantic meaning;</p>
</li>
<li>
<p>How a Specification Version becomes a Final Specification;</p>
</li>
<li>
<p>Requirements/guidelines to pass a Progress Review, along with timing of the review itself; and</p>
</li>
<li>
<p>A standard means of describing relationships between specifications.</p>
</li>
</ul>
</div>
</div>
</div>
</div>