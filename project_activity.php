<?php
/*******************************************************************************
 * Copyright (c) 2010 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Christopher Guindon (Eclipse Foundation) - Initial implementation
 *******************************************************************************/

	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");
	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php");
	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php");

	$App = new App();
	$Menu = new Menu();
	include($App->getProjectCommon());

	$pageTitle 		= "Project Activity News";
	$pageAuthor		= "";
	$pageKeywords	= "Eclipse projects activity, project proposal, upcoming review, recent activity";

	// Place your html content in a file called content/en_pagename.php
	ob_start();
	include("content/en_" . $App->getScriptName());
	$html = ob_get_clean();

	$App->AddExtraHtmlHeader('<link rel="stylesheet" type="text/css" href="/projects/web-parts/projects.css"/>');
	# Generate the web page
	$App->PageRSS = "/projects/reviews-rss.php";
	$App->generatePage(NULL, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
