<?php
/*******************************************************************************
 * Copyright (C) Eclipse Foundation, Inc. and others.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
require_once ($_SERVER ['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");
require_once ($_SERVER ['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php");
require_once ($_SERVER ['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php");

require_once dirname ( __FILE__ ) . "/../classes/debug.php";

$App = new App ();
$Nav = new Nav ();
$Menu = new Menu ();

$pageKeywords = "";
$pageTitle = "Eclipse Foundation Committer Training";
$pageAuthor = "Wayne Beaton";

include ($App->getProjectCommon ());
function added($when, $update=false) {
	global $App;
	if (strtotime ( "+3 months", strtotime ( $when ) ) > time ()) {
		$what = $update ? "Updated" : "Added";
		$formatted = $App->getFormattedDate ( $when );
		echo "<p><span class=\"new_video\">{$what} {$formatted}.</span></p>";
	}
}

ob_start ();
?>
<link rel="stylesheet" href="/projects/handbook/resources/handbook.css" />
<style>
div.followup {
	margin-top: 10px;
}

span.new_video::before {
	content: "New! ";
	color: red;
	font-weight: bold
}
</style>

<div id="maincontent">
	<h1>Eclipse Foundation Committer Training</h1>
	<p>
		The rules of engagement defined in the <a
			href="/projects/dev_process/#2_1_Open_Source_Rules_of_Engagement">Eclipse
			Foundation Development Process</a> are the foundational principles
		that underpin how we believe that open source should be done.
	</p>
	<p>
		Eclipse projects must be <strong>transparent</strong>. To operate in a
		transparent manner, Eclipse project teams need to ensure that the
		community has the ability to understand what the project team is
		doing. This means that all development plans, issues tracking and
		resolution, discussion, and more happens on open channels where people
		who are not a part of the project team can follow along.
	</p>
	<p>
		Eclipse projects must be <strong>open</strong>. Transparency is
		concerned with letting people know what you’re doing, openness is
		concerned with letting them participate as an equal player. As an
		Eclipse project committer, you have to be open to new ideas and work
		with contributors to ensure that their contributions have an equal
		chance of becoming part of the project. Project teams should have
		well-defined rules for participating that apply to everybody
		regardless of who they work for or factors other than the quality of
		their contributions.
	</p>
	<p>
		Eclipse projects must be <strong>meritocratic</strong>. Project
		participants earn their way to additional responsibility. A regular
		contributor of quality code should be invited to become a committer
		themselves. A committer who shows leadership may one day become a
		project lead. We have a well-defined process for turning somebody into
		a committer that requires a statement of merit; very often that
		statement of merit is just a bunch of pointers to contributions that
		the individual has made to the project.
	</p>
	<p>In these videos, we discuss certain key elements of life as a
		committer on an Eclipse Project.</p>
	<p>
		You can watch all of the videos with the <a
			href="https://www.youtube.com/playlist?list=PLg5j2ehoN9bYAs5F3fL_lXB9gj3liKb49">playlist</a>.
	</p>

	<div class="video">
		<h2 id="starting">
			<a class="anchor" href="#starting"></a><a class="link"
				href="#starting">Starting an Eclipse Open Source Project</a>
		</h2>
		<div class="description">
			<?php  added("2024-11-14", true); ?>
			<p>Eclipse open source projects start with a proposal that is made
				available to the community for review. At the end of the community
				review period, we engage in a creation review, and then provision
				the project resources.</p>
			<p>This video walks through some Eclipse open source project
				fundamentals (including our definition of "project") and the process
				that we follow to propose and create a new project. If you're new to
				Eclipse open source project work or are planning to propose a new
				Eclipse open source project, this video provides the information
				that you need to get started.</p>
		</div>
		<iframe width="560" height="315"
			src="https://www.youtube.com/embed/yHNg-aPrlPI?si=NDLEAyO0AW5l1K9Q"
			title="YouTube video player" frameborder="0"
			allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
			referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
		<div class="followup">
			<p>
				Use the web form to <a
					href="https://projects.eclipse.org/node/add/project-proposal">create
					a new project proposal</a>. Instructions are provided on the form.
				All new proposals are created in draft mode, and are accessible only
				by the original author and anybody designated as a project lead or
				committer in the proposal. Only those individuals designated as a
				project lead may edit the proposal.
			</p>
			<p>
				For more information, see <a
					href="https://www.eclipse.org/projects/handbook/#starting">Starting
					an Open Source Project at the Eclipse Foundation</a> in the Eclipse
				Foundation Project Handbook.
			</p>
		</div>
	</div>
	<div class="video">
		<h2 id="roles">
			<a class="anchor" href="#roles"></a><a class="link" href="#roles">Eclipse
				Project Roles</a>
		</h2>
		<div class="description">
			<p>The Eclipse Foundation Development Process defines multiple roles.</p>
			<p>Committers are the primary gatekeepers for the project. Committers
				are the ones who decide what code goes into the repository, what
				gets included in builds, and what ends up in the open source
				project’s products.</p>
			<p>Project Leads are the first link in the project leadership chain
				and the primary liaison between the project team and the Eclipse
				Management Organization, or EMO. The EMO will, for example, ensure
				that the project leads are looped in in all matters regarding
				project governance. In cases where public channels should not be
				used, for example in the case where the security team receives a
				vulnerability report, the project lead is the first point of
				contact.</p>
			<p>The Project Management Committee (PMC) provides oversight and
				overall leadership for the projects that fall under their top Level
				project. The PMC’s role is, fundamentally, to maintain the overall
				vision and operation of the top level project and all of the
				projects that fall within its purview. Very often (though it really
				depends on the nature of the top level project), the PMC will take a
				very active role in determining how its projects fit together, and
				how they communicate and interoperate.</p>
		</div>
		<iframe width="560" height="315"
			src="https://www.youtube.com/embed/Yna8V2sOqws"
			title="YouTube video player" frameborder="0"
			allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
			allowfullscreen></iframe>
	</div>
	<div class="video">
		<h2 id="git">
			<a class="anchor" href="#git"></a><a class="link" href="#git">Eclipse
				Projects and Git</a>
		</h2>
		<div class="description">
			<p>
				Eclipse committers live in Git repositories. They push their own
				content to project repositories, and review and merge content
				contributed by others. The Eclipse Foundation supports Git by way of
				the Eclipse Foundation's <a
					href="https://www.eclipse.org/projects/handbook/#resources-gitlab">GitLab
					instance</a>, <a
					href="https://www.eclipse.org/projects/handbook/#resources-github">GitHub</a>,
				and our <a
					href="https://www.eclipse.org/projects/handbook/#resources-gerrit">Gerrit
					Code Review</a> infrastructure. Committers need to know how
				resources are structured on these platforms, how we manage
				privileges, and their role in the intellectual property due
				diligence process
			</p>
			<p>
				The purpose of the <a
					href="https://www.eclipse.org/projects/handbook/#contributing-eca">Eclipse
					Contributor Agreement</a> (ECA) is to provide a written record that
				contributors have agreed to provide their contributions of code and
				documentation under the licenses used by the Eclipse project(s)
				they’re contributing to. It also makes it clear that contributors
				are promising that what they are contributing to Eclipse is code
				that they wrote, and that they have the necessary rights to
				contribute it to Eclipse projects. And finally, it documents a
				commitment from the contributor that their open source contributions
				will be permanently on the public record.
			</p>
		</div>
		<iframe width="560" height="315"
			src="https://www.youtube.com/embed/2aQ-4m1dwzA"
			title="YouTube video player" frameborder="0"
			allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
			allowfullscreen></iframe>
	</div>
	<div class="video">
		<h2 id="elections">
			<a class="anchor" href="#elections"></a><a class="link"
				href="#elections">Committer Elections</a>
		</h2>
		<div class="description">
			<p>For Eclipse projects, and the open source world in general,
				committers are the folks who have the real power. Committers decide
				what code goes into the code base, they decide how a project builds,
				and they ultimately decide what gets delivered to the adopter
				community.</p>

			<p>
				<a href="/projects/handbook/#elections-committer">Committer
					elections</a> are the means by which new committers are added to
				the project.
			</p>

			<p>Like every process that we have at the Eclipse Foundation, the
				committer election process has been defined, and tools have been
				created, to support important underlying principles.</p>
		</div>
		<iframe width="560" height="315"
			src="https://www.youtube.com/embed/86cbB6IHe7M?si=UoPo1gHaKX9qlHJj"
			title="YouTube video player" frameborder="0"
			allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
			referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
	</div>

	<div class="video">
		<h2 id="ip">
			<a class="anchor" href="#ip"></a><a class="link" href="#ip">The
				Eclipse Foundation Intellectual Property Policy</a>
		</h2>
		<div class="description">
			<p>
				The Eclipse Foundation has a well-defined Intellectual Property
				Policy, corresponding <a
					href="https://www.eclipse.org/projects/handbook/#ip">IP Due
					Diligence Process</a>, and a dedicated team of professional IP
				specialists who perform the heavy lifting in the due diligence
				process. Eclipse Committers, the software developers who ultimately
				decide what will become Project Code and how an Eclipse open source
				project will leverage Third Party Content, are responsible for
				bringing IP issues to the attention of the Eclipse IP Team.
			</p>
		</div>
		<iframe width="560" height="315"
			src="https://www.youtube.com/embed/iO3MSuOzzYM"
			title="YouTube video player" frameborder="0"
			allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
			allowfullscreen></iframe>
		<div class="followup">
			<h3>More information</h3>
			<ul>
				<li><a href="/projects/handbook/#ip">Eclipse Intellectual Property
						Due Diligence Process</a>
					<ul>
						<li><a href="/projects/handbook/#ip-cq">Contribution
								Questionnaires</a></li>
						<li><a href="/projects/handbook/#ip-initial-contribution">Initial
								Contribution</a></li>
						<li><a href="/projects/handbook/#ip-project-content">Project
								Content</a></li>
						<li><a href="/projects/handbook/#ip-third-party">Third Party
								Content</a></li>
					</ul></li>
				<li><a href="/org/documents/Eclipse_IP_Policy.pdf">Eclipse
						Foundation Intellectual Property Policy</a></li>
			</ul>
		</div>
	</div>

	<div class="video">
		<h2 id="dash">
			<a class="anchor" href="#dash"></a><a class="link" href="#dash">The
				Eclipse Dash License Tool</a>
		</h2>
		<div class="description">
			<p>
				The <a href="https://github.com/eclipse/dash-licenses">Eclipse Dash
					License Tool</a> is a tool that is intended to help Eclipse
				Committers assess the license status of the content that their
				project leverages.
			</p>

			<p>The tool really only knows what you tell it. While it does have
				some rudimentary functionality for rooting out content, we very much
				depend on committers &mdash; who form the first line of defense in
				our intellectual property due diligence process &mdash; to
				understand the content that their project contains and leverages and
				to ensure that all content is identified and fully vetted.</p>

		</div>
		<iframe width="560" height="315"
			src="https://www.youtube-nocookie.com/embed/aXmTangVDw4"
			title="YouTube video player" frameborder="0"
			allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
			allowfullscreen></iframe>
	</div>

</div>

<?php
$html = ob_get_contents ();
ob_end_clean ();
$App->generatePage ( $Theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html );
?>