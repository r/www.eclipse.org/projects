<?php
/*******************************************************************************
 * Copyright (c) 2010, 2021 Eclipse Foundation and others.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

/*
 * This script assumes that it is being included by another script. We
 * assume that the $App variable has already been defined.
 */
require_once ($_SERVER ['DOCUMENT_ROOT'] . "/projects/classes/Review.class.inc");
require_once ($_SERVER ['DOCUMENT_ROOT'] . "/projects/classes/Project.class.php");
require_once (dirname ( __FILE__ ) . '/../classes/debug.php');

$reviews = array();
Review::get(strtotime('-2 weeks'), function($review) use (&$reviews) {
	if ($review->getStatus() == 'pending') return;
	$reviews[$review->getDate()][] = $review;
});

uksort($reviews, function($aDate, $bDate) {
	/**
	 * Compare two Reviews for sorting. The most recently completed
	 * reviews are sorted first, followed by everything else. Within
	 * the groups, everything is sorted by date and then by project id.
	 *
	 * @internal
	 * @param Review $a
	 * @param Review $b
	 * @return -1, 0, 1 if <,==,>
	 */
	$now = strtotime('now');
	$lastWeek = strtotime('-1 week');

	$aRecent = ($aDate < $now) && ($aDate > $lastWeek);
	$bRecent = ($bDate < $now) && ($bDate > $lastWeek);

	if ($aRecent == $bRecent) {
		if ($aDate < $bDate) return -1;
		if ($aDate > $bDate) return 1;
		return 0;
	}
	if ($aRecent == 1) return -1;
	return 1;
});

print '<div class="block-box"><h3>Upcoming Reviews</a></h3><div class="content">';

if (! $reviews) {
	print '<p>There are no reviews scheduled at this time.</p>';
} else {
	foreach ( $reviews as $date => $list ) {
		$when = $App->getFormattedDate ( $date, 'long' );
		$when = str_replace ( ' ', '&nbsp;', $when );

		print "<div class=\"\">";
		print "<span class=\"orange date\">{$when}</span>";
		print "<ul class=\"list-unstyled reset\">";

		foreach ($list as $review) {
			print "<li><span class=\"review-{$review->getStatus()}\"><a href=\"{$review->getUrl()}\">{$review->getName()}</a></span></li>";
		}

		print "</ul>";
		print "<hr class=\"reset\"/>";
		print "</div>";
	}
}
print '<p align="right"><a href="/projects/tools/reviews.php">Show all reviews...</a></p>';
print '</div></div>';
