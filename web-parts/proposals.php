<?php
/*******************************************************************************
 * Copyright (c) 2010 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Wayne Beaton (Eclipse Foundation)- initial API and implementation
 *******************************************************************************/

/*
 * This script assumes that it is being included by another script. We
 * assume that the $App variable has already been defined.
 */
require_once ($_SERVER['DOCUMENT_ROOT'] . "/projects/classes/Proposal.class.php");
$activities = getProposals();

print '<div class="block-box"><h3>New Project Proposals</h3><div class="content">';
if (! $activities) {
    print '<p>There are no new project proposals at this time.</p>';
} else {
    print '<ul class="list-unstyled reset">';
    /** @var \Proposal $activity */
    foreach ($activities as $activity) {
        $name = htmlentities($activity->getName());
        
        if ($activity->isWithdrawn())
            continue;
        
        $link = $activity->getProposalUrl();
        
        $date = $App->getFormattedDate($activity->getDate(), 'long');
        $date = str_replace(' ', '&nbsp;', $date);
        
        print '<li><a href="' . $link . '">' . $name . '</a></li>';
    }
    print '</ul>';
}

print '<p align="right"><a href="/projects/tools/proposals.php">Show all proposals...</a></p>';
print '</div></div>';