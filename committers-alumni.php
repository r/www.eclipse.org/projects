<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'
/* 221934 - this page to remain on eclipse.org */
$pageTitle 		= "Eclipse Committers Alumni";
$pageKeywords   = '';
$pageAuthor     = '';

include( '_commonLeftNav.php' );

ob_start();
?>
<div id="maincontent">
<div id="midcolumn">

<h1>Eclipse Committers Alumni</h1>
<p>
When Committers who have contributed to Eclipse
become inactive and thus no longer Committers<sup>1</sup>, we want to make sure that we acknowledge their
 contributions to our (collective) success. We choose to make that acknowledgement
 here on the list of our <em>Committer Alumni</em>.
<p>
Our thanks to each of these people and their
 contributions to the long term success
of all of Eclipse.
<p>
<span style="color: #D4A017">*</span>The list of former Committers who made especially significant contributors can be found at <a href="committers-emeritus.php">Committers Emeritus</a>.
<p>
<?php if( isset($_GET['sort']) && $_GET['sort'] == 'project' ) { ?>
List is sorted by project (click on project to expand to list of names). <a href="?sort=name">Click here to re-sort</a> by name.
<?php } else { ?>
List is sorted by name. <a href="?sort=project">Click here to re-sort</a> by project.
<?php } ?>
</td>
<ul>
<?php
$sql = "SELECT DISTINCT PersonID AS PID FROM PeopleProjects
		WHERE Relation = 'CE'
		  AND InactiveDate IS NULL
		  ";
$result = $App->foundation_sql($sql);
$emeritus = array();
while( $row = mysqli_fetch_assoc($result) ) {
	$emeritus[$row['PID']] = 1;
}
$sql = "SELECT People.PersonID as PID, FName, LName, ProjectID FROM People, PeopleProjects
		WHERE People.PersonID = PeopleProjects.PersonID
		  AND PeopleProjects.Relation = 'CM'
		  AND PeopleProjects.InactiveDate IS NOT NULL
		  AND People.PersonID NOT IN (SELECT PersonID FROM PeopleProjects
		  		WHERE Relation = 'CM'
		  		  AND InactiveDate IS NULL)
		  ORDER BY LName, FName
		  ";
$result = $App->foundation_sql($sql);
$people = array();
while( $row = mysqli_fetch_assoc($result) ) {
	$people[] = $row;
}
function sortbyproject( $a, $b ) {
	if( $a['ProjectID'] > $b['ProjectID'] ) return 1;
	if( $a['ProjectID'] < $b['ProjectID'] ) return -1;
	if( $a['LName'] > $b['LName'] ) return 1;
	if( $a['LName'] < $b['LName'] ) return -1;
	if( $a['FName'] > $b['FName'] ) return 1;
	if( $a['FName'] < $b['FName'] ) return -1;
	return 0;
}
?>
<script>
function toggleshow(id) {
	var obj = document.getElementById(id);
	if( obj.style.display == 'block' ) {
		obj.style.display = 'none';
	} else {
		obj.style.display = 'block';
	}
}
</script>
<?php
if( isset($_GET['sort']) && $_GET['sort'] == 'project' ) {
	/* sorted by project */
	usort( $people, 'sortbyproject' );
	$showing = -1;
	foreach( $people as $row ) {
		if( $showing != $row['ProjectID'] ) {
			if( $showing != -1 ) {
				?></ul></li>
				<?php
			}
			?><li><a onclick="toggleshow('<?= $row['ProjectID'] ?>')"
			style="font-weight: normal"><?= $row['ProjectID'] ?></a><ul
				id="<?= $row['ProjectID'] ?>" style="display: none"><?php
			$showing = $row['ProjectID'];
		}
		?><li><b><?= $row['FName'] ?> <?= $row['LName'] ?></b><?=
			isset($emeritus[$row['PID']]) ? '<span style="color: #D4A017">*</span>' : ''
		?></li><?php
	}
	?></ul></li>
	<?php
} else {
	/* sorted by name with (projects) */
	$showing = -1;
	foreach( $people as $row ) {
		if( $showing == $row['PID'] ) {
			?>, <?= $row['ProjectID'] ?><?php
		} else {
			if( $showing == -1 ) {
				//first row, nothing to close
			} else {
				?>)</li><?php
			}
			?><li><b><?= $row['FName'] ?> <?= $row['LName'] ?></b><?=
				isset($emeritus[$row['PID']]) ? '<span style="color: #D4A017">*</span>' : ''
			?> (<?= $row['ProjectID'] ?><?php
			$showing = $row['PID'];
		}
	}
	if( $showing != -1 ) {
		?>)</li></ul>
		<?php
	}
}
?>
<p>
<hr style="margin-top: 5px; margin-bottom: 5px;"/>
<div style="font-size: 60%"><sup>1</sup>
Being a Committer is a privilege that is earned by
contributing actively, and showing discipline and good judgment. It is a responsibility
that is not given, nor be treated, lightly. At the same
time, we acknowledge that active participation in Eclipse Projects
is not a lifetime commitment, and thus Committers may/will become
inactive for a variety of reasons.
</div>
</div><!-- midcolumn -->
</div><!-- maincontent -->
<?php
	# Paste your HTML content between the EOHTML markers!
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
